/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application;
	
import java.util.logging.Level;
import java.util.logging.Logger;

import application.gui.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;

/**
 * This class is the main entry point for ClusVis. It consists of two 
 * methods which are used to instantiate the app. 
 * 
 * @author Nejc Trdin
 * @version 1.0
 */

public class ClusVis extends Application {
	
	/**
	 * GPL message and about message. 
	 */	
	public final static String aboutAndGPL = 
			"ClusVis is a visualization application for models "
			+ "produced with Clus. To use the tool, import an xml file generated with Clus "
			+ "giving it -xml argument.\n\n"
			+ "Clus is a decision tree and rule induction system that implements "
		    + "the predictive clustering framework. This framework unifies unsupervised "
		    + "clustering and predictive modeling and allows for a natural extension to more "
		    + "complex prediction settings such as multi-task learning and multi-label "
		    + "classification. While most decision tree learners induce classification or "
		    + "regression trees, Clus generalizes this approach by learning trees that are "
		    + "interpreted as cluster hierarchies. We call such trees predictive clustering "
		    + "trees or PCTs. Depending on the learning task at hand, different goal criteria "
		    + "are to be optimized while creating the clusters, and different heuristics will be "
		    + "suitable to achieve this.\n\n"
		    + "This program is free software: you can redistribute it and/or modify it under the "
		    + "terms of the GNU General Public License as published by the Free Software Foundation"
		    + ", either version 3 of the License, or (at your option) any later version.\n\n"
		    + "This program is distributed in the hope that it will be useful, but WITHOUT ANY"
		    + " WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A"
		    + " PARTICULAR PURPOSE. See the GNU General Public License for more details.\n\n"
		    + "Clus is free software (licensed under the GPL)\n"
		    + "ClusVis is free software (licensed under the GPL).\n\n"
		    + "Clus - Software for Predictive Clustering\n"
		    + "Copyright \u00A9 2015\n"
		    + "    Katholieke Universiteit Leuven, Leuven, Belgium\n"
		    + "    Jozef Stefan Institute, Ljubljana, Slovenia\n\n"
		    + "Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.\n"
		    + "Contact information for Clus: http://www.cs.kuleuven.be/~dtai/clus/.";
	
	/**
	 * Application name.
	 */
	public final static String appName = "ClusVis";
	
	/**
	 * Application version.
	 */
	public final static String version = "1.0";
	
	
	/**
	 * Method reads and loads the FXML file. Sends the main stage reference to the controller.
	 * Creates the scene, sets up the name and the icon of the app. Finally it shows the main stage
	 * and waits until app is closed.
	 * 
	 * @param primaryStage the primary stage that is constructed during initialization and is used
	 * 			for showing the main app.
	 */
	@Override
	public void start(Stage primaryStage) 
	{
		try {			
			FXMLLoader loader = new FXMLLoader(ClusVis.class.getResource("gui/Main.fxml"));
			AnchorPane page = (AnchorPane)loader.load();
			MainController controller = (MainController)loader.getController();
			controller.setStage(primaryStage);
			
            Scene scene = new Scene(page);
            primaryStage.setScene(scene);            
            primaryStage.setTitle(String.join(" ", new String[]{appName, version}));
            primaryStage.getIcons().add(new Image(ClusVis.class.getResourceAsStream("gui/app-icon.png")));
            
            primaryStage.show();
		} catch(Exception ex) {
			Logger.getLogger(ClusVis.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	/**
	 * Method that launches the app.
	 * 
	 * @param args arguments from command line
	 */
	public static void main(String[] args) 
	{
		launch(args);
	}
}
