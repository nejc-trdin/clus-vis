/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/
package application.gui;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javax.management.modelmbean.XMLParseException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import application.ClusVis;
import application.gui.plot.PlotController;
import application.model.ClusModel;
import application.model.ClusNode;
import application.model.examples.ExampleRepo;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Transform;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * This is the main controller for the application. It gives a lot of options,
 * for the user to use. It supports opening the XML files and then passes the file
 * to the correct methods for parsing. The parsed model is then displayed as a tree
 * by using the grid pane. User can double-click on labels, so they disappear or appear
 * with their corresponding choices and edges. User can also right-click on the nodes,
 * and a context menu appears, where the user can select prediction (to show the 
 * prediction) or examples in this node. If the node is not terminal, the user can also 
 * select to show the test string from Clus. Additionally, some information about the
 * model is displayed on the top and on the bottom.
 * 
 * @author Nejc Trdin
 *
 */
public class MainController implements Initializable {
	
	/**
	 * A <code>Label</code> defined in the FXML, to display the file name of the model.
	 */
	@FXML
	private Label fileNameLbl;
	
	/**
	 * A <code>Label</code> defined in the FXML, to display the name of the model.
	 */
	@FXML
	
	private Label modelNameLbl;
	
	/**
	 * A <code>Label</code> defined in the FXML, to display the statistic name of the model.
	 */
	@FXML
	private Label statisticLbl;
	
	/**
	 * A <code>Label</code> defined in the FXML, to display the number of nodes, of the model.
	 */
	@FXML
	private Label nodesLbl;
	
	/**
	 * A <code>Label</code> defined in the FXML, to display the number of leafs, of the model.
	 */
	@FXML
	private Label leafsLbl;
	
	/**
	 * A <code>Label</code> defined in the FXML, to display the number of examples, of the model.
	 */
	@FXML
	private Label examplesLbl;
	
	/**
	 * A <code>MenuItem</code> defined in the FXML, to open a new model.
	 */
	@FXML
	private MenuItem openFileMenu;
	
	/**
	 * A <code>MenuItem</code> defined in the FXML, to save the model to image.
	 */
	@FXML
	private MenuItem saveImageMenu;
	
	/**
	 * A <code>MenuItem</code> defined in the FXML, to save the model to xml.
	 */
	@FXML
	private MenuItem saveModelMenu;
	
	/**
	 * A <code>MenuItem</code> defined in the FXML, to close the application.
	 */
	@FXML
	private MenuItem closeAppMenu;
	
	/**
	 * A <code>MenuItem</code> defined in the FXML, to open the about window.
	 */
	@FXML
	private MenuItem showAboutMenu;
	
	/**
	 * An <code>AnchorPane</code> defined in the FXML, that is used to hold all the model nodes and
	 * edges. Specifically, the nodes are plotted on the child <code>drawingPane</code> and edges are
	 * displayed on this layer.
	 */
	@FXML
	private AnchorPane drawingAnchorPane;
	
	/**
	 * A <code>GridPane</code> is put on <code>drawingAnchorPane</code> with code. It holds the nodes
	 * of the model.
	 */
	private GridPane drawingPane;
	
	/**
	 * The {@link ClusModel} that is currently open in the application.
	 */
	private ClusModel model;

	/**
	 * The <code>Stage</code> is needed to open new models. 
	 */
	private Stage mainStage;
	
	/**
	 * The mapping from each node, to a boolean value, if the node is visible.
	 */
	private HashMap<ClusNode, Boolean> visible; 
	
	/**
	 * The mapping from each displayed node, to its corresponding label.
	 */
	private HashMap<ClusNode, Label> nodeToLabelMap;
	
	/**
	 * This is a set of nodes, that have their position computed.
	 */
	private HashSet<ClusNode> computedBounds;
	
	/**
	 * The binding of x coordinate used for placement of parent nodes, during drawing of edges.
	 */
	private DoubleBinding parentX;
	
	/**
	 * The binding of y coordinate used for placement of parent nodes, during drawing of edges.
	 */
	private DoubleBinding parentY;
	
	/**
	 * The parent node, used during drawing of edges.
	 */
	private ClusNode drawingParent;
	
	/**
	 * This is a mapping from a node, to the line, that ends in this particular node. Used for
	 * showing and hiding lines.
	 */
	private HashMap<ClusNode, Line> lineEndsIn;
	
	/**
	 * This is a mapping from a node, to the label, that ends in this particular node. Used for
	 * showing and hiding choice labels on edges ("yes", "no").
	 */
	private HashMap<ClusNode, Label> labelEndsIn;
	
	/**
	 * This value is set to <code>true</code>, when all the labels have their position set.
	 */
	private static boolean isComputedBounds = false;
	
	/**
	 * The initialization of the controller, called by the library. It also initializes the
	 * labels that show information about the model, see {@link #initializeLabels()}.
	 * 
	 * @param location the supplied <code>URL</code> giving the location.
	 * @param resources the <code>ResourceBundle</code> giving the resources during
	 * 			initialization.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		initializeLabels();		
	}
	
	/**
	 * This sets the main stage and also initializes the menu with {@link #initializeMenu()}.
	 * 
	 * @param stage the main stage, that is used to show the application.
	 */
	public void setStage(Stage stage)
	{
		mainStage = stage;
		initializeMenu();		
	}
	
	/**
	 * Sets all the labels, that hold information about the model to empty string, so that
	 * nothing is displayed, when the application is loaded for the first time. The labels
	 * in question are {@link #fileNameLbl}, {@link #modelNameLbl}, {@link #statisticLbl},
	 * {@link #nodesLbl}, {@link #leafsLbl} and {@link #examplesLbl}.
	 */
	private void initializeLabels()
	{
		fileNameLbl.setText("");
		modelNameLbl.setText("");
		statisticLbl.setText("");
		nodesLbl.setText("");
		leafsLbl.setText("");
		examplesLbl.setText("");
	}
	
	/**
	 * This attaches action event handlers to the three menu items {@link #closeAppMenu}, 
	 * {@link #showAboutMenu} and {@link #openFileMenu}. {@link #closeAppMenu} closes the
	 * application, {@link #showAboutMenu} shows the about window and {@link #openFileMenu}
	 * opens a new file opener, and opens a file if specified.
	 */
	private void initializeMenu()
	{
		//close application
		closeAppMenu.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {
		        System.exit(0);
		    }
		});
		
		//show about window
		showAboutMenu.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {		    	
		    	Alert alert = new Alert(AlertType.INFORMATION);
		    	alert.setTitle(String.join(" ", 
		    			new String[]{"About", ClusVis.appName, ClusVis.version}));
		    	alert.setHeaderText(String.join(" ", 
		    			new String[]{ClusVis.appName, ClusVis.version}));		    	
		    	TextArea textArea = new TextArea(ClusVis.aboutAndGPL);
		    	textArea.setEditable(false);
		    	textArea.setWrapText(true);
		    	textArea.setMaxWidth(Double.MAX_VALUE);
		    	textArea.setMaxHeight(Double.MAX_VALUE);
		    	GridPane.setVgrow(textArea, Priority.ALWAYS);
		    	GridPane.setHgrow(textArea, Priority.ALWAYS);
		    	GridPane expContent = new GridPane();
		    	expContent.setMaxWidth(Double.MAX_VALUE);		    	
		    	expContent.add(textArea, 0, 1);
		    	alert.getDialogPane().setContent(expContent);		    		    			    		    	
		    	alert.showAndWait();		    	
		    }
		});
		
		//open new file
		openFileMenu.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {
		    	FileChooser fileChooser = new FileChooser();
		    	fileChooser.setTitle("Open Resource File");
		    	//only permissible files are xml
		    	fileChooser.getExtensionFilters().add(
		    			new FileChooser.ExtensionFilter("Clus XML files", "*.xml"));
		    	File file = fileChooser.showOpenDialog(mainStage);
		    	if(file != null)
		    	{		    				    		
		    		try {
						model = new ClusModel(file);						
						updateModelValues();
						drawModel();						
					} catch (ParserConfigurationException | SAXException
							| IOException | XMLParseException ex) {	
						//if there is an exception, catch it and open a new window, 
						//showing the message and stack trace
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Exception");
						alert.setHeaderText("Exception occured!");
						
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						ex.printStackTrace(pw);
						String exceptionText = sw.toString();
						Label labelMessage = new Label(ex.getMessage());
						Label label = new Label("The exception stacktrace:");

						TextArea textArea = new TextArea(exceptionText);
						textArea.setEditable(false);
						textArea.setWrapText(true);

						textArea.setMaxWidth(Double.MAX_VALUE);
						textArea.setMaxHeight(Double.MAX_VALUE);
						GridPane.setVgrow(textArea, Priority.ALWAYS);
						GridPane.setHgrow(textArea, Priority.ALWAYS);

						GridPane expContent = new GridPane();
						expContent.setMaxWidth(Double.MAX_VALUE);
						expContent.add(labelMessage, 0, 0);
						expContent.add(label, 0, 1);
						expContent.add(textArea, 0, 2);
						
						alert.getDialogPane().setContent(expContent);
						alert.showAndWait();
					}		    		
		    	}
		    }
		});
		
		//disable saving until model exists
		saveImageMenu.setDisable(true);
		saveModelMenu.setDisable(true);
	}
	
	/**
	 * The method updates the labels {@link #fileNameLbl}, {@link #modelNameLbl}, 
	 * {@link #statisticLbl}, {@link #nodesLbl}, {@link #leafsLbl} and 
	 * {@link #examplesLbl} to values that are parsed from the model. It also enables
	 * {@link #saveImageMenu} and {@link #saveModelMenu}.
	 */
	private void updateModelValues()
	{
		if(model != null)
		{
			saveImageMenu.setDisable(false);
			saveModelMenu.setDisable(false);
			fileNameLbl.setText(model.getModelFileName());
			modelNameLbl.setText(model.getModelName());
			statisticLbl.setText(model.getStatisticName());
			nodesLbl.setText(String.valueOf(model.getNNodes()));
			leafsLbl.setText(String.valueOf(model.getNLeafs()));
			examplesLbl.setText(String.valueOf(model.getNExamples()));
		}
	}
	
	/**
	 * The main method that is an entry point for plotting the model, if 
	 * the model is not <code>null</code>. It initializes the data structures 
	 * needed for plotting the model. It also constructs a new grid pane, sets 
	 * its position on the {@link #drawingAnchorPane} and allocates the 
	 * appropriate number of rows and columns in the {@link #drawingPane}, by
	 * getting the model width and depth via {@link #getModelWidth()} and
	 * {@link #getModelDepth()}.
	 */
	private void drawModel()
	{
		if(model != null)
		{
			//initialize data structures
			visible = new HashMap<ClusNode, Boolean>();
			computedBounds = new HashSet<ClusNode>();
			isComputedBounds = false;
			nodeToLabelMap = new HashMap<ClusNode, Label>();
			parentX = null;
			parentY = null;			
			lineEndsIn = new HashMap<ClusNode, Line>();
			labelEndsIn = new HashMap<ClusNode, Label>();
			
			//get model depth and width
			int depth = getModelDepth();
			int width = getModelWidth();			
			//create drawing pane
			drawingPane = new GridPane();
			drawingPane.setHgap(5.0);
			drawingPane.setVgap(5.0);
			//position the pane and add it to the drawing pane
			drawingAnchorPane.getChildren().clear();
			drawingAnchorPane.getChildren().add(drawingPane);
			AnchorPane.setTopAnchor(drawingPane, 10.0);
			AnchorPane.setBottomAnchor(drawingPane, 10.0);
			AnchorPane.setLeftAnchor(drawingPane, 10.0);
			AnchorPane.setRightAnchor(drawingPane, 10.0);
			
			//initialize space for columns and rows
			for(int i=0;i<width;i++)
			{
				ColumnConstraints column = new ColumnConstraints();
				column.setHgrow(Priority.ALWAYS);								
				drawingPane.getColumnConstraints().add(column);
			}
			for(int i=0;i<depth;i++)
			{
				RowConstraints row = new RowConstraints();
				row.setVgrow(Priority.ALWAYS);				
				drawingPane.getRowConstraints().add(row);
			}		
			
			//actually draw the hierarchy
			drawHierarchy();			
		}		
	}
	
	/**
	 * The main entry point for drawing the model. It uses method 
	 * {@link #drawNode(ClusNode, int, int)} to draw the model tree.
	 */
	private void drawHierarchy()
	{
		drawNode(model.getRoot(), 0, 0);		
	}
	
	/**
	 * This method is used for traversing the model, updating corresponding
	 * positions via depth and width. The depth is increased by 1 for each recursive
	 * call to a child and width is updated as the output of the recursive call. The
	 * return is actually the right-most child's width position in the model. The method
	 * calls {@link #drawAndInitNode(ClusNode, int, int)} to place the label of this node
	 * on the {@link #drawingPane}.
	 * 
	 * @param node the {@link ClusNode} being plotted.
	 * @param depth the current depth of the node.
	 * @param width the width, that is given as the right most currently plotted node.
	 * 			All the nodes below this node (inclusively this one) are plotted at
	 * 			width larger than this parameter.
	 * 
	 * @return the width parameter of the right-most plotted node.
	 */
	private int drawNode(ClusNode node, int depth, int width)
	{
		ArrayList<ClusNode> children = node.getChildren();
		if(children.size()==0)
		{
			//node is terminal
			drawAndInitNode(node, depth, width);
			return width + 2;
		}
		else
		{
			//node is not terminal
			int currentMax = width;
			//first plot the children
			for(ClusNode child: children)
			{
				currentMax = drawNode(child, depth + 1, currentMax);				
			}
			//finally plot this parent in the middle of the children
			int parentW = (width+currentMax-2)/2;
			drawAndInitNode(node, depth, parentW);
			return currentMax;
		}
	}
	
	/**
	 * Recursively hides all sub-nodes (and corresponding edges) and this node.
	 * Note that {@link #visible} mapping is not changed in this method. It is
	 * changed via user interaction.
	 * 
	 * @param node the node which needs to be hidden.
	 */
	private void hideAllSubNodes(ClusNode node)
	{
		//hide line
		lineEndsIn.get(node).setVisible(false);
		//hide choice label
		labelEndsIn.get(node).setVisible(false);
		//hide node
		nodeToLabelMap.get(node).setVisible(false);
		//hide all children below
		for(ClusNode child: node.getChildren())
		{
			hideAllSubNodes(child);
		}
	}
	
	/**
	 * Recursively shows all sub-nodes (and corresponding edges) and this node, 
	 * if they are visible according to the {@link #visible} mapping. Note that 
	 * {@link #visible} mapping is not changed in this method. It is changed 
	 * via user interaction.
	 * 
	 * @param node the node which needs to be shown.
	 */
	private void showAllVisibleSubNodes(ClusNode node)
	{
		if(visible.get(node))
		{
			//current node is visible
			//show line
			if(lineEndsIn.containsKey(node))
			{
				lineEndsIn.get(node).setVisible(true);	
			}
			//show choice label
			if(labelEndsIn.containsKey(node))
			{
				labelEndsIn.get(node).setVisible(true);	
			}
			//show node
			nodeToLabelMap.get(node).setVisible(true);
			//show nodes below
			for(ClusNode child: node.getChildren())
			{
				showAllVisibleSubNodes(child);
			}
		}
	}
	
	/**
	 * This is the most complex method of this class. It constructs a label for
	 * current node and places it at <code>depth</code>'s row and 
	 * <code>width</code>'s column of the {@link #drawingPane}. It also attaches
	 * information about test string and possible examples and prediction summary.
	 * It installs a tooltip on mouse-over, so that if the label is to small to
	 * display everything, the tooltip shows all the information. 
	 * Finally it also adds mouse listeners, for showing/hiding labels and adds
	 * a listener, for when the node has a position set. After that, the method for
	 * drawing edges is called (that actually draws the edges), see 
	 * {@link #drawEdges(ClusNode)}.
	 * 
	 * @param node the {@link ClusNode} being drawn.
	 * @param depth the depth at which the node should be drawn.
	 * @param width the width at which the node should be drawn.
	 */
	private void drawAndInitNode(ClusNode node, int depth, int width)
	{
		Label lbl;
		if(node.getChildren().size()==0)
		{
			//this is a leaf node
			String labelS = "";
			if(node.isCOPKMeans())
			{
				labelS = node.getCOPKMeansShortDescription()+"\n";
			}			
			labelS += String.join(", ", new String[]{String.join("", new String[]{
					"E: ", Integer.toString(node.getNExamples())}), node.getStatistic().getShortString()});
			lbl = new Label(labelS);
			//add tooltip
			lbl.setTooltip(new Tooltip(labelS));
		}
		else
		{
			//node has children, and hence should have a test string
			//add information
			String labelS = String.join("", new String[]{
					node.getTestString(),"\n","E: ", Integer.toString(node.getNExamples()), 
					", ", node.getStatistic().getShortString()});
			lbl = new Label(labelS);	
			//adding a tooltip
			lbl.setTooltip(new Tooltip(labelS));
			
			//handling double click event
			lbl.setOnMouseClicked(new EventHandler<MouseEvent>(){		
				public void handle(MouseEvent event) {
					if(event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2)
					{
						ArrayList<ClusNode> children = node.getChildren();						
						
						boolean allHidden = true;
						//we need to determine if all children are hidden
						for(ClusNode child: children)
						{
							allHidden &= !visible.get(child);
						}						
						if(allHidden)
						{
							//all children are hidden, we make them visible
							for(ClusNode child: children)
							{
								visible.put(child, true);									
							}
							showAllVisibleSubNodes(node);
						}
						else
						{
							//at least one child is visible, we make all hidden							
							for(ClusNode child: children)
							{
								visible.put(child, false);
								hideAllSubNodes(child);
							}							
						}						
					}
				}			
			});
		}	
		//set the label properties, to position the text
		lbl.setTextAlignment(TextAlignment.CENTER);
		lbl.setStyle("-fx-background-color: white;");
		lbl.setAlignment(Pos.CENTER);	
		lbl.setMinWidth(100);		
		lbl.setMaxWidth(200);
		//set the growing properties of the label inside the parent
		HBox.setHgrow(lbl, Priority.ALWAYS);
		VBox.setVgrow(lbl, Priority.ALWAYS);
		
		//put the node into the data structures for visibility and mapping
		visible.put(node, true);
		nodeToLabelMap.put(node, lbl);
		//position the node on the drawingPane
		drawingPane.add(lbl, width, depth);	
		
		//a listener to for the position property
		//it calls a method for drawing edges, when all labels have their position set
		lbl.localToParentTransformProperty().addListener(new ChangeListener<Transform>(){			
			@Override
			public void changed(
					ObservableValue<? extends Transform> observable,
					Transform oldValue, Transform newValue) {
				computedBounds.add(node);
				if(!isComputedBounds && computedBounds.size()==model.getNNodes())
				{						
					//isComputedBounds prevents from drawing multiple edges
					isComputedBounds = true;					
					drawEdges(model.getRoot());					
				}				
			}}
		);
				
		MainController parent = this;
		//add context menu
		lbl.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {		    
			@Override
			public void handle(ContextMenuEvent event) {
				ContextMenu cm = new ContextMenu();
				MenuItem stats = new MenuItem("Show Prediction");
				//showing the statistic
				stats.setOnAction(new EventHandler<ActionEvent>(){
					@Override
					public void handle(ActionEvent event) {
						try 
						{
							//load the correct statistic
							FXMLLoader loader = new FXMLLoader(ClusVis.class.getResource(node.getStatisticFXML()));			
							AnchorPane pane = loader.load();
							PlotController controller = (PlotController)loader.getController();		
							//sent the parent controller and the node, to get the information
							controller.setParentController(parent);
							controller.setParentNode(node);
							Stage stage = new Stage();
							Scene scene = new Scene(pane);
							stage.setTitle(node.getStatisticName());
							stage.setScene(scene);
							stage.initModality(Modality.APPLICATION_MODAL);								
							stage.show();	

						} catch (Exception ex) 
						{
							//catch exceptions and show a message window with the stack trace
							Alert alert = new Alert(AlertType.ERROR);
							alert.setTitle("Exception");
							alert.setHeaderText("Exception occured!");
							
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							ex.printStackTrace(pw);
							String exceptionText = sw.toString();
							Label labelMessage = new Label(ex.getMessage());
							Label label = new Label("The exception stacktrace:");

							TextArea textArea = new TextArea(exceptionText);
							textArea.setEditable(false);
							textArea.setWrapText(true);

							textArea.setMaxWidth(Double.MAX_VALUE);
							textArea.setMaxHeight(Double.MAX_VALUE);
							GridPane.setVgrow(textArea, Priority.ALWAYS);
							GridPane.setHgrow(textArea, Priority.ALWAYS);

							GridPane expContent = new GridPane();
							expContent.setMaxWidth(Double.MAX_VALUE);
							expContent.add(labelMessage, 0, 0);
							expContent.add(label, 0, 1);
							expContent.add(textArea, 0, 2);
							
							alert.getDialogPane().setContent(expContent);
							alert.showAndWait();
						}
					}});
				MenuItem test = new MenuItem("Show Test String");
				//showing the test string in a window
				test.setOnAction(new EventHandler<ActionEvent>(){
					@Override
					public void handle(ActionEvent event) {
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Test String");
						alert.setHeaderText(null);
						alert.setContentText(node.getTestString());
						alert.showAndWait();						
					}});
				MenuItem examples = new MenuItem("Show Examples");
				
				//showing examples
				examples.setOnAction(new EventHandler<ActionEvent>(){
					@Override
					public void handle(ActionEvent event) {
						try {		
							//load the example viewer
							FXMLLoader loader = new FXMLLoader(ClusVis.class.getResource("gui/ExampleView.fxml"));
							AnchorPane pane = loader.load();
							ExampleViewController controller = (ExampleViewController)loader.getController();
							Stage stage = new Stage();
							Scene scene = new Scene(pane);
							stage.setTitle(String.join(" ", new String[]{
									"Examples:", Integer.toString(node.getNExamples())}));
							stage.setScene(scene);
							stage.initModality(Modality.APPLICATION_MODAL);
							ExampleRepo exampleRepo = node.getExamples();
							int i=1;
							//send all the columns and examples to the example viewer
							for(String columnName: exampleRepo.getAttributes())
							{
								controller.addColumn(columnName, i);
								i++;
							}
							for(ArrayList<String> example: exampleRepo.getExampleValues())
							{
								controller.addRow(example);
							}	
							controller.setClasses(exampleRepo.getClassIDAttributes());
							controller.setParentNode(node);
							stage.show();
						} catch (IOException ex) 
						{
							//catch exceptions and show a message window with the stack trace
							Alert alert = new Alert(AlertType.ERROR);
							alert.setTitle("Exception");
							alert.setHeaderText("Exception occured!");
							
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							ex.printStackTrace(pw);
							String exceptionText = sw.toString();
							Label labelMessage = new Label(ex.getMessage());
							Label label = new Label("The exception stacktrace:");

							TextArea textArea = new TextArea(exceptionText);
							textArea.setEditable(false);
							textArea.setWrapText(true);

							textArea.setMaxWidth(Double.MAX_VALUE);
							textArea.setMaxHeight(Double.MAX_VALUE);
							GridPane.setVgrow(textArea, Priority.ALWAYS);
							GridPane.setHgrow(textArea, Priority.ALWAYS);

							GridPane expContent = new GridPane();
							expContent.setMaxWidth(Double.MAX_VALUE);
							expContent.add(labelMessage, 0, 0);
							expContent.add(label, 0, 1);
							expContent.add(textArea, 0, 2);
							
							alert.getDialogPane().setContent(expContent);
							alert.showAndWait();
						}					
					}});
				//add menu items to the context menu
				cm.getItems().add(stats);
				if(node.getChildren().size()>0)
				{
					cm.getItems().add(test);
				}
				cm.getItems().add(examples);
				//show the context menu at the event position
				cm.show(lbl, event.getScreenX(), event.getScreenY());				
			}});		
	}
	
	/**
	 * Edges are drawn, by keeping track of the class defined parent of the
	 * currently being drawn child. The position of the edge are computed
	 * from the position of the parent and the currently being drawn node.
	 * The method is called for all children of the node, updating the parent
	 * accordingly. It also positions the choice text if it exists.
	 * 
	 * @param node {@link ClusNode} that is being drawn.
	 */
	private void drawEdges(ClusNode node)
	{
		//get the label
		Label label = nodeToLabelMap.get(node);
		if(parentX == null && parentY == null)
		{			
			//we have the root
			drawingParent = node;
			//get the positions of this node
			parentX = label.layoutXProperty().add(label.widthProperty().divide(2.0));
			parentY = label.layoutYProperty().add(label.heightProperty().divide(2.0));	
			//draw children
			for(ClusNode child: node.getChildren())
			{
				drawEdges(child);				
			}			
			//return to the previous state
			drawingParent = null;
			parentX = null;
			parentY = null;			
		}
		else
		{			
			//save data
			DoubleBinding oldX = parentX;
			DoubleBinding oldY = parentY;
			ClusNode oldParent = drawingParent;
			drawingParent = node;
			//get new parent
			parentX = label.layoutXProperty().add(label.widthProperty().divide(2.0));
			parentY = label.layoutYProperty().add(label.heightProperty().divide(2.0));
			
			//drawing choice		
			if(node.getChoice()!=null)
			{
				//add a new label in the middle of the line
				Label lbl = new Label(node.getChoice());			
				lbl.setFont(Font.font("System", FontWeight.BOLD, 13));
				lbl.setLayoutX(Math.abs((oldX.doubleValue()+parentX.doubleValue()))/2);
				lbl.setLayoutY(Math.abs((oldY.doubleValue()+parentY.doubleValue()))/2);	
				labelEndsIn.put(node, lbl);
				drawingAnchorPane.getChildren().add(0,lbl);
			}	
			
			//create line
			Line line = new Line();
			drawingAnchorPane.getChildren().add(0,line);
			//bind the positions of the line to the nodes
			line.startXProperty().bind(oldX);
			line.startYProperty().bind(oldY);			
			line.endXProperty().bind(parentX);
			line.endYProperty().bind(parentY);
			//put the line into the data structures
			lineEndsIn.put(node, line);			
			
			//create edges below below
			for(ClusNode child: node.getChildren())
			{
				drawEdges(child);				
			}
			
			//establish previous state
			parentX = oldX;
			parentY = oldY;
			drawingParent = oldParent;
		}
	}
	
	/**
	 * Gets the model depth, by calling the {@link #getModelDepthVNodes(ClusNode)}
	 * on the root node.
	 * 
	 * @return the model depth.
	 */
	private int getModelDepth()
	{
		return getModelDepthVNodes(model.getRoot());
	}
	
	/**
	 * Gets the depth of the model from this node onwards, by
	 * computing the maximal value of the hierarchy depth over nodes
	 * children. If the node is a leaf, 1 is returned.
	 * 
	 * @param node the {@link ClusNode} for which the depth is queried.
	 * 
	 * @return the maximal depth of the model from this node.
	 */
	private int getModelDepthVNodes(ClusNode node)
	{
		ArrayList<ClusNode> children = node.getChildren();
		if(children.size()==0)
		{
			return 1;
		}
		else
		{
			int maximum = Integer.MIN_VALUE;
			for(ClusNode child: children)
			{
				int childDepth = getModelDepthVNodes(child);
				if(childDepth > maximum)
				{
					maximum = childDepth;
				}
			}
			return maximum + 1;
		}
	}
	
	/**
	 * Gets the width of the model, so that each node in a column
	 * is separated by one column. It does it by calling
	 * {@link #getModelWidthVNodes(ClusNode, int)} on the root of the model.
	 * 
	 * @return the width of the hierarchy.
	 */
	private int getModelWidth()
	{
		return getModelWidthVNodes(model.getRoot(), 0);
	}
	
	/**
	 * Recursively gets the width of the model, so that each node in
	 * a column is separated by one column. If this is a hierarchical leaf
	 * <code>min+1</code> is returned. Otherwise the values are recursively
	 * computed with the invariant, changing the value of <code>min</code> 
	 * accordingly.
	 * 
	 * @param node the {@link ClusNode} for which we would like to get
	 * 			the width.
	 * @param min the leftmost position of the parent's most right child.
	 * 
	 * @return the width position of the right-most child of the model below this node.
	 */
	private int getModelWidthVNodes(ClusNode node, int min)
	{
		ArrayList<ClusNode> children = node.getChildren();
		if(children.size()==0)
		{
			return min + 1;
		}
		else
		{
			int currentMax = min;
			for(ClusNode child: children)
			{
				currentMax = getModelWidthVNodes(child, currentMax);				
			}
			return currentMax + 1;
		}
	}
	
	/**
	 * Save model to XML as it is currently displayed. This means, that the user can 
	 * prune the tree by hand. It calls method {@link ClusModel#exportToXML(Document, HashSet)}, 
	 * to get an XML representation of the model, according to the visibility of nodes. 
	 */
	@FXML
	private void saveModelXML()
	{
		FileChooser fileChooser = new FileChooser();
	    fileChooser.setTitle("Save as XML");
	    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML", "*.xml"));
	    File file = fileChooser.showSaveDialog(mainStage);
	    if(file != null)
	    {	    	
	    	HashSet<ClusNode> exportedNodes = new HashSet<ClusNode>();
	    	for(Entry<ClusNode, Boolean> entry: visible.entrySet())
	    	{
	    		if(entry.getValue())
	    		{
	    			exportedNodes.add(entry.getKey());
	    		}
	    	}
	    	//construct the XML file
	    	try 
	    	{
	    		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document document = docBuilder.newDocument();
				Element modelElement = model.exportToXML(document, exportedNodes);
				document.appendChild(modelElement);				
				
				//write the model to selected file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(document);
				StreamResult result = new StreamResult(file);
				transformer.transform(source, result);
			} catch (ParserConfigurationException | TransformerException ex) 
	    	{			
				//catch exceptions and show a message window with the stack trace
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Exception");
				alert.setHeaderText("Exception occured!");
				
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				ex.printStackTrace(pw);
				String exceptionText = sw.toString();
				Label labelMessage = new Label(ex.getMessage());
				Label label = new Label("The exception stacktrace:");

				TextArea textArea = new TextArea(exceptionText);
				textArea.setEditable(false);
				textArea.setWrapText(true);

				textArea.setMaxWidth(Double.MAX_VALUE);
				textArea.setMaxHeight(Double.MAX_VALUE);
				GridPane.setVgrow(textArea, Priority.ALWAYS);
				GridPane.setHgrow(textArea, Priority.ALWAYS);

				GridPane expContent = new GridPane();
				expContent.setMaxWidth(Double.MAX_VALUE);
				expContent.add(labelMessage, 0, 0);
				expContent.add(label, 0, 1);
				expContent.add(textArea, 0, 2);
				
				alert.getDialogPane().setContent(expContent);
				alert.showAndWait();
			}
	    }
	}
	
	/**
	 * Method for saving the whole model that is currently displayed.
	 */
	@FXML
	private void saveModelImage() 
	{
		try {		
			PlotController.saveAsPng(drawingAnchorPane, (Stage) nodesLbl.getScene().getWindow());
		} catch (Exception ex) 
		{
			//catch exceptions and show a message window with the stack trace
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Exception");
			alert.setHeaderText("Exception occured!");
			
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			String exceptionText = sw.toString();
			Label labelMessage = new Label(ex.getMessage());
			Label label = new Label("The exception stacktrace:");

			TextArea textArea = new TextArea(exceptionText);
			textArea.setEditable(false);
			textArea.setWrapText(true);

			textArea.setMaxWidth(Double.MAX_VALUE);
			textArea.setMaxHeight(Double.MAX_VALUE);
			GridPane.setVgrow(textArea, Priority.ALWAYS);
			GridPane.setHgrow(textArea, Priority.ALWAYS);

			GridPane expContent = new GridPane();
			expContent.setMaxWidth(Double.MAX_VALUE);
			expContent.add(labelMessage, 0, 0);
			expContent.add(label, 0, 1);
			expContent.add(textArea, 0, 2);
			
			alert.getDialogPane().setContent(expContent);
			alert.showAndWait();
		}		
	}
}