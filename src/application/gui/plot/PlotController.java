/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/
package application.gui.plot;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import application.gui.MainController;
import application.model.ClusNode;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * This is an abstract class for plot controllers. All plot controllers should
 * extend this class. This is because during initialization of stage for showing 
 * a plot, some specific sub-class needs to be decided (and its parent node set).
 * 
 * @author Nejc Trdin
 *
 */
public abstract class PlotController implements Initializable {

	/**
	 * Some plots need access to the main controller.
	 */
	protected MainController mainController;
	
	/**
	 * To get the stats, the reference to the parent node is supplied.
	 */
	protected ClusNode parentNode;
	
	/**
	 * The initialization of the controller, called by the library.
	 * 
	 * @param location the supplied <code>URL</code> giving the location.
	 * @param resources the <code>ResourceBundle</code> giving the resources during
	 * 			initialization.
	 */
	@Override
	public abstract void initialize(URL location, ResourceBundle resources);
	
	/**
	 * The parent controller is set, before the stage is shown. So that everything
	 * can be set up, before showing the screen.
	 * 
	 * @param parent the parent controller.
	 */
	public void setParentController(MainController parent)
	{
		mainController = parent;
	}
	
	/**
	 * Each controller should set-up everything for showing during this method call.
	 * All handlers should be set-up in this method.
	 * 
	 * @param parent the {@link ClusNode}, which is used for plotting.
	 * 
	 * @throws Exception is thrown if an error occurs.
	 */
	public abstract void setParentNode(ClusNode parent) throws Exception;
	
	/**
	 * A static method that asks for a file name, and saves the supplied node
	 * to a png file.
	 * 
	 * @param node that we want to save as an image.
	 * @param stage at which stage we want to open the file chooser.
	 * 
	 * @throws IOException if there is an exception during output. 
	 */
	public static void saveAsPng(Node node, Stage stage) throws IOException 
	{
	    WritableImage image = node.snapshot(new SnapshotParameters(), null);
	    FileChooser fileChooser = new FileChooser();
	    fileChooser.setTitle("Save as PNG Image");
	    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG", "*.png"));
	    File file = fileChooser.showSaveDialog(stage);
	    if(file!=null)ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);	    
	}
}
