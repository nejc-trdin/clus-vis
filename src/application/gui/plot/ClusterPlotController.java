/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/
package application.gui.plot;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import application.model.ClusNode;
import application.model.statistic.ILevelCStat;

/**
 * This controller controls the statistics view for k-NN models, using the
 * corresponding FXML file (ClusterPlot.fxml). It sub-classes the {@link PlotController}.
 * The implementation of plotting is similar to the {@link ScatterPlotController}, because
 * all combinations of attribute types need to be handled.
 * 
 * @author Nejc Trdin
 *
 */
public class ClusterPlotController extends PlotController implements Initializable {

	/**
	 * The close <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button closeButton;
	
	/**
	 * The save <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button saveButton;
	
	/**
	 * The first attribute that is plotted on X axis. Described in the FXML.
	 */
	@FXML
	private ChoiceBox<String> firstChoice;
	
	/**
	 * The main <code>AnchorPane</code> specified in FXML.
	 */
	@FXML
	private AnchorPane mainAnchor;
	
	/**
	 * The second attribute that is plotted on Y axis. Described in the FXML.
	 */
	@FXML
	private ChoiceBox<String> secondChoice;
	
	/**
	 * The plotting chart type is <code>ScatterChart</code>.
	 */
	private ScatterChart scatterChart;	
	
	/**
	 * The name of the first selected attribute.
	 */
	private String firstSelection;
	
	/**
	 * The index of the second selected attribute.
	 */
	private String secondSelection;
	
	/**
	 * The normalization factor is used to set the scales of the attributes. It is used as
	 * a factor that sets the edges of the plot.
	 */
	private static final int NORMALIZATION = 10;
	
	/**
	 * The statistic that holds the data for plotting.
	 */
	private ILevelCStat statistic;
	
	/**
	 * IDs of clusters, that are decided by Clus.
	 */
	private Set<String> clusterIDs;
	
	/**
	 * A mapping from cluster ID to the number of examples in each cluster.
	 */
	private HashMap<String, Integer> clusterExamplesByID;
	
	/**
	 * <code>Label</code> for number of examples specified in FXML.
	 */
	@FXML
	private Label nExamples;
	
	/**
	 * The initialization of the controller, called by the library. It also adds the listeners
	 * for choosing the attributes from the drop-down menus.
	 * 
	 * @param location the supplied <code>URL</code> giving the location.
	 * @param resources the <code>ResourceBundle</code> giving the resources during
	 * 			initialization.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		firstChoice.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>(){
			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue) {
				firstSelection = newValue;
				if(secondSelection != null)
				{
					constructChart();
				}
			}			
		});
		secondChoice.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>(){
			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue) {
				secondSelection = newValue;		
				if(firstSelection != null)
				{
					constructChart();
				}
			}			
		});				
	}
	
	/**
	 * Sets the parent {@link ClusNode}. Gets the statistic, fills number of examples
	 * label. Also it selects the first target, if it exists and the second target also.
	 * 
	 * @param parent the parent {@link ClusNode}, that holds the statistic.
	 * 
	 * @throws Exception when the supplied statistic is not {@link ILevelCStat}.
	 */
	@Override
	public void setParentNode(ClusNode parent)throws Exception
	{
		parentNode = parent;
		if(!parentNode.isCOPKMeans())
		{
			throw new Exception("Cluster visualization is possible only for COPKMeans model.");
		}
		statistic = (ILevelCStat)parentNode.getStatistic();
		ArrayList<String> clusterTargets = statistic.getClusterAttributes();
		for(String attr: clusterTargets)
		{
			firstChoice.getItems().add(attr);
			secondChoice.getItems().add(attr);
		}
		clusterIDs = statistic.getClusterIDs();
		nExamples.setText(String.valueOf(statistic.getNExamples()));
		clusterExamplesByID = statistic.getClusterExamples();
		if(firstChoice.getItems().size()>0)
		{
			firstChoice.selectionModelProperty().get().select(0);
			if(firstChoice.getItems().size()>1)
			{
				secondChoice.selectionModelProperty().get().select(1);	
			}
			else
			{
				secondChoice.selectionModelProperty().get().select(0);	
			}
		}
	}
	
	/**
	 * Constructs the <code>ScatterChart</code> from the selected <code>firstSelection</code>
	 * and <code>secondSelection</code>. It gets the data from the <code>statistic</code>.
	 * Normalizes the scales with the <code>NORMALIZATION</code> value. And finally it adds the 
	 * data to the chart and puts the chart on the pane. This is a far more complicated method 
	 * than {@link RegressionPlotController#constructChart()}, because it needs to handle all
	 * 4 possible combinations of attributes on two axes (qualitative and numeric attributes).
	 */
	private void constructChart()
	{	
		mainAnchor.getChildren().remove(scatterChart);
		//get data		
		ArrayList<String> firstData = new ArrayList<String>();
		ArrayList<String> secondData = new ArrayList<String>();
		String firstAttribute = firstSelection;
		String secondAttribute = secondSelection;
		ArrayList<String> clusterExamples = new ArrayList<String>();
		//creating clusters, for different series
		for(String id: clusterIDs)
		{			
			HashMap<String,String> cValues = statistic.getClusterValues(id);
			firstData.add(cValues.get(firstSelection));
			secondData.add(cValues.get(secondSelection));
			clusterExamples.add(id);
		}	
		
		//get the minimal and maximal values of the attributes, if they are numeric
		boolean isFNumeric = true;
		double fMin = Double.MAX_VALUE;
		double fMax = Double.MIN_VALUE;		
		for(String value: firstData)
		{
			try
			{
				Double val = Double.parseDouble(value);
				if(val>fMax)
				{
					fMax = val;
				}
				if(val<fMin)
				{
					fMin = val;
				}
			}
			catch (NumberFormatException ex)
			{
				isFNumeric = false;
				break;
			}			
		}
		boolean isSNumeric = true;
		double sMin = Double.MAX_VALUE;
		double sMax = Double.MIN_VALUE;
		for(String value: secondData)
		{
			try
			{
				Double val = Double.parseDouble(value);	
				if(val>sMax)
				{
					sMax = val;
				}
				if(val<sMin)
				{
					sMin = val;
				}
			}
			catch (NumberFormatException ex)
			{
				isSNumeric = false;
				break;
			}			
		}
		
		scatterChart = null;
		if(isFNumeric && isSNumeric)
		{
			//if both are numeric, we can normalize both
			int ifMin = (int)fMin;
			ifMin = ifMin - ifMin%NORMALIZATION;
			int ifMax = (int)Math.ceil(fMax);
			ifMax = ifMax + (NORMALIZATION-(ifMax%NORMALIZATION));
			int isMin = (int)sMin;
			isMin = isMin - isMin%NORMALIZATION;
			int isMax = (int)Math.ceil(sMax);
			isMax = isMax + (NORMALIZATION-(isMax%NORMALIZATION));			
			NumberAxis xAxis = new NumberAxis(ifMin, ifMax, 1);
	        NumberAxis yAxis = new NumberAxis(isMin, isMax, 1);
	        xAxis.setLabel(firstAttribute);
	        yAxis.setLabel(secondAttribute);
	        scatterChart = new ScatterChart(xAxis,yAxis);
	        //we construct multiple series, for each cluster
			HashMap<String, XYChart.Series> series = new HashMap<String, XYChart.Series>(); 
			for(String value: clusterExamples)
			{
				XYChart.Series serie = new XYChart.Series();
				serie.setName(value);
				series.put(value, serie);
			}
			for(int i=0;i<firstData.size();i++)
			{
				String id = clusterExamples.get(i);
				//create data point and add a tooltip
				XYChart.Data data = new XYChart.Data(Double.parseDouble(firstData.get(i)), Double.parseDouble(secondData.get(i)));
				series.get(clusterExamples.get(i)).getData().add(data);	
				data.nodeProperty().addListener(new ChangeListener<Node>() {
	        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
	        		{
	        			if (newNode != null) 
	        			{        				
	        				Tooltip.install(newNode, new Tooltip("Examples: "+String.valueOf(clusterExamplesByID.get(id))));
	        		    }
	        		}
	        	});
			}			
			//add series to chart
			scatterChart.getData().addAll(series.values());						
		}
		else if(isFNumeric)
		{			
			//if first is numeric, we can normalize it
			int ifMin = (int)fMin;
			ifMin = ifMin - ifMin%NORMALIZATION;			
			int ifMax = (int)Math.ceil(fMax);
			ifMax = ifMax + (NORMALIZATION-(ifMax%NORMALIZATION));
			NumberAxis xAxis = new NumberAxis(ifMin, ifMax, 1);
			CategoryAxis yAxis = new CategoryAxis();
			xAxis.setLabel(firstAttribute);
	        yAxis.setLabel(secondAttribute);
			scatterChart = new ScatterChart(xAxis,yAxis);
			//we construct multiple series, for each cluster
			HashMap<String, XYChart.Series> series = new HashMap<String, XYChart.Series>(); 
			for(String value: clusterExamples)
			{
				XYChart.Series serie = new XYChart.Series();
				serie.setName(value);
				series.put(value, serie);
			}
			for(int i=0;i<firstData.size();i++)
			{
				String id = clusterExamples.get(i);
				//create data point and add a tooltip
				XYChart.Data data = new XYChart.Data(Double.parseDouble(firstData.get(i)), secondData.get(i));
				series.get(clusterExamples.get(i)).getData().add(data);
				data.nodeProperty().addListener(new ChangeListener<Node>() {
	        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
	        		{
	        			if (newNode != null) 
	        			{        				
	        				Tooltip.install(newNode, new Tooltip("Examples: "+String.valueOf(clusterExamplesByID.get(id))));
	        		    }
	        		}
	        	});
			}
			//add series to chart
			scatterChart.getData().addAll(series.values());
					
		}
		else if(isSNumeric)
		{
			//if second is numeric, we can normalize it
			int isMin = (int)sMin;
			isMin = isMin - isMin%NORMALIZATION;
			int isMax = (int)Math.ceil(sMax);
			isMax = isMax + (NORMALIZATION-(isMax%NORMALIZATION));
			CategoryAxis xAxis = new CategoryAxis();
	        NumberAxis yAxis = new NumberAxis(isMin, isMax, 1);
	        xAxis.setLabel(firstAttribute);
	        yAxis.setLabel(secondAttribute);
	        scatterChart = new ScatterChart(xAxis,yAxis);
	        //we construct multiple series, for each cluster
			HashMap<String, XYChart.Series> series = new HashMap<String, XYChart.Series>(); 
			for(String value: clusterExamples)
			{
				XYChart.Series serie = new XYChart.Series();
				serie.setName(value);
				series.put(value, serie);
			}
			for(int i=0;i<firstData.size();i++)
			{
				String id = clusterExamples.get(i);
				//create data point and add a tooltip
				XYChart.Data data = new XYChart.Data(firstData.get(i), Double.parseDouble(secondData.get(i)));
				series.get(clusterExamples.get(i)).getData().add(data);
				data.nodeProperty().addListener(new ChangeListener<Node>() {
	        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
	        		{
	        			if (newNode != null) 
	        			{        				
	        				Tooltip.install(newNode, new Tooltip("Examples: "+String.valueOf(clusterExamplesByID.get(id))));
	        		    }
	        		}
	        	});
			}
			//add series to chart
			scatterChart.getData().addAll(series.values());
						
		}
		else
		{
			//if none are numeric, we plot them directly
			CategoryAxis xAxis = new CategoryAxis();
	        CategoryAxis yAxis = new CategoryAxis();
	        scatterChart = new ScatterChart(xAxis,yAxis);		
	        //we construct multiple series, for each cluster
			HashMap<String, XYChart.Series> series = new HashMap<String, XYChart.Series>(); 
			for(String value: clusterExamples)
			{
				XYChart.Series serie = new XYChart.Series();
				serie.setName(value);
				series.put(value, serie);
			}
			for(int i=0;i<firstData.size();i++)
			{
				String id = clusterExamples.get(i);
				//create data point and add a tooltip
				XYChart.Data data = new XYChart.Data(firstData.get(i), secondData.get(i));
				series.get(clusterExamples.get(i)).getData().add(data);
				data.nodeProperty().addListener(new ChangeListener<Node>() {
	        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
	        		{
	        			if (newNode != null) 
	        			{        				
	        				Tooltip.install(newNode, new Tooltip("Examples: "+String.valueOf(clusterExamplesByID.get(id))));
	        		    }
	        		}
	        	});
			}
			//add series to chart
			scatterChart.getData().addAll(series.values());
					
		}				
		scatterChart.setTitle(String.join(" ", new String[]{"Scatter Chart:", firstAttribute, "vs.", secondAttribute}));
		
		//position and add the chart to the pane
		AnchorPane.setBottomAnchor(scatterChart, 34.0);
		AnchorPane.setLeftAnchor(scatterChart, 0.0);
		AnchorPane.setRightAnchor(scatterChart, 0.0);
		AnchorPane.setTopAnchor(scatterChart, 0.0);
		mainAnchor.getChildren().add(scatterChart);	
	}	
	
	/**
	 * This method is specified in the FXML schema, which closes the window.
	 */
	@FXML
	private void closeButtonAction(){	    
	    Stage stage = (Stage) closeButton.getScene().getWindow();
	    stage.close();
	}	
	
	/**
	 * Method for saving the chart that is currently displayed.
	 * 
	 * @throws IOException if an exception occurs during saving an image.
	 */
	@FXML
	private void saveChart() throws IOException
	{
		PlotController.saveAsPng(scatterChart, (Stage) saveButton.getScene().getWindow());
	}
}
