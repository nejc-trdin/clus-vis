/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/
package application.gui.plot;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ResourceBundle;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Transform;
import javafx.stage.Stage;
import application.model.ClusNode;
import application.model.Hierarchy;
import application.model.HierarchyNode;
import application.model.statistic.StatisticImpl;
import application.model.statistic.WHTDStat;

/**
 * This controller controls the statistics view for hierarchy prediction, using the
 * corresponding FXML file (HierarchyPlot.fxml). It sub-classes the {@link PlotController}.
 * 
 * @author Nejc Trdin
 *
 */
public class HierarchyPlotController extends PlotController implements Initializable {

	/**
	 * The main <code>AnchorPane</code> specified in FXML.
	 */
	@FXML
	private AnchorPane mainAnchorPane;
	
	/**
	 * The close <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button closeButton;
	
	/**
	 * The save <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button saveButton;
	
	/**
	 * <code>Label</code> for number of examples specified in FXML.
	 */
	@FXML
	private Label nExamples;
	
	/**
	 * The pane that is used for drawing.
	 */
	private GridPane drawingPane;
	
	/**
	 * The hierarchy that must be displayed for prediction.
	 */
	private Hierarchy hierarchy;
	
	/**
	 * The mapping from each node, to a boolean value, if the node
	 * is visible.
	 */
	private HashMap<HierarchyNode,Boolean> visible = new HashMap<HierarchyNode,Boolean>();
	
	/**
	 * The mapping from each displayed node, to its corresponding label.
	 */
	private HashMap<HierarchyNode,Label> nodeToLabelMap = new HashMap<HierarchyNode,Label>();
	
	/**
	 * This value is set to <code>true</code>, when all the labels have their position set.
	 */
	private boolean isComputedBounds = false;
	
	/**
	 * This is a set of nodes, that have their position computed.
	 */
	private HashSet<HierarchyNode> computedBounds = new HashSet<HierarchyNode>();
	
	/**
	 * This is a mapping from a node, to the line, that ends in this particular node.
	 */
	private HashMap<HierarchyNode,Line> lineEndsIn = new HashMap<HierarchyNode,Line>();

	/**
	 * The binding of x coordinate used for placement of parent nodes, during drawing of edges. 
	 */
	private DoubleBinding parentY;
	
	/**
	 * The binding of y coordinate used for placement of parent nodes, during drawing of edges. 
	 */
	private DoubleBinding parentX;

	/**
	 * The parent node, used during drawing of edges.
	 */
	private HierarchyNode drawingParent;
	
	/**
	 * The initialization of the controller, called by the library.
	 * 
	 * @param location the supplied <code>URL</code> giving the location.
	 * @param resources the <code>ResourceBundle</code> giving the resources during
	 * 			initialization.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources){}

	/**
	 * This method is specified in the FXML schema, which closes the window.
	 */
	@FXML
	private void closeButtonAction(){	    
	    Stage stage = (Stage) closeButton.getScene().getWindow();
	    stage.close();
	}
	
	/**
	 * Sets the parent {@link ClusNode}. Gets the statistic and fills number of examples
	 * label.
	 * 
	 * @param parent the parent {@link ClusNode}, that holds the statistic.
	 * 
	 * @throws Exception when the supplied statistic is not {@link WHTDStat}.
	 */
	@Override
	public void setParentNode(ClusNode parent) throws Exception
	{
		parentNode = parent;
		StatisticImpl s = parentNode.getStatistic();
		if(s instanceof WHTDStat)
		{
			WHTDStat statistic = (WHTDStat)s;
			nExamples.setText(String.valueOf(statistic.getNExamples()));
			//get the hierarchy and compute the width and depth
			hierarchy = statistic.getDisplayHierarchy();
			int depth = getHierarchyDepth();
			int width = getHierarchyWidth();
			
			//create the drawing pane, on which the nodes are positioned
			drawingPane = new GridPane();
			drawingPane.setHgap(5.0);
			drawingPane.setVgap(5.0);
			
			//main anchor pane holds the edges of the hierarchy
			mainAnchorPane.getChildren().clear();
			mainAnchorPane.getChildren().add(drawingPane);
			AnchorPane.setTopAnchor(drawingPane, 10.0);
			AnchorPane.setBottomAnchor(drawingPane, 10.0);
			AnchorPane.setLeftAnchor(drawingPane, 10.0);
			AnchorPane.setRightAnchor(drawingPane, 10.0);
			//initialize space for columns and rows
			for(int i=0;i<width;i++)
			{
				ColumnConstraints column = new ColumnConstraints();
				column.setHgrow(Priority.ALWAYS);								
				drawingPane.getColumnConstraints().add(column);
			}
			for(int i=0;i<depth;i++)
			{
				RowConstraints row = new RowConstraints();
				row.setVgrow(Priority.ALWAYS);				
				drawingPane.getRowConstraints().add(row);
			}			
			//draw the hierarchy
			drawHierarchy();
		}
		else
		{
			throw new Exception(String.join(" ", new String[]{
					"This type of statistic cannot be modeled with Hierarchy chart:", 
					s.getStatisticName()}));
		}
	}	
	
	/**
	 * Recursively shows all sub-nodes (and corresponding edges) and this node, 
	 * if they are visible according to the <code>visible</code> mapping.
	 * 
	 * @param node the node which needs to be shown.
	 */
	private void showAllVisibleSubNodes(HierarchyNode node)
	{
		if(visible.get(node))
		{
			if(lineEndsIn.containsKey(node))
			{
				lineEndsIn.get(node).setVisible(true);	
			}
			nodeToLabelMap.get(node).setVisible(true);					
			for(HierarchyNode child: node.getChildren())
			{
				showAllVisibleSubNodes(child);
			}
		}
	}
	
	/**
	 * Recursively hides all sub-nodes (and corresponding edges) and this node.
	 * 
	 * @param node the node which needs to be hidden.
	 */
	private void hideAllSubNodes(HierarchyNode node)
	{
		lineEndsIn.get(node).setVisible(false);		
		nodeToLabelMap.get(node).setVisible(false);		
		for(HierarchyNode child: node.getChildren())
		{
			hideAllSubNodes(child);
		}
	}
	
	/**
	 * Creates a label for the currently drawn node at the specified position. If the node
	 * has children, it also registers a mouse listener (for double click).
	 * 
	 * @param node the node currently being drawn.
	 * @param depth the specified depth, at which the node should be position. This is directly
	 * 			translated to the row of the <code>drawingPane</code>. 
	 * @param width the specified width, at which the node should be position. This is directly
	 * 			translated to the column of the <code>drawingPane</code>.
	 */
	private void drawAndInitNode(HierarchyNode node, int depth, int width)
	{
		Label lbl;
		if(node.getChildren().size()==0)
		{		
			//create the node label and a tooltip
			String labelS = node.getName()+(node.getWeight()!=null?"\n"+node.getWeight():"");
			lbl = new Label(labelS);
			lbl.setTooltip(new Tooltip(labelS));
		}
		else
		{
			//create the node label and a tooltip
			String labelS = node.getName()+(node.getWeight()!=null?"\n"+node.getWeight():"");
			lbl = new Label(labelS);
			lbl.setTooltip(new Tooltip(labelS));
			
			//handling double click event
			lbl.setOnMouseClicked(new EventHandler<MouseEvent>(){		
				public void handle(MouseEvent event) {
					if(event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2)
					{
						ArrayList<HierarchyNode> children = node.getChildren();						
						boolean allHidden = true;
						//we need to determine if all children are hidden
						for(HierarchyNode child: children)
						{
							allHidden &= !visible.get(child);
						}						
						if(allHidden)
						{
							//all children are hidden, we make them visible
							for(HierarchyNode child: children)
							{
								visible.put(child, true);									
							}
							showAllVisibleSubNodes(node);
						}
						else
						{
							//at least one child is visible, we make all hidden							
							for(HierarchyNode child: children)
							{
								visible.put(child, false);
								hideAllSubNodes(child);
							}							
						}						
					}
				}			
			});
		}		
		//set the node as visible
		visible.put(node, true);
		//put a mapping to get the corresponding label from the node reference
		nodeToLabelMap.put(node, lbl);
		//add the label to the drawingPane
		drawingPane.add(lbl, width, depth);	
		
		//an instance of this listener is attached to each label
		//when all the labels have their position computed, we can also compute the
		//position of the connecting edges
		lbl.localToParentTransformProperty().addListener(new ChangeListener<Transform>(){			
			@Override
			public void changed(
					ObservableValue<? extends Transform> observable,
					Transform oldValue, Transform newValue) 
			{
				computedBounds.add(node);
				//we want to compute the bounds only once
				if(!isComputedBounds && computedBounds.size()==nodeToLabelMap.size())
				{						
					isComputedBounds = true;					
					drawHierarchyEdges();					
				}				
			}
		});		
		
		//label properties
		lbl.setTextAlignment(TextAlignment.CENTER);
		lbl.setStyle("-fx-background-color: white;");
		lbl.setAlignment(Pos.CENTER);	
		lbl.setMinWidth(100);		
		lbl.setMaxWidth(200);
		
		//growing properties of the label
		HBox.setHgrow(lbl, Priority.ALWAYS);
		VBox.setVgrow(lbl, Priority.ALWAYS);		
	}
	
	/**
	 * When all the nodes have a position on the drawing pane, this 
	 * method is called, which constructs the edges between nodes.
	 * Edges are drawn, by keeping track of the class defined parent of the
	 * currently being drawn child. Also see {@link #drawEdges(HierarchyNode)}.
	 */
	private void drawHierarchyEdges()
	{
		for(HierarchyNode root: hierarchy.getRoots())
		{
			drawingParent = root;	
			Label label = nodeToLabelMap.get(root);
			parentX = label.layoutXProperty().add(label.widthProperty().divide(2.0));
			parentY = label.layoutYProperty().add(label.heightProperty().divide(2.0));			
			for(HierarchyNode child: root.getChildren())
			{
				drawEdges(child);				
			}
		}
		drawingParent = null;
		parentX = null;
		parentY = null;
	}
	
	/**
	 * Edges are drawn, by keeping track of the class defined parent of the
	 * currently being drawn child. The position of the edge are computed
	 * from the position of the parent and the currently being drawn node.
	 * The method is called for all children of the node, updating the parent
	 * accordingly.
	 * 
	 * @param node that is being drawn.
	 */
	private void drawEdges(HierarchyNode node)
	{
		Label label = nodeToLabelMap.get(node);			
		//save data
		DoubleBinding oldX = parentX;
		DoubleBinding oldY = parentY;
		HierarchyNode oldParent = drawingParent;
		drawingParent = node;
		//get new parent
		parentX = label.layoutXProperty().add(label.widthProperty().divide(2.0));
		parentY = label.layoutYProperty().add(label.heightProperty().divide(2.0));
				
		//create line
		Line line = new Line();
		mainAnchorPane.getChildren().add(0,line);			
		line.startXProperty().bind(oldX);
		line.startYProperty().bind(oldY);			
		line.endXProperty().bind(parentX);
		line.endYProperty().bind(parentY);
		lineEndsIn.put(node, line);			
		
		//create nodes below
		for(HierarchyNode child: node.getChildren())
		{
			drawEdges(child);				
		}		
		//establish previous state
		parentX = oldX;
		parentY = oldY;
		drawingParent = oldParent;
	}
	
	/**
	 * Used for drawing the hierarchy. It can be called only after,
	 * the plot has been initialized. It draws the hierarchy from the
	 * hierarchies roots, keeping track of the width of the left-most
	 * drawn node. Also see {@link #drawNode(HierarchyNode, int, int)}.
	 */
	private void drawHierarchy()
	{
		int currentMax = 0;
		for(HierarchyNode root: hierarchy.getRoots())
		{
			currentMax = drawNode(root, 0, currentMax);	
		}				
	}
	
	/**
	 * Used for drawing the hierarchy. It can be called only after,
	 * the plot has been initialized. It draws the hierarchy from the
	 * nodes children, keeping track of the width of the left-most drawn
	 * node. Also see {@link #drawAndInitNode(HierarchyNode, int, int)}.
	 *	 
	 * @param node the hierarchy node, which is being drawn.
	 * @param depth the depth at which the node should be positioned.
	 * @param width the width at which the node should be positioned.
	 * 
	 * @return the right-most drawn node's width.
	 */
	private int drawNode(HierarchyNode node, int depth, int width)
	{
		ArrayList<HierarchyNode> children = node.getChildren();
		if(children.size()==0)
		{
			drawAndInitNode(node, depth, width);
			return width + 2;
		}
		else
		{
			int currentMax = width;
			for(HierarchyNode child: children)
			{
				currentMax = drawNode(child, depth + 1, currentMax);				
			}
			int parentW = (width+currentMax-2)/2;
			drawAndInitNode(node, depth, parentW);
			return currentMax;
		}
	}
	
	/**
	 * Gets the hierarchies depth, by computing the maximal depth
	 * over all hierarchie's roots.
	 * 
	 * @return the hierarchy depth.
	 */
	private int getHierarchyDepth()
	{
		int maximum = Integer.MIN_VALUE;
		for(HierarchyNode root: hierarchy.getRoots())
		{
			int depth = getHierarchyDepthVNodes(root);
			if(depth > maximum)
			{
				maximum = depth;
			}
		}
		return maximum;
	}
	
	/**
	 * Gets the depth of the hierarchy from this node onwards, by
	 * computing the maximal value of the hierarchy depth over nodes
	 * children. If the node is a leaf, 1 is returned.
	 * 
	 * @param node the {@link HierarchyNode} for which the depth is queried.
	 * 
	 * @return the maximal depth of the hierarchy from this node.
	 */
	private int getHierarchyDepthVNodes(HierarchyNode node)
	{
		ArrayList<HierarchyNode> children = node.getChildren();
		if(children.size()==0)
		{
			return 1;
		}
		else
		{
			int maximum = Integer.MIN_VALUE;
			for(HierarchyNode child: children)
			{
				int childDepth = getHierarchyDepthVNodes(child);
				if(childDepth > maximum)
				{
					maximum = childDepth;
				}
			}
			return maximum + 1;
		}
	}
	
	/**
	 * Gets the width of the hierarchy, so that each node in a column
	 * is separated by one column. It does it by successively calling
	 * {@link #getHierarchyWidthVNodes(HierarchyNode, int)} on hierarchies
	 * roots.
	 * 
	 * @return the width of the hierarchy.
	 */
	private int getHierarchyWidth()
	{
		int maximum = 0;
		for(HierarchyNode root: hierarchy.getRoots())
		{
			maximum = getHierarchyWidthVNodes(root, maximum);			
		}
		return maximum+1;		
	}
	
	/**
	 * Recursively gets the width of the hierarchy, so that each node in
	 * a column is separated by one column. If this is a hierarchical leaf
	 * <code>min+1</code> is returned. Otherwise the values are recursively
	 * computed with the invariant, changing the value of <code>min</code>.
	 * 
	 * @param node the {@link HierarchyNode} for which we would like to get
	 * 			the width.
	 * @param min the leftmost position of the parent's most right child.
	 * 
	 * @return the width of the hierarchy below this node.
	 */
	private int getHierarchyWidthVNodes(HierarchyNode node, int min)
	{
		ArrayList<HierarchyNode> children = node.getChildren();
		if(children.size()==0)
		{
			return min + 1;
		}
		else
		{
			int currentMax = min;
			for(HierarchyNode child: children)
			{
				currentMax = getHierarchyWidthVNodes(child, currentMax);				
			}
			return currentMax + 1;
		}
	}
	
	/**
	 * Method for saving the chart that is currently displayed.
	 * 
	 * @throws IOException if an exception occurs during saving an image.
	 */
	@FXML
	private void saveChart() throws IOException
	{
		PlotController.saveAsPng(mainAnchorPane, (Stage) saveButton.getScene().getWindow());
	}
}