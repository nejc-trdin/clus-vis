/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/
package application.gui.plot;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import application.ClusVis;
import application.model.ClusNode;
import application.model.statistic.ClassificationStat;
import application.model.statistic.CombStat;
import application.model.statistic.RegressionStat;
import application.model.statistic.StatisticImpl;

/**
 * This controller controls the statistics view for the combination of classification 
 * and regression, using the corresponding FXML file (CombStatPlot.fxml). It sub-classes 
 * the {@link PlotController}.
 * 
 * @author Nejc Trdin
 *
 */
public class CombStatPlotController extends PlotController implements Initializable {

	/**
	 * The main <code>AnchorPane</code> specified in FXML.
	 */
	@FXML
	private AnchorPane mainAnchor;
	
	/**
	 * The close <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button closeButton;	
	
	/**
	 * The <code>Button</code> specified in FXML, for displaying the classification 
	 * statistic.
	 */
	@FXML
	private Button classButton;
	
	/**
	 * The <code>Button</code> specified in FXML, for displaying the regression 
	 * statistic.
	 */
	@FXML
	private Button regButton;
	
	/**
	 * <code>Label</code> for number of examples specified in FXML.
	 */
	@FXML
	private Label nExamples;	 
	
	/**
	 * The initialization of the controller, called by the library.
	 * 
	 * @param location the supplied <code>URL</code> giving the location.
	 * @param resources the <code>ResourceBundle</code> giving the resources during
	 * 			initialization.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources){}

	/**
	 * This method is specified in the FXML schema, which closes the window.
	 */
	@FXML
	private void closeButtonAction(){	    
	    Stage stage = (Stage) closeButton.getScene().getWindow();
	    stage.close();
	}
	
	/**
	 * Method checks from the parent if the supplied statistic is expected 
	 * ({@link CombStat}). Fills the label for number of examples and
	 * attaches listeners to the classification and regression buttons.
	 * 
	 * @param parent the {@link ClusNode}, which is used for plotting and holds the
	 * 			statistic.
	 * 
	 * @throws Exception is thrown if the expected statistic is not 
	 * 			{@link CombStat}.
	 */
	@Override
	public void setParentNode(ClusNode parent) throws Exception
	{
		parentNode = parent;
		StatisticImpl s = parentNode.getStatistic();
		if(s instanceof CombStat)
		{
			CombStat statistic = (CombStat)s;
			
			//set number of examples
			nExamples.setText(String.valueOf(statistic.getNExamples()));
			
			//getting specified statistics
			RegressionStat regStat = statistic.getRegression();
			ClassificationStat classStat = statistic.getClassification();
			
			//adding a handler for events on classification button
			//it merely opens a new stage with regression statistic and catches exceptions
			classButton.setOnAction(new EventHandler<ActionEvent>(){
					@Override
					public void handle(ActionEvent event) {
						try 
						{
							FXMLLoader loader = new FXMLLoader(ClusVis.class.getResource(classStat.getStatisticFXML()));			
							AnchorPane pane = loader.load();
							PlotController controller = (PlotController)loader.getController();							
							controller.setParentNode(parentNode);
							Stage stage = new Stage();
							Scene scene = new Scene(pane);
							stage.setTitle(classStat.getStatisticName());
							stage.setScene(scene);
							stage.initModality(Modality.APPLICATION_MODAL);								
							stage.show();	

						} catch (Exception ex) 
						{
							Alert alert = new Alert(AlertType.ERROR);
							alert.setTitle("Exception");
							alert.setHeaderText("Exception occured!");
							
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							ex.printStackTrace(pw);
							String exceptionText = sw.toString();
							Label labelMessage = new Label(ex.getMessage());
							Label label = new Label("The exception stacktrace:");

							TextArea textArea = new TextArea(exceptionText);
							textArea.setEditable(false);
							textArea.setWrapText(true);

							textArea.setMaxWidth(Double.MAX_VALUE);
							textArea.setMaxHeight(Double.MAX_VALUE);
							GridPane.setVgrow(textArea, Priority.ALWAYS);
							GridPane.setHgrow(textArea, Priority.ALWAYS);

							GridPane expContent = new GridPane();
							expContent.setMaxWidth(Double.MAX_VALUE);
							expContent.add(labelMessage, 0, 0);
							expContent.add(label, 0, 1);
							expContent.add(textArea, 0, 2);
							
							alert.getDialogPane().setContent(expContent);
							alert.showAndWait();
						}
					}
			});
			
			//adding a handler for events on regression button
			//it merely opens a new stage with regression statistic and catches exceptions
			regButton.setOnAction(new EventHandler<ActionEvent>(){
				@Override
				public void handle(ActionEvent event) {
					try 
					{
						FXMLLoader loader = new FXMLLoader(ClusVis.class.getResource(regStat.getStatisticFXML()));			
						AnchorPane pane = loader.load();
						PlotController controller = (PlotController)loader.getController();							
						controller.setParentNode(parentNode);
						Stage stage = new Stage();
						Scene scene = new Scene(pane);
						stage.setTitle(regStat.getStatisticName());
						stage.setScene(scene);
						stage.initModality(Modality.APPLICATION_MODAL);								
						stage.show();	

					} catch (Exception ex) 
					{
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Exception");
						alert.setHeaderText("Exception occured!");
						
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						ex.printStackTrace(pw);
						String exceptionText = sw.toString();
						Label labelMessage = new Label(ex.getMessage());
						Label label = new Label("The exception stacktrace:");

						TextArea textArea = new TextArea(exceptionText);
						textArea.setEditable(false);
						textArea.setWrapText(true);

						textArea.setMaxWidth(Double.MAX_VALUE);
						textArea.setMaxHeight(Double.MAX_VALUE);
						GridPane.setVgrow(textArea, Priority.ALWAYS);
						GridPane.setHgrow(textArea, Priority.ALWAYS);

						GridPane expContent = new GridPane();
						expContent.setMaxWidth(Double.MAX_VALUE);
						expContent.add(labelMessage, 0, 0);
						expContent.add(label, 0, 1);
						expContent.add(textArea, 0, 2);
						
						alert.getDialogPane().setContent(expContent);
						alert.showAndWait();
					}
				}
		});
		}
		else
		{
			throw new Exception(String.join(" ", new String[]{
					"This type of statistic cannot be modeled with combined chart:", 
					s.getStatisticName()}));
		}
	}
}
