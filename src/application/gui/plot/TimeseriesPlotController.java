/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/
package application.gui.plot;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import application.model.ClusNode;
import application.model.statistic.StatisticImpl;
import application.model.statistic.TimeseriesStat;

/**
 * This controller controls the statistics view for timeseries prediction, using the
 * corresponding FXML file (TimeseriesPlot.fxml). It sub-classes the {@link PlotController}.
 * 
 * @author Nejc Trdin
 *
 */
public class TimeseriesPlotController extends PlotController implements Initializable {

	/**
	 * The main <code>AnchorPane</code> specified in FXML.
	 */
	@FXML
	private AnchorPane mainAnchor;
	
	/**
	 * The close <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button closeButton;
	
	/**
	 * The save <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button saveButton;
	
	/**
	 * <code>Label</code> for number of examples specified in FXML.
	 */
	@FXML
	private Label nExamples;
	
	/**
	 * <code>Label</code> for average distance of examples from median specified in FXML.
	 */
	@FXML
	private Label avgDist;
	
	/**
	 * <code>LineChart</code> which is used for plotting the timeseries.
	 */
	private LineChart<Number,Number> lc; 
	
	/**
	 * The initialization of the controller, called by the library.
	 * 
	 * @param location the supplied <code>URL</code> giving the location.
	 * @param resources the <code>ResourceBundle</code> giving the resources during
	 * 			initialization.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources){}

	/**
	 * This method is specified in the FXML schema, which closes the window.
	 */
	@FXML
	private void closeButtonAction(){	    
	    Stage stage = (Stage) closeButton.getScene().getWindow();	    
	    stage.close();
	}
	
	/**
	 * Method checks from the parent if the supplied statistic is expected 
	 * ({@link TimeseriesStat}). Fills the label for number of examples and
	 * average distance from the median. Then it calls {@link #constructChart()}.
	 * 
	 * @param parent the {@link ClusNode}, which is used for plotting and holds the
	 * 			statistic.
	 * 
	 * @throws Exception is thrown if the expected statistic is not 
	 * 			{@link TimeseriesStat}.
	 */
	@Override
	public void setParentNode(ClusNode parent) throws Exception
	{
		parentNode = parent;
		StatisticImpl s = parentNode.getStatistic();
		if(s instanceof TimeseriesStat)
		{
			TimeseriesStat statistic = (TimeseriesStat)s;
			nExamples.setText(String.valueOf(statistic.getNExamples()));	
			double avgDistance = statistic.getAvgDistance();
			avgDist.setText(String.valueOf(avgDistance));
			constructChart();
		}
		else
		{
			throw new Exception(String.join(" ", new String[]{
					"This type of statistic cannot be modeled with Line chart:", 
					s.getStatisticName()}));
		}
	}
	
	/**
	 * Constructs the chart for viewing the timeseries prediction. It first removes an
	 * existing line chart from the <code>mainAnchorPane</code>. It gets the statistic,
	 * constructs two new series of data (mean and median) and plots them. Also adds
	 * examples to the plot. Finally it places the <code>LineChart</code> on the 
	 * <code>mainAnchorPane</code>.
	 */
	private void constructChart()
	{
		if(lc != null) mainAnchor.getChildren().remove(lc);
		TimeseriesStat statistic = (TimeseriesStat)parentNode.getStatistic();		
		NumberAxis xAxis = new NumberAxis();
		NumberAxis yAxis = new NumberAxis();
		lc = new LineChart<Number,Number>(xAxis,yAxis);
		lc.setTitle("Timeseries prediction");
		xAxis.setLabel("Timestamp");       
        yAxis.setLabel("Value");
        Series<Number, Number> meanSeries = new XYChart.Series<Number, Number>();
        meanSeries.setName("Mean");
        int i = 0;
        
        //creation of data-points for mean
        for(Double value: statistic.getMean())
        {
        	//a datapoint is specified with the timestamp (i) and value
        	XYChart.Data<Number, Number> data = new XYChart.Data<Number, Number>(i, value);
        	meanSeries.getData().add(data);        	        	
        	i++;
        	
        	//for larger timeseries scales, tooltips are useful
        	data.nodeProperty().addListener(new ChangeListener<Node>() {
        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
        		{
        			if (newNode != null) 
        			{        				
        				Tooltip.install(newNode, new Tooltip(String.valueOf(value)));
        		    }
        		}
        	});
        }        
        
        //we add mean to the plot
        lc.getData().add(meanSeries);
        
        Series<Number, Number> medianSeries = new XYChart.Series<Number, Number>();       
        medianSeries.setName("Median");
        i=0;
        
        //creation of data-points for median
        for(Double value: statistic.getMedian())
        {
        	//a datapoint is specified with the timestamp (i) and value
        	XYChart.Data<Number, Number> data = new XYChart.Data<Number, Number>(i, value);        	
        	medianSeries.getData().add(data); 
        	i++;
        	
        	//for larger timeseries scales, tooltips are useful
        	data.nodeProperty().addListener(new ChangeListener<Node>() {
        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
        		{
        			if (newNode != null) 
        			{        				
        				Tooltip.install(newNode, new Tooltip(String.valueOf(value)));
        		    }
        		}
        	});
        }            
        
        //we add median to the plot
        lc.getData().add(medianSeries); 
        
        ArrayList<Integer> classes = parentNode.getExamples().getClassIDAttributes();
        if(classes.size()==1)
        {
        	int classAttr = classes.get(0);   
        	boolean incorrectClassAttr = false;
        	//clus sometimes incorrectly reports, the class attribute, that is why we need to do 
        	//the manual extraction of timeseries
        	ArrayList<ArrayList<String>> exampleValues = parentNode.getExamples().getExampleValues();
        	ArrayList<ArrayList<Double>> timeseries = new ArrayList<ArrayList<Double>>();
        	for(ArrayList<String> example: exampleValues)
        	{
        		String ts = example.get(classAttr);
        		if(ts.contains("["))
        		{
        			ts = ts.replace("[", "").replace("]", "");
        			String[] ts_split = ts.split(",");
        			ArrayList<Double> one_ts = new ArrayList<Double>();
        			for(String value: ts_split)
        			{
        				one_ts.add(Double.parseDouble(value));
        			}
        			timeseries.add(one_ts);
        		}
        		else
        		{
        			incorrectClassAttr = true;
        			break;
        		}
        	}
        	if(incorrectClassAttr)
        	{
        		timeseries.clear();
        		classAttr = -1;
        		if(exampleValues.size()>0)
        		{
        			ArrayList<String> example = exampleValues.get(0);
        			int counter = 0;
        			for(String value: example)
        			{
        				if(value.contains("["))
        				{
        					classAttr = counter;
        					break;
        				}
        				counter++;
        			}
        		}
        		if(classAttr != -1)
        		{
        			for(ArrayList<String> example: exampleValues)
                	{
                		String ts = example.get(classAttr);
                		if(ts.contains("["))
                		{
                			ts = ts.replace("[", "").replace("]", "");
                			String[] ts_split = ts.split(",");
                			ArrayList<Double> one_ts = new ArrayList<Double>();
                			for(String value: ts_split)
                			{
                				one_ts.add(Double.parseDouble(value));
                			}
                			timeseries.add(one_ts);
                		}                		
                	}
        		}
        	}
        	
        	//here timeseries is filled with values for timeseries  
        	int exampleC = 1;
        	for(ArrayList<Double> ts: timeseries)
        	{
        		Series<Number, Number> example = new XYChart.Series<Number, Number>();
        		example.setName("Example"+exampleC);
        		exampleC++;
        		i=0;
        		for(Double value: ts)
        		{
        			//a datapoint is specified with the timestamp (i) and value
                	XYChart.Data<Number, Number> data = new XYChart.Data<Number, Number>(i, value);        	
                	example.getData().add(data); 
                	i++;
                	
                	//for larger timeseries scales, tooltips are useful
                	data.nodeProperty().addListener(new ChangeListener<Node>() {
                		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
                		{
                			if (newNode != null) 
                			{        				
                				newNode.setStyle(".series0 { -fx-background-color: #860061, white;"
                						+ " -fx-stroke: #ff5700;}");
                				Tooltip.install(newNode, new Tooltip(String.valueOf(value)));
                		    }
                		}
                	});
        		} 
        		lc.getData().add(example);
        	}
        }
        
        //adding plot to the main anchor
        mainAnchor.getChildren().add(lc);
        		
        //placement of the line chart on the main pane
        //this should not be changed, because it is optimally defined to be stretched
		AnchorPane.setBottomAnchor(lc, 34.0);
		AnchorPane.setLeftAnchor(lc, 0.0);
		AnchorPane.setRightAnchor(lc, 0.0);
		AnchorPane.setTopAnchor(lc, 0.0);
	}	
	
	/**
	 * Method for saving the chart that is currently displayed.
	 * 
	 * @throws IOException if an exception occurs during saving an image.
	 */
	@FXML
	private void saveChart() throws IOException
	{
		PlotController.saveAsPng(lc, (Stage) saveButton.getScene().getWindow());
	}
}
