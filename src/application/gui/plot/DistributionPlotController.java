/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/
package application.gui.plot;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * This controller is used for displaying the statistics of attributes from examples.
 * Particularly, for classification, which displays a bar chart with frequency of occured
 * attribute values.
 * 
 * @author Nejc Trdin
 *
 */
public class DistributionPlotController implements Initializable {

	/**
	 * The main <code>AnchorPane</code> specified in FXML.
	 */
	@FXML
	private AnchorPane mainAnchor;
	
	/**
	 * The close <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button closeButton;	
	
	/**
	 * The save <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button saveButton;
	
	/**
	 * <code>Label</code> for attribute name, specified in FXML.
	 */
	@FXML
	private Label attrName;
	
	/**
	 * The <code>BarChart</code> used for displaying 
	 */
	private BarChart<String,Number> bc; 
	
	/**
	 * The initialization of the controller, called by the library.
	 * 
	 * @param location the supplied <code>URL</code> giving the location.
	 * @param resources the <code>ResourceBundle</code> giving the resources during
	 * 			initialization.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources){}

	/**
	 * This method is specified in the FXML schema, which closes the window.
	 */
	@FXML
	private void closeButtonAction(){	    
	    Stage stage = (Stage) closeButton.getScene().getWindow();
	    stage.close();
	}
	
	/**
	 * Supplied with the distribution of values and the attribute name, we clean
	 * the data (removing "?" occurence) and finding the majority value. Finally the
	 * chart is constructed with {@link #constructChart(HashMap, String, String)}.
	 * The method also sets the <code>attrName</code> label to the attribute's name.
	 * The method is called by {@link application.gui.ExampleViewController#updateStats(int)}.
	 * 
	 * @param distribution a mapping from String to Integer, representing the distribution 
	 * 			of values.
	 * @param attributeName the name of the attribute considered.
	 */
	public void setArgs(HashMap<String,Integer> distribution, String attributeName)
	{
		attrName.setText(attributeName);
		if(distribution.get("?")==0)
		{
			distribution.remove("?");
		}
		String key = "";
		int max = -1;
		for(Entry<String,Integer> entry: distribution.entrySet())
		{
			if(entry.getValue() > max)
			{
				max = entry.getValue();
				key = entry.getKey();
			}
		}		
		constructChart(distribution, key, attributeName);
	}
	
	/**
	 * The method constructs a new <code>BarChart</code> which is created from the supplied 
	 * distribution. It also sets the color of the majority value to red, and all others to blue.
	 *  
	 * @param distribution a mapping from String to Integer, representing the distribution 
	 * 			of values.
	 * @param majority the value which is the majority in the distribution.
	 * @param selectedTarget the name of the attribute which is being plotted.
	 */
	private void constructChart(HashMap<String, Integer> distribution, String majority, String selectedTarget)
	{			
		CategoryAxis xAxis = new CategoryAxis();
		NumberAxis yAxis = new NumberAxis();
		bc = new BarChart<String,Number>(xAxis,yAxis);
		bc.setTitle(String.join("", new String[]{"Target: ", selectedTarget}));
		xAxis.setLabel("Value");       
        yAxis.setLabel("Examples");
        
        //creating a series for plotting
        Series<String, Number> series = new XYChart.Series<String, Number>();
        for(Entry<String, Integer> entry: distribution.entrySet())
        {
        	XYChart.Data<String, Number> data = new XYChart.Data<String, Number>(entry.getKey(), entry.getValue());
        	series.getData().add(data);
        	
        	//installing a tooltip and coloring the bars
        	data.nodeProperty().addListener(new ChangeListener<Node>() {
        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
        		{      			       
        			Tooltip.install(newNode, new Tooltip(String.valueOf(entry.getValue())));
        			if (data.getXValue().equals(majority)) 
        			{
        				newNode.setStyle("-fx-bar-fill: firebrick;");
        			}
        			else
        			{
        				newNode.setStyle("-fx-bar-fill: navy;");
        			}  
        		}        		
        	});        	
        }
        //adding the series to the chart
        bc.getData().add(series);
        //adding the chart to the mainAnchor
        mainAnchor.getChildren().add(bc);
        //disabling legend view
        bc.setLegendVisible(false);
        
        //positioning the plot on the pane
		AnchorPane.setBottomAnchor(bc, 34.0);
		AnchorPane.setLeftAnchor(bc, 0.0);
		AnchorPane.setRightAnchor(bc, 0.0);
		AnchorPane.setTopAnchor(bc, 0.0);
	}	
	
	/**
	 * Method for saving the chart that is currently displayed.
	 * 
	 * @throws IOException if an exception occurs during saving an image.
	 */
	@FXML
	private void saveChart() throws IOException
	{
		PlotController.saveAsPng(bc, (Stage) saveButton.getScene().getWindow());
	}
}