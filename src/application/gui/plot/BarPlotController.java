/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/
package application.gui.plot;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import application.model.ClusNode;
import application.model.statistic.ClassificationStat;
import application.model.statistic.CombStat;
import application.model.statistic.StatisticImpl;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * This controller controls the statistics view for classification, using the
 * corresponding FXML file (BarPlot.fxml). It sub-classes the {@link PlotController}.
 * 
 * @author Nejc Trdin
 *
 */
public class BarPlotController extends PlotController implements Initializable {

	/**
	 * The main <code>AnchorPane</code> specified in FXML.
	 */
	@FXML
	private AnchorPane mainAnchor;
	
	/**
	 * The close <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button closeButton;
	
	/**
	 * The save <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button saveButton;
	
	/**
	 * A <code>ChoiceBox</code> for selection of the target attribute.
	 */
	@FXML
	private ChoiceBox<String> targetField;
	
	/**
	 * <code>Label</code> for number of examples specified in FXML.
	 */
	@FXML
	private Label nExamples;
	
	/**
	 * Bar chart, where the distribution is displayed.
	 */
	private BarChart<String,Number> bc; 
		
	/**
	 * The target that is currently selected in the drop-down menu.
	 */
	private String selectedTarget;
	
	/**
	 * The initialization of the controller, called by the library.
	 * 
	 * @param location the supplied <code>URL</code> giving the location.
	 * @param resources the <code>ResourceBundle</code> giving the resources during
	 * 			initialization.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources){}

	/**
	 * This method is specified in the FXML schema, which closes the window.
	 */
	@FXML
	private void closeButtonAction(){	    
	    Stage stage = (Stage) closeButton.getScene().getWindow();
	    stage.close();
	}	
	
	/**
	 * Sets the parent {@link ClusNode}. Gets the statistic, fills number of examples
	 * label and adds a listener to the drop-down for change of state. Also it selects
	 * the first target, if it exists. The method can also handle the case, when the
	 * statistic is {@link CombStat}.
	 * 
	 * @param parent the parent {@link ClusNode}, that holds the statistic.
	 * 
	 * @throws Exception when the supplied statistic is not {@link ClassificationStat} or
	 * 			{@link CombStat}.
	 */
	@Override
	public void setParentNode(ClusNode parent) throws Exception
	{
		parentNode = parent;
		StatisticImpl s = parentNode.getStatistic();
		if(s instanceof ClassificationStat)
		{
			ClassificationStat statistic = (ClassificationStat)s;
			nExamples.setText(String.valueOf(statistic.getNExamples()));
			ArrayList<String> targets = statistic.getTargets();
			targetField.getItems().addAll(targets);		
			targetField.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>(){
				@Override
				public void changed(ObservableValue<? extends String> observable,
						String oldValue, String newValue) {
					selectedTarget = newValue;
					constructChart();
				}
				
			});
			if(targets.size()>0)
			{
				targetField.selectionModelProperty().get().select(0);	
			}			
		}
		else if(s instanceof CombStat)
		{
			ClassificationStat statistic = ((CombStat)s).getClassification();
			nExamples.setText(String.valueOf(statistic.getNExamples()));
			ArrayList<String> targets = statistic.getTargets();
			targetField.getItems().addAll(targets);		
			targetField.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>(){
				@Override
				public void changed(ObservableValue<? extends String> observable,
						String oldValue, String newValue) {
					selectedTarget = newValue;
					constructChart();
				}
				
			});
			if(targets.size()>0)
			{
				targetField.selectionModelProperty().get().select(0);	
			}			
		}
		else
		{
			throw new Exception(String.join(" ", new String[]{
					"This type of statistic cannot be modeled with Bar chart:", 
					s.getStatisticName()}));
		}
	}
	
	/**
	 * Constructs the <code>BarChart</code> from the selected <code>selectedTarget</code>.
	 * It constructs a chart given the majority and the distribution from the statistic.  
	 */
	private void constructChart()
	{
		if(selectedTarget == null) return;
		//remove the previous chart
		if(bc != null) mainAnchor.getChildren().remove(bc);
		ClassificationStat statistic;
		
		//get the correct statistic
		if(parentNode.getStatistic() instanceof ClassificationStat)
		{
			statistic = (ClassificationStat)parentNode.getStatistic();	
		}
		else
		{
			statistic = ((CombStat)parentNode.getStatistic()).getClassification();
		}
		HashMap<String, Double> distribution = statistic.getDistribution(selectedTarget);
		String majority = statistic.getMajority(selectedTarget);
		
		CategoryAxis xAxis = new CategoryAxis();
		NumberAxis yAxis = new NumberAxis();
		bc = new BarChart<String,Number>(xAxis,yAxis);
		bc.setTitle(String.join("", new String[]{"Target: ", selectedTarget}));
		xAxis.setLabel("Value");       
        yAxis.setLabel("Examples");
        
        //construct series
        Series<String, Number> series = new XYChart.Series<String, Number>();
        for(Entry<String, Double> entry: distribution.entrySet())
        {
        	//add data point
        	XYChart.Data<String, Number> data = new XYChart.Data<String, Number>(entry.getKey(), entry.getValue());
        	series.getData().add(data);
        	
        	//add tooltip and color the majority with red
        	data.nodeProperty().addListener(new ChangeListener<Node>() {
        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
        		{      			       
        			Tooltip.install(newNode, new Tooltip(String.valueOf(entry.getValue())));
        			if (data.getXValue().equals(majority)) 
        			{
        				newNode.setStyle("-fx-bar-fill: firebrick;");
        			}
        			else
        			{
        				newNode.setStyle("-fx-bar-fill: navy;");
        			}  
        		}        		
        	});        	
        }
        
        //add series to the chart
        bc.getData().add(series);
        //add the chart to the pane
        mainAnchor.getChildren().add(bc);
        bc.setLegendVisible(false);
        
        //position the chart
		AnchorPane.setBottomAnchor(bc, 34.0);
		AnchorPane.setLeftAnchor(bc, 0.0);
		AnchorPane.setRightAnchor(bc, 0.0);
		AnchorPane.setTopAnchor(bc, 0.0);
	}	
	
	/**
	 * Method for saving the chart that is currently displayed.
	 * 
	 * @throws IOException if an exception occurs during saving an image.
	 */
	@FXML
	private void saveChart() throws IOException
	{
		PlotController.saveAsPng(bc, (Stage) saveButton.getScene().getWindow());
	}
}