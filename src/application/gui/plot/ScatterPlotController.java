/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/
package application.gui.plot;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import application.gui.ExampleViewController;
import application.model.statistic.ClassificationStat;
import application.model.statistic.TimeseriesStat;
import application.model.statistic.WHTDStat;

/**
 * This is a controller for scatter plotting the examples. The user can select
 * two attributes, and their values are plotted on a scatter plot. If the
 * target is single class prediction, the points are also colored according
 * to class. Scatter plot cannot show points for hierarchies and timeseries.
 * 
 * @author Nejc Trdin
 *
 */
public class ScatterPlotController implements Initializable {

	/**
	 * The close <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button closeButton;
	
	/**
	 * The save <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button saveButton;
	
	/**
	 * The first attribute that is plotted on X axis. Described in the FXML.
	 */
	@FXML
	private ChoiceBox<String> firstChoice;
	
	/**
	 * The main <code>AnchorPane</code> specified in FXML.
	 */
	@FXML
	private AnchorPane mainAnchor;
	
	/**
	 * The second attribute that is plotted on Y axis. Described in the FXML.
	 */
	@FXML
	private ChoiceBox<String> secondChoice;
	
	/**
	 * The plotting chart type is <code>ScatterChart</code>.
	 */
	private ScatterChart scatterChart;
	
	/**
	 * The parent controller, to get the attributes and values.
	 */
	private ExampleViewController parentController;
	
	/**
	 * The index of the first selected attribute.
	 */
	private int firstSelection = -1;
	
	/**
	 * The index of the second selected attribute.
	 */
	private int secondSelection = -1;
	
	/**
	 * The normalization factor is used to set the scales of the attributes. It is used as
	 * a factor that sets the edges of the plot.
	 */
	private static final int NORMALIZATION = 10;
	
	/**
	 * The IDs of the attributes, that are classes.
	 */
	private ArrayList<Integer> classes = new ArrayList<Integer>();
	
	/**
	 * The initialization of the controller, called by the library. It also adds
	 * listeners to drop-down menus for choosing the attributes.
	 * 
	 * @param location the supplied <code>URL</code> giving the location.
	 * @param resources the <code>ResourceBundle</code> giving the resources during
	 * 			initialization.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		firstChoice.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {
				firstSelection = newValue.intValue();
				if(secondSelection>-1)
				{
					constructChart();
				}
			}			
		});
		secondChoice.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {
				secondSelection = newValue.intValue();		
				if(firstSelection>-1)
				{
					constructChart();
				}
			}			
		});				
	}
	
	/**
	 * Sets the parent controller {@link ExampleViewController}. It selects the first 
	 * target, if it exists and the second target also.
	 * 
	 * @param parent the parent controller {@link ExampleViewController}, that is a connection
	 * 			point for getting data. 
	 */
	public void setParentController(ExampleViewController parent)
	{
		parentController = parent;
		boolean isHierarchical = (parent.getParentNode().getStatistic() instanceof WHTDStat);
		boolean isTimeseries = (parent.getParentNode().getStatistic() instanceof TimeseriesStat);
		//shifted column index because of the id
		int columnIndex = -1;		
		for(TableColumn<ObservableList<String>, ?> t: parentController.getColumns())
		{
			String name = t.getText();
			if((isTimeseries || isHierarchical) && classes.contains(columnIndex))
			{
				//pass we must not include hierarchical attribute to the list of options
			}
			else if(!name.equals("id"))
			{				
				firstChoice.getItems().add(name);
				secondChoice.getItems().add(name);
			}				
			columnIndex++;
		}
		if(firstChoice.getItems().size()>0)
		{
			firstChoice.selectionModelProperty().get().select(0);
			if(firstChoice.getItems().size()>1)
			{
				secondChoice.selectionModelProperty().get().select(1);	
			}
			else
			{
				secondChoice.selectionModelProperty().get().select(0);	
			}
		}
	}
	
	/**
	 * Constructs the <code>ScatterChart</code> from the selected <code>firstSelection</code>
	 * and <code>secondSelection</code>. It gets the data from the <code>parentController</code>.
	 * Normalizes the scales with the <code>NORMALIZATION</code> value. And finally it adds the 
	 * data to the chart and puts the chart on the pane. This is a far more complicated method 
	 * than {@link RegressionPlotController#constructChart()}, because it needs to handle all
	 * 4 possible combinations of attributes on two axes (qualitative and numeric attributes).
	 */
	private void constructChart()
	{	
		//remove the previous chart
		mainAnchor.getChildren().remove(scatterChart);
		
		//try to find out, if prediction is classification and only one class
		//values in the scatter plot can be then be colored by the value
		boolean isClassificationAndOneClass = true;
		HashSet<String> uniqueClassValues = new HashSet<String>();
		ArrayList<String> classValues = new ArrayList<String>();
		if(classes.size() == 1 && parentController.getParentNode().getStatistic() instanceof ClassificationStat)
		{	
			int classId = classes.get(0) + 1;
			ArrayList<String> data = parentController.getData(classId);			
			for(String value: data)
			{
				classValues.add(value);				
			}
			uniqueClassValues.addAll(classValues);
		}		
		else
		{
			isClassificationAndOneClass = false;
		}
		
		//get data
		ArrayList<String> firstData = parentController.getData(firstSelection + 1);
		ArrayList<String> secondData = parentController.getData(secondSelection + 1);	
		String firstAttribute = parentController.getColumnName(firstSelection + 1);
		String secondAttribute = parentController.getColumnName(secondSelection + 1);
		
		//get the minimal and maximal values of the attributes, if they are numeric
		boolean isFNumeric = true;
		double fMin = Double.MAX_VALUE;
		double fMax = Double.MIN_VALUE;		
		for(String value: firstData)
		{
			try
			{
				Double val = Double.parseDouble(value);
				if(val>fMax)
				{
					fMax = val;
				}
				if(val<fMin)
				{
					fMin = val;
				}
			}
			catch (NumberFormatException ex)
			{
				isFNumeric = false;
				break;
			}			
		}
		boolean isSNumeric = true;
		double sMin = Double.MAX_VALUE;
		double sMax = Double.MIN_VALUE;
		for(String value: secondData)
		{
			try
			{
				Double val = Double.parseDouble(value);	
				if(val>sMax)
				{
					sMax = val;
				}
				if(val<sMin)
				{
					sMin = val;
				}
			}
			catch (NumberFormatException ex)
			{
				isSNumeric = false;
				break;
			}			
		}
				
		scatterChart = null;
		if(isFNumeric && isSNumeric)
		{
			//if both are numeric, we can normalize both
			int ifMin = (int)fMin;
			ifMin = ifMin - ifMin%NORMALIZATION;
			int ifMax = (int)Math.ceil(fMax);
			ifMax = ifMax + (NORMALIZATION-(ifMax%NORMALIZATION));
			int isMin = (int)sMin;
			isMin = isMin - isMin%NORMALIZATION;
			int isMax = (int)Math.ceil(sMax);
			isMax = isMax + (NORMALIZATION-(isMax%NORMALIZATION));			
			NumberAxis xAxis = new NumberAxis(ifMin, ifMax, 1);
	        NumberAxis yAxis = new NumberAxis(isMin, isMax, 1);
	        xAxis.setLabel(firstAttribute);
	        yAxis.setLabel(secondAttribute);
	        scatterChart = new ScatterChart(xAxis,yAxis);
			if(isClassificationAndOneClass)
			{
				//if this is classification with one class, we construct multiple series, for
				//each value of the class attribute
				HashMap<String, XYChart.Series> series = new HashMap<String, XYChart.Series>(); 
				for(String value: uniqueClassValues)
				{
					XYChart.Series serie = new XYChart.Series();
					serie.setName(value);
					series.put(value, serie);
				}
				for(int i=0;i<firstData.size();i++)
				{
					int id = i;
					XYChart.Data data = 
							new XYChart.Data(Double.parseDouble(firstData.get(i)), Double.parseDouble(secondData.get(i)));
					series.get(classValues.get(i)).getData().add(data);	
					//create data point and add a tooltip
					data.nodeProperty().addListener(new ChangeListener<Node>() {
		        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
		        		{
		        			if (newNode != null) 
		        			{        				
		        				Tooltip.install(newNode, new Tooltip(String.valueOf(firstData.get(id)+", "+secondData.get(id))));
		        		    }
		        		}
		        	});
				}
				//add series to chart
				scatterChart.getData().addAll(series.values());
			}
			else
			{
				//if not, construct only one series
				XYChart.Series series = new XYChart.Series();				
				for(int i=0;i<firstData.size();i++)
				{
					int id = i;
					XYChart.Data data = 
							new XYChart.Data(Double.parseDouble(firstData.get(i)), Double.parseDouble(secondData.get(i)));
					series.getData().add(data);
					//create data point and add a tooltip
					data.nodeProperty().addListener(new ChangeListener<Node>() {
		        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
		        		{
		        			if (newNode != null) 
		        			{        				
		        				Tooltip.install(newNode, new Tooltip(String.valueOf(firstData.get(id)+", "+secondData.get(id))));
		        		    }
		        		}
		        	});
				}
				//add series to chart
				scatterChart.getData().addAll(series);
			}			
		}
		else if(isFNumeric)
		{			
			//if first is numeric, we can normalize it
			int ifMin = (int)fMin;
			ifMin = ifMin - ifMin%NORMALIZATION;			
			int ifMax = (int)Math.ceil(fMax);
			ifMax = ifMax + (NORMALIZATION-(ifMax%NORMALIZATION));
			NumberAxis xAxis = new NumberAxis(ifMin, ifMax, 1);
			CategoryAxis yAxis = new CategoryAxis();
			xAxis.setLabel(firstAttribute);
	        yAxis.setLabel(secondAttribute);
			scatterChart = new ScatterChart(xAxis,yAxis);
			if(isClassificationAndOneClass)
			{
				//if this is classification with one class, we construct multiple series, for
				//each value of the class attribute
				HashMap<String, XYChart.Series> series = new HashMap<String, XYChart.Series>(); 
				for(String value: uniqueClassValues)
				{
					XYChart.Series serie = new XYChart.Series();
					serie.setName(value);
					series.put(value, serie);
				}
				for(int i=0;i<firstData.size();i++)
				{
					int id = i;
					XYChart.Data data = new XYChart.Data(Double.parseDouble(firstData.get(i)), secondData.get(i));
					series.get(classValues.get(i)).getData().add(data);	
					//create data point and add a tooltip
					data.nodeProperty().addListener(new ChangeListener<Node>() {
		        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
		        		{
		        			if (newNode != null) 
		        			{        				
		        				Tooltip.install(newNode, new Tooltip(String.valueOf(firstData.get(id)+", "+secondData.get(id))));
		        		    }
		        		}
		        	});
				}
				//add series to chart
				scatterChart.getData().addAll(series.values());
			}
			else
			{
				//if not, construct only one series
				XYChart.Series series = new XYChart.Series();
				for(int i=0;i<firstData.size();i++)
				{
					int id = i;
					XYChart.Data data = new XYChart.Data(Double.parseDouble(firstData.get(i)), secondData.get(i));
					series.getData().add(data);
					//create data point and add a tooltip
					data.nodeProperty().addListener(new ChangeListener<Node>() {
		        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
		        		{
		        			if (newNode != null) 
		        			{        				
		        				Tooltip.install(newNode, new Tooltip(String.valueOf(firstData.get(id)+", "+secondData.get(id))));
		        		    }
		        		}
		        	});
				}
				//add series to chart
				scatterChart.getData().addAll(series);
			}			
		}
		else if(isSNumeric)
		{
			//if second is numeric, we can normalize it
			int isMin = (int)sMin;
			isMin = isMin - isMin%NORMALIZATION;
			int isMax = (int)Math.ceil(sMax);
			isMax = isMax + (NORMALIZATION-(isMax%NORMALIZATION));
			CategoryAxis xAxis = new CategoryAxis();
	        NumberAxis yAxis = new NumberAxis(isMin, isMax, 1);
	        xAxis.setLabel(firstAttribute);
	        yAxis.setLabel(secondAttribute);
	        scatterChart = new ScatterChart(xAxis,yAxis);
			if(isClassificationAndOneClass)
			{
				//if this is classification with one class, we construct multiple series, for
				//each value of the class attribute
				HashMap<String, XYChart.Series> series = new HashMap<String, XYChart.Series>(); 
				for(String value: uniqueClassValues)
				{
					XYChart.Series serie = new XYChart.Series();
					serie.setName(value);
					series.put(value, serie);
				}
				for(int i=0;i<firstData.size();i++)
				{
					int id = i;
					XYChart.Data data = new XYChart.Data(firstData.get(i), Double.parseDouble(secondData.get(i)));
					series.get(classValues.get(i)).getData().add(data);	
					//create data point and add a tooltip
					data.nodeProperty().addListener(new ChangeListener<Node>() {
		        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
		        		{
		        			if (newNode != null) 
		        			{        				
		        				Tooltip.install(newNode, new Tooltip(String.valueOf(firstData.get(id)+", "+secondData.get(id))));
		        		    }
		        		}
		        	});
				}
				//add series to chart
				scatterChart.getData().addAll(series.values());
			}
			else
			{
				//if not, construct only one series
				XYChart.Series series = new XYChart.Series();		        
				for(int i=0;i<firstData.size();i++)
				{
					int id = i;
					XYChart.Data data = new XYChart.Data(firstData.get(i), Double.parseDouble(secondData.get(i)));
					series.getData().add(data);
					//create data point and add a tooltip
					data.nodeProperty().addListener(new ChangeListener<Node>() {
		        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
		        		{
		        			if (newNode != null) 
		        			{        				
		        				Tooltip.install(newNode, new Tooltip(String.valueOf(firstData.get(id)+", "+secondData.get(id))));
		        		    }
		        		}
		        	});
				}
				//add series to chart
				scatterChart.getData().addAll(series);
			}			
		}
		else
		{
			//if none are numeric, we plot them directly
			CategoryAxis xAxis = new CategoryAxis();
	        CategoryAxis yAxis = new CategoryAxis();
	        scatterChart = new ScatterChart(xAxis,yAxis);
			if(isClassificationAndOneClass)
			{
				//if this is classification with one class, we construct multiple series, for
				//each value of the class attribute
				HashMap<String, XYChart.Series> series = new HashMap<String, XYChart.Series>(); 
				for(String value: uniqueClassValues)
				{
					XYChart.Series serie = new XYChart.Series();
					serie.setName(value);
					series.put(value, serie);
				}
				for(int i=0;i<firstData.size();i++)
				{
					int id = i;
					XYChart.Data data = new XYChart.Data(firstData.get(i), secondData.get(i));
					series.get(classValues.get(i)).getData().add(data);
					//create data point and add a tooltip
					data.nodeProperty().addListener(new ChangeListener<Node>() {
		        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
		        		{
		        			if (newNode != null) 
		        			{        				
		        				Tooltip.install(newNode, new Tooltip(String.valueOf(firstData.get(id)+", "+secondData.get(id))));
		        		    }
		        		}
		        	});
				}
				//add series to chart
				scatterChart.getData().addAll(series.values());
			}
			else
			{
				//if not, construct only one series
				XYChart.Series series = new XYChart.Series();				
				for(int i=0;i<firstData.size();i++)
				{
					int id = i;
					XYChart.Data data = new XYChart.Data(firstData.get(i), secondData.get(i));
					series.getData().add(data);
					//create data point and add a tooltip
					data.nodeProperty().addListener(new ChangeListener<Node>() {
		        		@Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) 
		        		{
		        			if (newNode != null) 
		        			{        				
		        				Tooltip.install(newNode, new Tooltip(String.valueOf(firstData.get(id)+", "+secondData.get(id))));
		        		    }
		        		}
		        	});
				}
				//add series to chart
				scatterChart.getData().addAll(series);
			}			
		}		
		//if this is classification with one class attribute, we can construct a legend
		if(!isClassificationAndOneClass)
		{			
			scatterChart.setLegendVisible(false);
		}		
		scatterChart.setTitle(String.join(" ", new String[]{"Scatter Chart:", firstAttribute, "vs.", secondAttribute}));
		
		//position and add the chart to the pane
		AnchorPane.setBottomAnchor(scatterChart, 34.0);
		AnchorPane.setLeftAnchor(scatterChart, 0.0);
		AnchorPane.setRightAnchor(scatterChart, 0.0);
		AnchorPane.setTopAnchor(scatterChart, 0.0);
		mainAnchor.getChildren().add(scatterChart);		
	}

	/**
	 * Sets the IDs of the class attributes.
	 * 
	 * @param inClasses the list of class attribute IDs.
	 */
	public void setClasses(ArrayList<Integer> inClasses)
	{
		classes = inClasses;		
	}
	
	/**
	 * This method is specified in the FXML schema, which closes the window.
	 */
	@FXML
	private void closeButtonAction(){	    
	    Stage stage = (Stage) closeButton.getScene().getWindow();
	    stage.close();
	}	
	
	/**
	 * Method for saving the chart that is currently displayed.
	 * 
	 * @throws IOException if an exception occurs during saving an image.
	 */
	@FXML
	private void saveChart() throws IOException
	{
		PlotController.saveAsPng(scatterChart, (Stage) saveButton.getScene().getWindow());
	}
}