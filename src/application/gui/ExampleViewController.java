/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ResourceBundle;

import application.ClusVis;
import application.gui.plot.DistributionPlotController;
import application.gui.plot.ScatterPlotController;
import application.model.ClusNode;
import application.model.statistic.TimeseriesStat;
import application.model.statistic.WHTDStat;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * This is a controller for viewing of examples. All the example values are
 * put into columns, according to their attributes. If the attributes are
 * numeric or qualitative, statistics are also shown in the lower bar.
 * 
 * @author Nejc Trdin
 *
 */
public class ExampleViewController implements Initializable {

	/**
	 * The close <code>Button</code> specified in FXML.
	 */
	@FXML
	private Button closeButton;
	
	/**
	 * The <code>Button</code> for showing scatter plot specified in FXML.
	 */
	@FXML
	private Button scatterButton;
	
	/**
	 * The <code>TableView</code> specified in FXML, used for showing the examples.
	 */
	@FXML
	private TableView<ObservableList<String>> table;
		
	/**
	 * The ID used for inserting examples into the table.
	 */
	private int id = 1;
	
	/**
	 * The <code>HBox</code> specified in FXML, used for showing attribute statistics.
	 */
	@FXML
	private HBox infoBox;
	
	/**
	 * The <code>Label</code> specified in FXML, used for showing currently selected
	 * attribute's name.
	 */
	@FXML
	private Label attrName;
	
	/**
	 * A list of integers that correspond with the class attributes.
	 */
	private ArrayList<Integer> classes = new ArrayList<Integer>();
	
	/**
	 * The parent node from the model, that corresponds with this example view.
	 */
	private ClusNode parentNode;
	
	/**
	 * A variable used while displaying the attribute stats, so that we do not need to
	 * recompute all the statistics, if the selected column did not change.
	 */
	private int previousColumnIndex = -1;
	
	/**
	 * A set of elements, that describe the currently selected attribute. When a new column
	 * is selected, all elements in this set are removed from the view, and new elements are
	 * shown.
	 */
	private HashSet<Node> appearingElements = new HashSet<Node>();
	
	/**
	 * A variable that tells, if the prediction is hierarchical prediction.
	 */
	private boolean isHierarchical = false;
	
	/**
	 * A variable that tells, if the prediction is timeseries prediction.
	 */
	private boolean isTimeseries = false;	
	
	/**
	 * The initialization of the controller, called by the library. It creates the ID
	 * column and registers the cell value factory callback method, for getting the values
	 * of the ID attribute. Also adds a mouse click handler, when the user selects a column/row
	 * so that the stats of the selected attribute update. See {@link #updateStats(int)}.
	 * 
	 * @param location the supplied <code>URL</code> giving the location.
	 * @param resources the <code>ResourceBundle</code> giving the resources during
	 * 			initialization.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		//set the currently selected attribute name to N/A
		attrName.setText("N/A");
		
		//add the first column as the ID
		TableColumn<ObservableList<String>, String> column = new TableColumn<ObservableList<String>, String>("id");		
		column.setCellValueFactory(new Callback<CellDataFeatures<ObservableList<String>,String>,ObservableValue<String>>(){
			public ObservableValue<String> call(CellDataFeatures<ObservableList<String>, String> param) {                                                                                             
				return new SimpleStringProperty(param.getValue().get(0).toString());                       
			}                   
		});
		table.getColumns().add(column);	
		//register a mouse event handler
		table.setOnMouseClicked(new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) 
			{
				//if it is the primary button click, and a single click
				if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount()==1)
				{
					ObservableList<TablePosition> cells = table.getSelectionModel().getSelectedCells();
					for(TablePosition cell : cells)
					{
						int column = cell.getColumn();
						//get the column of the clicked cell and update stats
					    if(column>0)
					    {
					    	updateStats(column);					    	
					    }
					}			           
			    }				
			}			
		});
	}
	
	/**
	 * Updates the statistic of the currently selected column. It deletes the previously
	 * displayed stats. It displays selected attribute's name and the average and standard 
	 * deviation for the numeric values and the number of values of the classification values, and
	 * additional distribution button, which displays the distribution as a nice plot.
	 * 
	 * @param columnIndex the index of the column, for which we update statistic
	 */
	private void updateStats(int columnIndex)
	{
		//if the clicked column index is not different, we do nothing
		if(previousColumnIndex==columnIndex)
		{
			return;
		}
		//if column index is in classes and the prediction is timeseries or hierarchical,
		//we do nothing
		if((isHierarchical||isTimeseries) && classes.contains(columnIndex - 1))
		{
			return;
		}
		
		previousColumnIndex = columnIndex;
		String attributeName = this.getColumnName(columnIndex);
		
		//set the name of the attribute
		attrName.setText(attributeName);
		
		//get the data for this column
		ArrayList<String> data = getData(columnIndex);
		
		//initialize lists, for cleaning and storing the data
		ArrayList<String> cleanData = new ArrayList<String>();
		ArrayList<Double> doubleData = new ArrayList<Double>();
		int questions = 0;
    	for(String value: data)
    	{
    		if(value.equals("?")||value.equals("-"))
    		{
    			//we just count the number of "?", we do not add them
    			questions++;
    			continue;
    		}
    		try
    		{
    			//this checks if number is integer... if all are integer, then we consider it as qualitative
    			Integer.parseInt(value);
    			//we always add to cleanData
    			cleanData.add(value);
    		}
    		catch (NumberFormatException ex)
    		{
    			try
        		{
    				//if all are double, we can consider it as double
        			Double dVal = Double.parseDouble(value);
        			doubleData.add(dVal);
        			//we always add to cleanData
        			cleanData.add(value);
        		}
        		catch(NumberFormatException e)
        		{
        			//we always add to cleanData
        			cleanData.add(value);
        		}	
    		}    		
    	}
    	if(cleanData.size()==0) return;
    	
    	//remove all elements that are currently appearing
    	infoBox.getChildren().removeAll(appearingElements);
    	//and clear the set of appearing elements
		appearingElements.clear();
		
		//get the unique values of the data
		HashSet<String> values = new HashSet<String>(cleanData);
		
    	if(doubleData.size()==cleanData.size())
    	{
    		//we are considering numeric values    		
    		String frmt = "%.5f";
    		
    		//compute average and standard deviation
    		double average = computeAvg(doubleData);
    		double stdDev = computeStdDev(doubleData, average);
    		//set the labels for statistic and their corresponding values
    		//average
    		Label avgLbl = new Label("Average:");
    		avgLbl.setFont(Font.font("System", FontWeight.BOLD, 13));
    		Label avgNLbl = new Label(String.format(frmt, average));
    		//standard deviation
    		Label stdDevLbl = new Label("StdDev:");
    		stdDevLbl.setFont(Font.font("System", FontWeight.BOLD, 13));
    		Label stdDevNLbl = new Label(String.format(frmt, stdDev));
    		Separator separator = new Separator(Orientation.VERTICAL);
    		
    		//add them to the appearing elements
    		appearingElements.add(avgLbl);
    		appearingElements.add(avgNLbl);
    		appearingElements.add(stdDevLbl);
    		appearingElements.add(stdDevNLbl);
    		appearingElements.add(separator);
    		//display them
    		infoBox.getChildren().add(separator);
    		infoBox.getChildren().add(avgLbl);
    		infoBox.getChildren().add(avgNLbl);    		
    		separator = new Separator(Orientation.VERTICAL);
    		appearingElements.add(separator);
    		infoBox.getChildren().add(separator);
    		infoBox.getChildren().add(stdDevLbl);
    		infoBox.getChildren().add(stdDevNLbl);    		
    	}
    	else
    	{
    		//we are considering qualitative values    
    		//set the labels for statistic and their corresponding value
    		Label valuesLbl = new Label("Values:");
    		valuesLbl.setFont(Font.font("System", FontWeight.BOLD, 13));
    		Label valuesNLbl = new Label(String.valueOf(values.size()));    		
    		Separator separator = new Separator(Orientation.VERTICAL);
    		
    		//add them to the appearing elements
    		appearingElements.add(separator);
    		appearingElements.add(valuesLbl);
    		appearingElements.add(valuesNLbl);
    		
    		//display them
    		infoBox.getChildren().add(separator);
    		infoBox.getChildren().add(valuesLbl);
    		infoBox.getChildren().add(valuesNLbl);
    		separator = new Separator(Orientation.VERTICAL);
    		appearingElements.add(separator);
    		infoBox.getChildren().add(separator);
    		
    		if(values.size() > 1)
    		{
    			//if there are more than 1 values, we can display a distribution plot
    			//create the distribution    			
    			HashMap<String,Integer> distribution = new HashMap<String,Integer>();
    			distribution.put("?", questions);
    			for(String value: values)
    			{
    				distribution.put(value, 0);
    			}
    			for(String value: cleanData)
    			{
    				distribution.put(value, distribution.get(value) + 1);
    			}
    			//create a button for displaying the distribution
    			//and add it to the appearing elements and display it
    			Button distrBtn = new Button("Plot distribution");
        		infoBox.getChildren().add(distrBtn);
        		appearingElements.add(distrBtn);
        		
        		//register an event handler, for when the button is pressed
        		//so that the distribution is displayed
        		distrBtn.setOnAction(new EventHandler<ActionEvent>()
        		{
					@Override
					public void handle(ActionEvent event) 
					{					
						try 
						{
							//load the FXML file
							FXMLLoader loader = new FXMLLoader(ClusVis.class.getResource("gui/plot/DistributionPlot.fxml"));			
							AnchorPane pane = loader.load();
							DistributionPlotController controller = (DistributionPlotController)loader.getController();
							//send tht arguments to the plot
							controller.setArgs(distribution, attributeName);
							Stage stage = new Stage();
							Scene scene = new Scene(pane);
							stage.setTitle(String.join(" ", new String[]{"Distribution for", attributeName}));
							stage.setScene(scene);
							//wait for the window to close
							stage.initModality(Modality.APPLICATION_MODAL);
							//show the window
							stage.show();	

						} catch (IOException ex) 
						{
							//catch exceptions that occur and show a window displaying the stack trace
							Alert alert = new Alert(AlertType.ERROR);
							alert.setTitle("Exception");
							alert.setHeaderText("Exception occured!");
							
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							ex.printStackTrace(pw);
							String exceptionText = sw.toString();
							Label labelMessage = new Label(ex.getMessage());
							Label label = new Label("The exception stacktrace:");

							TextArea textArea = new TextArea(exceptionText);
							textArea.setEditable(false);
							textArea.setWrapText(true);

							textArea.setMaxWidth(Double.MAX_VALUE);
							textArea.setMaxHeight(Double.MAX_VALUE);
							GridPane.setVgrow(textArea, Priority.ALWAYS);
							GridPane.setHgrow(textArea, Priority.ALWAYS);

							GridPane expContent = new GridPane();
							expContent.setMaxWidth(Double.MAX_VALUE);
							expContent.add(labelMessage, 0, 0);
							expContent.add(label, 0, 1);
							expContent.add(textArea, 0, 2);
							
							alert.getDialogPane().setContent(expContent);
							alert.showAndWait();
						}
					}
				});
    		}    		
    	}
	}
	
	/**
	 * Computing the average of the supplied double values.
	 * 
	 * @param values a list of double values.
	 * 
	 * @return the average value of the supplied list.
	 */
	private double computeAvg(ArrayList<Double> values)
	{
		double sum = 0.0;
		for(Double value: values)
		{
			sum += value;
		}
		return sum/values.size();
	}
	
	/**
	 * Computing the standard deviation of double values, given the average.
	 * 
	 * @param values a list of double values.
	 * @param average the average value of the elements in the list.
	 * 
	 * @return the standard deviation value of the supplied list.
	 */
	private double computeStdDev(ArrayList<Double> values, double average)
	{
		double squareSum = 0.0;
		for(Double value: values)
		{
			squareSum += Math.pow(value - average, 2.0);			
		}
		return Math.sqrt(squareSum/values.size());
	}
	
	/**
	 * Setting the list of IDs, which represent the indices of columns, which 
	 * are class attributes.
	 * 
	 * @param inClasses a list of integers, that represent the indices of class
	 * 			attributes. 
	 */
	public void setClasses(ArrayList<Integer> inClasses)
	{
		classes = inClasses;		
	}
	
	/**
	 * Sets the parent {@link ClusNode} in the model of this particular example view.
	 * 
	 * @param parent the parent {@link ClusNode}.
	 */
	public void setParentNode(ClusNode parent)
	{
		parentNode = parent;
		isHierarchical = (parentNode.getStatistic() instanceof WHTDStat);
		isTimeseries = (parentNode.getStatistic() instanceof TimeseriesStat);
	}
	
	/**
	 * Gets the parent {@link ClusNode} in the model of this particular example view.
	 * 
	 * @return the parent {@link ClusNode}.
	 */
	public ClusNode getParentNode()
	{
		return parentNode;
	}
	
	/**
	 * This method is specified in the FXML schema, which closes the window.
	 */
	@FXML
	private void closeButtonAction(){	    
	    Stage stage = (Stage) closeButton.getScene().getWindow();
	    stage.close();
	}
	
	/**
	 * Adds one column to the example table. Because the getting of the values of the cells
	 * is quite complicated (using properties), a callback function needs to be added to the
	 * cell value factory, which shows the cell, how to get it's value.
	 * 
	 * @param name the name of the column.
	 * @param index the index of the column in the example row.
	 */
	public void addColumn(String name, int index)
	{
		TableColumn<ObservableList<String>, String> column = new TableColumn<ObservableList<String>, String>(name);		
		column.setCellValueFactory(new Callback<CellDataFeatures<ObservableList<String>,String>,ObservableValue<String>>(){
			public ObservableValue<String> call(CellDataFeatures<ObservableList<String>, String> param) {                                                                                             
				return new SimpleStringProperty(param.getValue().get(index).toString());                       
			}                   
		});		
		table.getColumns().add(column);		
	}
	
	/**
	 * Adds one example to the table.
	 * 
	 * @param example a list of strings, representing the values of the attributes.
	 */
	public void addRow(ArrayList<String> example)
	{		
		//everything is put into an ObservableList
		ObservableList<String> row = FXCollections.observableArrayList();
		//first value is always ID
		row.add(Integer.toString((id++)));
		for(String value: example)
		{
			row.add(value);
		}	
		//the row is put into the table
		table.getItems().add(row);
	}
	
	/**
	 * Gets all data entries for the specified column index.
	 * 
	 * @param column the index of the column, for which the data is returned.
	 * 
	 * @return a list of strings. Column values in the column with the corresponding index. 
	 */
	public ArrayList<String> getData(int column)
	{
		ArrayList<String> out = new ArrayList<String>();
		for(ObservableList<String> row: table.getItems())
		{
			out.add(row.get(column));
		}
		return out;
	}
	
	/**
	 * Gets the column name from the supplied ID.
	 * 
	 * @param id the ID for which we need the column name.
	 * 
	 * @return the name of the column.
	 */
	public String getColumnName(int id)
	{
		return table.getColumns().get(id).getText();
	}
	
	/**
	 * A method defined in FXML description. It loads the scatter plot for this particular
	 * example set. Then it waits for closure of the window. It also catches the problems
	 * in construction of the scatter plot, and show a nice window giving the stack trace.
	 */
	@FXML
	private void scatterButtonAction(){		
		try 
		{
			FXMLLoader loader = new FXMLLoader(ClusVis.class.getResource("gui/plot/ScatterPlot.fxml"));			
			AnchorPane pane = loader.load();
			ScatterPlotController controller = (ScatterPlotController)loader.getController();
			//supply parent classes
			controller.setClasses(classes);
			controller.setParentController(this);
			Stage stage = new Stage();
			Scene scene = new Scene(pane);
			stage.setTitle("Scatter Plot");
			stage.setScene(scene);
			//the waiting is done by setting the application modality
			stage.initModality(Modality.APPLICATION_MODAL);								
			stage.show();	

		} catch (IOException ex) 
		{
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Exception");
			alert.setHeaderText("Exception occured!");
			
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			String exceptionText = sw.toString();
			Label labelMessage = new Label(ex.getMessage());
			Label label = new Label("The exception stacktrace:");

			TextArea textArea = new TextArea(exceptionText);
			textArea.setEditable(false);
			textArea.setWrapText(true);

			textArea.setMaxWidth(Double.MAX_VALUE);
			textArea.setMaxHeight(Double.MAX_VALUE);
			GridPane.setVgrow(textArea, Priority.ALWAYS);
			GridPane.setHgrow(textArea, Priority.ALWAYS);

			GridPane expContent = new GridPane();
			expContent.setMaxWidth(Double.MAX_VALUE);
			expContent.add(labelMessage, 0, 0);
			expContent.add(label, 0, 1);
			expContent.add(textArea, 0, 2);
			
			alert.getDialogPane().setContent(expContent);
			alert.showAndWait();
		}		
	}

	/**
	 * Method used for getting the column names (attribute names).
	 * 
	 * @return a list of column names.
	 */
	public ObservableList<TableColumn<ObservableList<String>, ?>> getColumns()
	{
		return table.getColumns();
	}

}
