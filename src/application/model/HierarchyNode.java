/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model;

import java.util.ArrayList;
import java.util.Arrays;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * A class for node in the hierarchy for usage for prediction.
 * 
 * @author Nejc Trdin
 *
 */
public class HierarchyNode {

	/**
	 * List of child elements in the hierarchy.
	 */
	private ArrayList<HierarchyNode> children = new ArrayList<HierarchyNode>();
	
	/**
	 * ID of the hierarchical node, assigned by Clus.
	 */
	private String id;
	
	/**
	 * Name of the hierarchical node, described in the hierarchy.
	 */
	private String name;
	
	/**
	 * Probability of this node being in the hierarchy (during prediction).
	 */
	private Double weight;
	
	/**
	 * Constructor of the hierarchical node from the XML element "Node". This constructor 
	 * is used when the hierarchy is parsed from the supplied XML document.
	 * 
	 * @param item described by an XML element Node.
	 * @param hierarchy the instance of the hierarchy this node belongs to.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public HierarchyNode(Element item, Hierarchy hierarchy) throws XMLParseException 
	{
		if(!item.getNodeName().equals("Node"))
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Elements in hierarchy must be Node. Got ", item.getNodeName(), "."}));
		}
		if(!item.hasAttribute("name")||!item.hasAttribute("id"))
		{
			throw new XMLParseException("Hierarchy Nodes must have attributes id and name.");					
		}
		id = item.getAttribute("id");
		name = item.getAttribute("name");
		hierarchy.setNodeID(id, this);
		NodeList childs = item.getChildNodes();
		
		//adding children of this node if they exist
		for(int i=0;i<childs.getLength();i++)
		{
			Node child = childs.item(i);
			if(!(child instanceof Element))
			{
				throw new XMLParseException("Children nodes in the hierarchy must be of Element type.");	
			}
			children.add(new HierarchyNode((Element)child, hierarchy));
		}
	}
	
	/**
	 * Constructor for hierarchy node. Used for cloning the hierarchy during display in the prediction.
	 * 
	 * @param node the existing node being cloned.
	 * @param hierarchy the hierarchy to which this node belongs to.
	 * 
	 * @throws IllegalStateException when the {@link application.model.Hierarchy#setNodeID(String, HierarchyNode)}
	 * 			throws an exception.
	 */
	public HierarchyNode(HierarchyNode node, Hierarchy hierarchy) throws IllegalStateException
	{
		this.id = node.getID();
		this.name = node.getName();
		hierarchy.setNodeID(this.id, this);
		for(HierarchyNode child: node.getChildren())
		{
			children.add(new HierarchyNode(child, hierarchy));
		}
	}
	
	/**
	 * Constructor for hierarchy node used when parsing predictions as path in the hierarchy. Note this node
	 * does not belong to any hierarchy.
	 * 
	 * @param path the path of this particular node, where the parents have already been covered.
	 */
	public HierarchyNode(String path)
	{
		String[] names = path.split("/");
		name = names[0];
		if(names.length>1)
		{
			children.add(new HierarchyNode(String.join("/", Arrays.copyOfRange(names, 1, names.length))));
		}
	}
	
	/**
	 * Get children of this hierarchy node.
	 * 
	 * @return list of children of the hierarchy node.
	 */
	public ArrayList<HierarchyNode> getChildren()
	{
		return children;
	}
	
	/**
	 * Get the id of this node.
	 * 
	 * @return the string id of this node.
	 */
	public String getID()
	{
		return id;
	}
	
	/**
	 * Get name of this node.
	 * 
	 * @return the name of this node.
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * A method used for extending the hierarchy with an additional path. If the whole path 
	 * is already present in the hierarchy, this method does nothing. Otherwise it traverses
	 * the hierarchy, finds the point from where the path does not exist anymore. Then it 
	 * continues constructing hierarchical nodes {@link #HierarchyNode(String)}.
	 * 
	 * @param path the path representing the hierarchical structure from this node downwards.
	 */
	public void extend(String path)
	{
		String[] names = path.split("/");
		
		if(names.length==0)
		{
			//we reached the end so the path already exists in the hierarchy.
			return;
		}
		
		// a flag to check if we found a child matching
		boolean handled = false;		
		for(HierarchyNode child: children)
		{			
			if(names[0].equals(child.getName()))
			{
				//we found a matching child
				handled = true;
				child.extend(String.join("/", Arrays.copyOfRange(names, 1, names.length)));
				//we can break and finish
				break;
			}
		}
		
		//if we did not find a matching child, we construct a new node
		if(!handled)
		{
			children.add(new HierarchyNode(path));
		}
	}
	
	/**
	 * Set a custom weight of this node.
	 * 
	 * @param weight the probability of this node in the hierarchy.
	 */
	public void setWeight(double weight)
	{
		this.weight = weight;
	}
	
	/**
	 * Get the probability of this node in the hierarchy.
	 * 
	 * @return the probability of this node.
	 */
	public Double getWeight()
	{
		return weight;
	}
	
	/**
	 * A method used for pruning the hierarchy using the probability of nodes.
	 * If the node has a probability of 0.0, we prune the subtree. This is used in
	 * displaying predictions in the hierarchy. We do not want to display nodes that
	 * have probability of 0.0. The function is needed because the hierarchies with
	 * probabilities are constructed from a full hierarchy and all the nodes are
	 * included.
	 */
	public void prune() 
	{		
		ArrayList<HierarchyNode> newChildren = new ArrayList<HierarchyNode>();
		for(HierarchyNode child: getChildren())
		{
			if(child.getWeight()>0.0)
			{
				newChildren.add(child);
				child.prune();
			}
		}
		children = newChildren;
	}

	/**
	 * Exports a hierarchy node to XML, while appending the children below as a
	 * structure. Attributes for {@link #id} and {@link #name} are
	 * also exported. Child Elements are constructed by the recursive calls.
	 * 
	 * @param document the document in which elements are constructed.
	 * 
	 * @return the Element representing the hierarchy.
	 */
	public Element exportToXML(Document document) 
	{		
		Element node = document.createElement("Node");
		node.setAttribute("id", getID());
		node.setAttribute("name", getName());
		for(HierarchyNode child: children)
		{
			node.appendChild(child.exportToXML(document));
		}		
		return node;
	}
}
