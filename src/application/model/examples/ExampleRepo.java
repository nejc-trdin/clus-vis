/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model.examples;

import java.util.ArrayList;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import application.model.ClusNode;

/**
 * Class for storing the examples in each node.
 *  
 * @author Nejc Trdin
 *
 */

public class ExampleRepo {

	/**
	 * Boolean value for constructing the attributes. If this value is true,
	 * the attribute names and types are added.
	 */	
	private boolean first = true;
	
	/**
	 * List contains the names of the attributes.
	 */
	private ArrayList<String> attributes = new ArrayList<String>();
	
	/**
	 * List contains the types of attributes. Usually "regular" or "class".
	 */
	private ArrayList<String> attributeTypes = new ArrayList<String>();
	
	/**
	 * Each element in the list represents one single example. The list inside consists 
	 * of values that belong to this examples. Their corresponding attribute names are matched
	 * with the index of the value.  
	 */
	private ArrayList<ArrayList<String>> exampleValues = new ArrayList<ArrayList<String>>();
	
	/**
	 * The node in the tree that contains these examples.
	 */
	private ClusNode owner;
	
	/**
	 * Constructor for the <code>ExampleRepo</code>.
	 * 
	 * @param owner the tree node that has the examples in question.
	 */
	public ExampleRepo(ClusNode owner)
	{
		this.owner = owner;
	}
	
	/**
	 * Constructor for the <code>ExampleRepo</code>.
	 * 
	 * @param element a XML element type that holds examples, beginning with <code>Examples</code> node.
	 * @param owner the tree node that has the examples in question.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency in the supplied element.
	 */
	public ExampleRepo(Element element, ClusNode owner) throws XMLParseException
	{
		this.owner = owner;
		if(element.getNodeName() == "Examples")
		{
			NodeList examples = element.getChildNodes();
			for(int i=0;i<examples.getLength();i++)
			{
				Node ex = examples.item(i);
				if(!(ex instanceof Element))
				{
					throw new XMLParseException(
							"Children of Examples should always be XML Element.");
				}
				Element example = (Element)ex;
				addExample(example);
			}
		}
		else
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Unknown node type: ", element.getNodeName(), ". Expected Examples."}));
		}
	}
	
	/**
	 * Method for parsing one example from XML.
	 * 
	 * @param example a XML element type that holds one example, beginning with <code>Example</code>.
	 * 
	 * @throws XMLParseException when there is an inconsistency in the supplied element.
	 */
	private void addExample(Element example) throws XMLParseException
	{
		if(example.getNodeName() == "Example")
		{
			NodeList attributeList = example.getChildNodes();
			ArrayList<String> exampleVals = new ArrayList<String>();
			for(int i=0;i<attributeList.getLength();i++)
			{
				Node attr = attributeList.item(i);
				if(!(attr instanceof Element))
				{
					throw new XMLParseException(
							"Children of Examples should always be XML Element.");
				}
				Element attribute = (Element)attr;				
				if(attribute.hasAttribute("name") && attribute.hasAttribute("type"))
				{
					if(first)
					{
						//if this is the first example that we encountered
						//we add the name and the attribute type to the lists
						attributes.add(attribute.getAttribute("name"));
						attributeTypes.add(attribute.getAttribute("type"));						
					}
					else
					{
						if(!attributes.get(i).equals(attribute.getAttribute("name")))
						{
							throw new XMLParseException(String.join("", 
									new String[]{"Attribute ", attribute.getAttribute("name"), 
									" should have name ", attributes.get(i)}));
						}
						if(!attributeTypes.get(i).equals(attribute.getAttribute("type")))
						{
							throw new XMLParseException(String.join("", 
									new String[]{"Attribute type of ", attribute.getAttribute("name"), 
									" should be ", attributeTypes.get(i)}));
						}
					}
					exampleVals.add(attribute.getTextContent());
				}
				else
				{
					throw new XMLParseException(
							"Attribute should always have 'name' and 'type' attribute.");
				}				
			}
			exampleValues.add(exampleVals);
			first = false;
		}
		else
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Unknown node type: ", example.getNodeName(), ". Expected Example."}));
		}
	}
	
	/**
	 * Get the node from the hierarchy.
	 * 
	 * @return the node from the hierarchy that this examples belong to.
	 */
	public ClusNode getOwner()
	{
		return owner;
	}
	
	/**
	 * Get attribute names.
	 * 
	 * @return the list of attribute names.
	 */
	public ArrayList<String> getAttributes()
	{
		return attributes;
	}
	
	/**
	 * Get attribute types.
	 * 
	 * @return the list of attribute types.
	 */
	public ArrayList<String> getAttributeTypes()
	{
		return attributeTypes;
	}
	
	/**
	 * Get all example values. Each list in the main list contains the values
	 * that correspond to the indices of attributes in {@link #getAttributes}.
	 * 
	 * @return the example values.
	 */
	public ArrayList<ArrayList<String>> getExampleValues()
	{
		return exampleValues;
	}
	
	/**
	 * Get number of examples in this node.
	 * 
	 * @return number of examples for this node.
	 */
	public int getNExamples()
	{
		return exampleValues.size();
	}
	
	/**
	 * Get the IDs of class attributes.
	 * 
	 * @return a list of IDs of class attributes.
	 */
	public ArrayList<Integer> getClassIDAttributes()
	{
		ArrayList<Integer> out = new ArrayList<Integer>();
		for(int i=0;i<attributeTypes.size();i++)
		{
			if(attributeTypes.get(i).equals("class"))
			{				
				out.add(i);
			}
		}
		return out;
	}
	
	/**
	 * A static method that given two <code>ExampleRepo</code>s, creates a new 
	 * <code>ExampleRepo</code> which has all examples joined. The method checks that
	 * attribute names and attribute types match. Note that the joined 
	 * <code>ExampleRepo</code> has the supplied owner specified, disreagrding the owners
	 * of supplied <code>ExampleRepo</code>s.
	 * 
	 * @param owner the node in the tree to which these joined examples should be assigned.
	 * @param first the first repository that is joined.
	 * @param second the second repository that is joined.
	 * 
	 * @return the resulting joined repository.
	 * 
	 * @throws IllegalStateException is produced if sizes of attribute names, attribute types,
	 * 			names of the attributes or types of the attributes do not match.
	 */
	public static ExampleRepo join(ClusNode owner, ExampleRepo first, ExampleRepo second)
	{
		ExampleRepo result = new ExampleRepo(owner);
		
		//Attribute values will be set manually
		result.first = false;
		
		//Getting the attribute names and attribute types.
		ArrayList<String> firstAttr = first.getAttributes();
		ArrayList<String> secondAttr = second.getAttributes();
		ArrayList<String> firstTypes = first.getAttributeTypes();
		ArrayList<String> secondTypes = second.getAttributeTypes();
		
		//Handling cases if either of the repositories have empty attributes
		if(firstAttr.size()==0 && secondAttr.size()>0)
		{
			//first branch is empty
			for(int i=0;i<secondAttr.size();i++)
			{				
				result.attributes.add(secondAttr.get(i));
			}
			for(int i=0;i<secondTypes.size();i++)
			{				
				result.attributeTypes.add(secondTypes.get(i));
			}
			for(ArrayList<String> example: second.getExampleValues())
			{
				result.exampleValues.add(example);
			}
			return result;
		}
		else if(firstAttr.size()>0 && secondAttr.size()==0)
		{
			//second branch is empty
			for(int i=0;i<firstAttr.size();i++)
			{				
				result.attributes.add(firstAttr.get(i));
			}
			for(int i=0;i<firstTypes.size();i++)
			{				
				result.attributeTypes.add(firstTypes.get(i));
			}
			for(ArrayList<String> example: first.getExampleValues())
			{
				result.exampleValues.add(example);
			}
			return result;
		}
		
		//Checking that both attribute names and attribute types are consistent.
		if(firstAttr.size() != secondAttr.size())
		{
			throw new IllegalStateException(String.join("", 
					new String[] {"Attribute sizes during joining in ", owner.toString(),
					" do not match."}));
		}
		if(firstTypes.size() != secondTypes.size())
		{
			throw new IllegalStateException(String.join("", 
					new String[] {"Attribute type sizes during joining in ", owner.toString(),
					" do not match."}));
		}
		for(int i=0;i<firstAttr.size();i++)
		{
			if(!firstAttr.get(i).equals(secondAttr.get(i)))
			{
				throw new IllegalStateException(String.join("", 
						new String[] {"Attribute names during joining in ", owner.toString(),
						" do not match. (", firstAttr.get(i), ", ", secondAttr.get(i), ")."}));
			}
			result.attributes.add(firstAttr.get(i));
		}
		for(int i=0;i<firstTypes.size();i++)
		{
			if(!firstTypes.get(i).equals(secondTypes.get(i)))
			{
				throw new IllegalStateException(String.join("", 
						new String[] {"Attribute types during joining in ", owner.toString(), 
						" do not match, for attribute ", firstAttr.get(i), ". (",
						firstTypes.get(i), ", ", secondTypes.get(i), ")."}));
			}
			result.attributeTypes.add(firstTypes.get(i));
		}
		
		//Adding examples to the list of example values.
		for(ArrayList<String> example: first.getExampleValues())
		{
			result.exampleValues.add(example);
		}
		for(ArrayList<String> example: second.getExampleValues())
		{
			result.exampleValues.add(example);
		}
		return result;
	}

	/**
	 * Exporting the example section to XML. It exports the examples as
	 * Example elements, with attributes. Also exportin the attribute
	 * types, names and values.
	 * 
	 * @param document the document in which elements are constructed.
	 * 
	 * @return the Element representing the examples.
	 */
	public Node exportToXML(Document document) 
	{
		Element examples = document.createElement("Examples");
		examples.setAttribute("examples", String.valueOf(getNExamples()));
		for(ArrayList<String> exampleValueList: exampleValues)
		{
			Element example = document.createElement("Example");
			for(int i=0;i<exampleValueList.size();i++)
			{
				Element attribute = document.createElement("Attribute");
				attribute.setAttribute("name", attributes.get(i));
				attribute.setAttribute("type", attributeTypes.get(i));
				attribute.setTextContent(exampleValueList.get(i));
				example.appendChild(attribute);
			}			
			examples.appendChild(example);
		}
		return examples;
	}	
}
