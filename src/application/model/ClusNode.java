/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model;

import java.util.ArrayList;
import java.util.HashSet;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import application.model.examples.ExampleRepo;
import application.model.statistic.Statistic;
import application.model.statistic.StatisticImpl;
import application.model.statistic.WHTDStat;

/**
 * This class is the main building block of the model. It holds the examples and
 * statistic in the node of the model.
 * 
 * @author Nejc Trdin
 *
 */
public class ClusNode {
		
	/**
	 * The test string of the node. Leafs have test string null.
	 */
	private String testString;
	
	/**
	 * The choice string in this node. Either "yes", "no" or "unk". Roots have
	 * choice set to null. 
	 */
	private String choice;
	
	/**
	 * The parent node of this node in the hierarchy.
	 */
	protected ClusNode parent;
	
	/**
	 * The direct children nodes in the hierarchy.
	 */
	private ArrayList<ClusNode> children = new ArrayList<ClusNode>();
	
	/**
	 * Statistic is the intermediate class for storing the particular nodes statistic.
	 */
	private Statistic statistic;
	
	/**
	 * Examples that belong to this node. Variable is null for non-leaf nodes.
	 */
	protected ExampleRepo examples;
	
	/**
	 * This is a variable holding the k-NN model if the whole model is k-NN.
	 */
	private COPKMeansNode copKMeans;	
	
	/**
	 * An empty constructor used when the model is k-NN.
	 */
	public ClusNode(){}
	
	/**
	 * Constructor used for creation of roots in the hierarchy, because they do not have
	 * any parents. It is generated from XML element. Also see {@link #ClusNode(Element)}.
	 * 
	 * @param element the XML description of Node.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public ClusNode(Element element) throws XMLParseException
	{
		this(element, null);
	}
	
	/**
	 * Constructor used for creation of non-roots in the hierarchy from XML element.
	 * Also see {@link #ClusNode(Element)}. The constructor calls various other constructors.
	 * See {@link Statistic#Statistic(Element)}, {@link ExampleRepo#ExampleRepo(Element, ClusNode)},
	 * {@link ExampleRepo#ExampleRepo(ClusNode)} and {@link COPKMeansNode#COPKMeansNode(Element, ClusNode)}.
	 * 
	 * @param element the XML description of Node.
	 * @param parent the parent node in the model tree.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public ClusNode(Element element, ClusNode parent) throws XMLParseException
	{
		this.parent = parent;		
		if(element.getNodeName() == "Node")
		{
			//this is a node which is not a leaf
			if(element.hasAttribute("test"))
			{
				testString = element.getAttribute("test");
			}
			if(element.hasAttribute("choice"))
			{
				choice = element.getAttribute("choice");
			}
			NodeList list = element.getChildNodes();
			
			//parsing nodes and statistic
			for(int i=0; i<list.getLength();i++)
			{
				Node node = list.item(i);
				String nodeName = node.getNodeName();
				if(!(node instanceof Element))
				{
					throw new XMLParseException(
							"Children of Node should always be XML Element (Node, Leaf, Statistic).");
				}
				Element childElement = (Element)node;
				if(nodeName == "Node" || nodeName == "Leaf")
				{
					//parsing other children nodes
					children.add(new ClusNode(childElement, this));
				}
				else if(nodeName.contains("Stat"))
				{
					//parsing statistic
					statistic = new Statistic(childElement);
				}
				else
				{
					throw new XMLParseException(String.join("", 
							new String[]{"Unknown node type: ", nodeName, 
							". Expected Node, Leaf or *Stat."}));
				}					
			}
		}
		else if(element.getNodeName() == "Leaf")
		{
			//node is a leaf
			if(element.hasAttribute("choice"))
			{
				choice = element.getAttribute("choice");
			}
			NodeList list = element.getChildNodes();
			boolean gotExamples = false;
			
			//parsing the examples and statistic
			for(int i=0; i<list.getLength();i++)
			{
				Node node = list.item(i);
				String nodeName = node.getNodeName();
				if(!(node instanceof Element))
				{
					throw new XMLParseException(
							"Children of Node should always be XML Element (Examples or *Stat).");
				}
				Element childElement = (Element)node;
				if(nodeName == "Examples")
				{
					//parsing examples
					examples = new ExampleRepo(childElement, this);
					gotExamples = true;
				}
				else if(nodeName.contains("Stat"))
				{
					//parsing statistic
					statistic = new Statistic(childElement);
				}
				else
				{
					throw new XMLParseException(String.join("", 
							new String[]{"Unknown node type: ", nodeName, ". Expected Examples or *Stat."}));
				}					
			}
			if(!gotExamples)
			{
				//if there are not examples, we create an empty example section
				examples = new ExampleRepo(this);
			}
		}
		else if(element.getNodeName() == "COPKMeans")
		{
			//the model is actually k-NN
			copKMeans = new COPKMeansNode(element, parent);
		}
		else
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Unknown node type: ", element.getNodeName(), ". Expected Node or Leaf."}));
		}
	}

	/**
	 * Gets the {@link COPKMeansNode}. If this {@link ClusNode} is not COPKMeans type,
	 * it returns <code>null</code>.
	 * 
	 * @return the COPKMeansNode.
	 */
	public COPKMeansNode getCOPKMeansNode()
	{
		return copKMeans;
	}
	
	/**
	 * Gets the number of successors of this node. If the model is k-NN it gets it from 
	 * {@link COPKMeansNode#getNSuccessors()}.
	 * 
	 * @return the number of successors.
	 */
	public int getNSuccessors() 
	{
		if(copKMeans != null) return copKMeans.getNSuccessors();
		int sum = children.size();
		for(ClusNode n: children)
		{
			sum += n.getNSuccessors();
		}
		return sum;
	}

	/**
	 * Gets the number of leafs under this node. If the model is k-NN it gets it from 
	 * {@link COPKMeansNode#getNLeafs()}.
	 * 
	 * @return the number of leafs under this node.
	 */
	public int getNLeafs() 
	{	
		if(copKMeans != null) return copKMeans.getNLeafs();
		if(children.size() == 0)
		{
			return 1;
		}
		int sum = 0;
		for(ClusNode n: children)
		{
			sum += n.getNLeafs();
		}
		return sum;
	}

	/**
	 * Gets the statistic name of this node. If the model is k-NN it gets it from 
	 * {@link COPKMeansNode#getStatisticName()}.
	 * 
	 * @return the number of leafs under this node.
	 */
	public String getStatisticName()
	{	
		if(copKMeans != null) return copKMeans.getStatisticName();
		return statistic.getStatisticName();
	}
	
	/**
	 * Gets the test string of this node. If the node is a leaf it returns null. If the model 
	 * is k-NN it gets it from {@link COPKMeansNode#getTestString()}.
	 * 
	 * @return the test string of the node.
	 */
	public String getTestString()
	{		
		if(copKMeans != null) return copKMeans.getTestString();
		return testString;
	}
	
	/**
	 * Gets the choice string of this node. If the node is a root it returns null. If the model 
	 * is k-NN it gets it from {@link COPKMeansNode#getChoice()}.
	 * 
	 * @return the choice string of the node.
	 */
	public String getChoice()
	{	
		if(copKMeans != null) return copKMeans.getChoice();
		return choice;		
	}

	/**
	 * Gets the number of examples in this node. If it is a leaf, it returns the number of examples
	 * in example repository. If not, it recursively calls the children for their example counts and
	 * sums the results together. If the model is k-NN it gets it from {@link COPKMeansNode#getNExamples()}.
	 * 
	 * @return the choice string of the node.
	 */
	public int getNExamples() 
	{	
		if(copKMeans != null) return copKMeans.getNExamples();
		if(children.size()==0)
		{
			if(examples!=null)
			{
				return examples.getNExamples();
			}
			return 0;
		}
		else
		{
			int sum = 0;
			for(ClusNode child: children)
			{
				sum += child.getNExamples();
			}
			return sum;
		}		
	}
	
	/**
	 * Gets the parent of this node. If it is a root it returns null. If the model is k-NN, it 
	 * returns <code>this</code>.
	 * 
	 * @return the parent node of this node.
	 */
	public ClusNode getParent()
	{
		if(copKMeans != null) return this;
		return parent;
	}
	
	/**
	 * Get the {@link ExampleRepo} of this node. If it is a leaf node, it returns the examples.
	 * Otherwise it recursively joins the examples of the children. If it is a k-NN model it returns
	 * the examples from {@link COPKMeansNode#getExamples()}.
	 * 
	 * @return the examples in this node.
	 */
	public ExampleRepo getExamples()
	{
		if(copKMeans != null) return copKMeans.getExamples();
		if(children.size()==0)
		{			
			return examples;
		}
		else
		{
			ExampleRepo result = ExampleRepo.join(this, children.get(0).getExamples(), new ExampleRepo(this));
			for(int i=1; i< children.size();i++)
			{
				result = ExampleRepo.join(this, children.get(i).getExamples(), result);
			}
			return result;
		}
	}
	
	/**
	 * Gets the list of children of this node.
	 * 
	 * @return the list of children.
	 */
	public ArrayList<ClusNode> getChildren()
	{
		return children;
	}
	
	/**
	 * Gets the statistic implementation of this node. If this is a k-NN model it gets
	 * {@link COPKMeansNode#getStatistic()}.
	 * 
	 * @return the statistic implementation.
	 */
	public StatisticImpl getStatistic()
	{
		if(copKMeans != null) return copKMeans.getStatistic();
		return statistic.getImplementation();
	}
	
	/**
	 * Getting the path of the corresponding statistic FXML file used for building the GUI.
	 * If this is a k-NN model it gets {@link COPKMeansNode#getStatisticFXML()}. 
	 * See {@link StatisticImpl#getStatisticFXML()}.
	 * 
	 * @return the string path to the FXML file.
	 */
	public String getStatisticFXML()
	{
		if(copKMeans != null) return copKMeans.getStatisticFXML();
		return statistic.getStatisticFXML();
	}
	
	/**
	 * It reports if this is a k-NN model. This is based on the variable <code>copKMeans</code> being null.
	 * 
	 * @return true if this is a {@link COPKMeansNode} model.
	 */
	public boolean isCOPKMeans()
	{
		return copKMeans != null;
	}
	
	/**
	 * Gets a short description of the {@link COPKMeansNode} model. If this is not a {@link COPKMeansNode}, 
	 * model it returns <code>null</code>.
	 * 
	 * @return a short description for {@link COPKMeansNode} model.
	 */
	public String getCOPKMeansShortDescription()
	{
		if(copKMeans != null) return copKMeans.getShortDescription();
		return null;
	}

	/**
	 * If this is a model with hierarchical predictions, it updates the hierarchy in all nodes of the model
	 * hierarchy. This is because the hierarchy in XML model is described at the end, and there is no way
	 * to parse it before with a single file pass.
	 *  
	 * @param hierarchy reference which is written at the end of the XML file.
	 */
	public void updateHierarchy(Hierarchy hierarchy)
	{		
		if(statistic.getImplementation() instanceof WHTDStat)
		{
			((WHTDStat)statistic.getImplementation()).updateHierarchy(hierarchy);
			for(ClusNode child: children)
			{
				child.updateHierarchy(hierarchy);
			}
		}
	}

	/**
	 * Exports a {@link ClusNode} to XML, while appending the children below as a
	 * structure. Attributes for {@link #testString} and {@link #choice} are
	 * also exported. Child Elements are constructed by the recursive calls.
	 * 
	 * @param document the document in which elements are constructed.
	 * @param exportedNodes the set of nodes, that should be exported.
	 * 
	 * @return the Element representing the hierarchy.
	 */
	public Element exportToXML(Document document, HashSet<ClusNode> exportedNodes)
	{
		boolean isLeaf = true;
		ArrayList<Element> childElements = new ArrayList<Element>();
		for(ClusNode child: children)
		{
			if(exportedNodes.contains(child))
			{
				childElements.add(child.exportToXML(document, exportedNodes));
				isLeaf = false;
			}
		}
		Element node = null;
		if(isLeaf)
		{
			node = document.createElement("Leaf");
			//exporting statistic
			node.appendChild(statistic.exportToXML(document));
			ExampleRepo example = this.getExamples();
			node.appendChild(example.exportToXML(document));
		}
		else
		{
			node = document.createElement("Node");
			//handling test string
			if(testString != null)
			{
				node.setAttribute("test", testString);
			}		
			//exporting statistic
			node.appendChild(statistic.exportToXML(document));
			//exporting children
			for(Element child: childElements)
			{
				node.appendChild(child);
			}
		}	
		//handling choice
		if(choice != null)
		{
			node.setAttribute("choice", choice);
		}		
		return node;
	}
}
