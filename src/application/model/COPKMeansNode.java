/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model;

import java.util.HashMap;
import java.util.HashSet;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import application.model.examples.ExampleRepo;
import application.model.statistic.ILevelCStat;
import application.model.statistic.StatisticImpl;

/**
 * Class used for representing a model for k-NN. It extends the class {@link application.model.ClusNode}.
 * 
 * @author Nejc Trdin
 *
 */
public class COPKMeansNode extends ClusNode {
	
	/**
	 * The statistic used in this node is always {@link application.model.statistic.ILevelCStat}. 
	 */
	private ILevelCStat statistic;
		
	/**
	 * Number of clusters created. 
	 */
	private int clusters = 0;
	
	/**
	 * Number of cluster sets.
	 */
	private int cSets = 0;
	
	/**
	 * Number of iterations used for generation of clusters.
	 */
	private int iterations = 0;
	
	/**
	 * The maximal number of iterations specified in the settings.
	 */
	private int maxIterations = 0;

	/**
	 * The constructor for <code>COPKMeansNode</code> used when parsing the XML file.
	 * 
	 * @param element the XML element representing COPKMeans.
	 * @param parent the {@link ClusNode} parent to which this node belongs to.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public COPKMeansNode(Element element, ClusNode parent) throws XMLParseException
	{
		this.parent = parent;
		if(element.getNodeName() == "COPKMeans")
		{			
			NodeList list = element.getChildNodes();
			boolean gotExamples = false;
			for(int i=0; i<list.getLength();i++)
			{
				Node node = list.item(i);
				String nodeName = node.getNodeName();
				if(!(node instanceof Element))
				{
					throw new XMLParseException(
							"Children of COPKMeans should always be XML Element (Examples or *Stat).");
				}
				Element childElement = (Element)node;
				if(nodeName == "Examples")
				{
					//parsing examples
					examples = new ExampleRepo(childElement, this);
					gotExamples = true;
				}
				else if(nodeName.contains("Stat"))
				{
					//parsing statistic
					statistic = new ILevelCStat(childElement);
				}
				else
				{
					throw new XMLParseException(String.join("", 
							new String[]{"Unknown node type: ", nodeName, ". Expected Examples or *Stat."}));
				}					
			}
			//if there are no examples, we create an empty example repository
			if(!gotExamples)
			{
				examples = new ExampleRepo(this);
			}
		}		
		else
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Unknown node type: ", element.getNodeName(), ". Expected COPKMeans."}));
		}
		//getting number of clusters
		if(element.hasAttribute("clusters"))
		{
			clusters = Integer.parseInt(element.getAttribute("clusters"));
		}
		//getting number of cluster sets
		if(element.hasAttribute("csets"))
		{
			cSets = Integer.parseInt(element.getAttribute("csets"));
		}
		//getting number of iterations
		if(element.hasAttribute("iter"))
		{
			iterations = Integer.parseInt(element.getAttribute("iter"));
		}
		//getting the maximal number of iterations
		if(element.hasAttribute("max"))
		{
			maxIterations = Integer.parseInt(element.getAttribute("max"));
		}
	}

	/**
	 * Getting the number of this nodes successors, which always returns 0.
	 * Also see {@link ClusNode#getNSuccessors()}.
	 * @return always returns 0.
	 */
	@Override
	public int getNSuccessors() 
	{		
		return 0;
	}

	/**
	 * Getting the number of leafs below this node. It always returns 1, since clustering is
	 * always a leaf.  Also see {@link ClusNode#getNLeafs()}.
	 * 
	 * @return always returns 1.
	 */
	@Override
	public int getNLeafs() 
	{		
		return 1;
	}

	/**
	 * Getting the statistic name by calling the name of the statistic.
	 * Also see {@link ClusNode#getStatisticName()}.
	 * 
	 * @return the name of the statistic used.
	 */
	@Override
	public String getStatisticName()
	{		
		return statistic.getStatisticName();
	}
	
	/**
	 * Getting the path of the corresponding statistic FXML file used for building the GUI. Also see
	 * {@link StatisticImpl#getStatisticFXML()}. 
	 * 
	 * @return the string path to the FXML file.
	 */
	@Override
	public String getStatisticFXML()
	{		
		return statistic.getStatisticFXML();
	}
	
	/**
	 * Getting the test string for this node. It always returns <code>null</code> since the k-NN node is
	 * always the whole model. Also see {@link ClusNode#getTestString()}.
	 * 
	 * @return the test string which is null.
	 */
	@Override
	public String getTestString()
	{			
		return null;
	}
	
	/**
	 * Getting the choice string for this node. It always returns <code>null</code> since the k-NN node is
	 * always the whole model. Also see {@link ClusNode#getChoice()}.
	 * 
	 * @return the test string which is null.
	 */
	@Override
	public String getChoice()
	{	
		return null;		
	}

	/**
	 * Getting the number of examples in this node. Also see {@link ClusNode#getNExamples()}.
	 * 
	 * @return the number of examples (if they exist), otherwise 0.
	 */
	@Override
	public int getNExamples() 
	{	
		if(examples!=null)
		{
			return examples.getNExamples();
		}
		return 0;				
	}
	
	/**
	 * Get the parent {@link ClusNode} of this k-NN model.
	 * 
	 * @return the parent of the k-NN model.
	 */
	@Override
	public ClusNode getParent()
	{
		return parent;
	}
	
	/**
	 * Get the {@link ExampleRepo} of this node. Also see {@link ClusNode#getExamples()}.
	 * 
	 * @return the examples in this node.
	 */
	@Override
	public ExampleRepo getExamples()
	{
		return examples;
		
	}	
	
	/**
	 * Get number of clusters in k-NN model.
	 * 
	 * @return the number of clusters.
	 */
	public int getClusters()
	{
		return clusters;
	}
	
	/**
	 * Get number of cluster sets in k-NN model.
	 * 
	 * @return the number of cluster sets.
	 */
	public int getCSets()
	{
		return cSets;
	}
	
	/**
	 * Get number of iterations used for construction of this k-NN model.
	 * 
	 * @return the number of clusters.
	 */
	public int getIterations()
	{
		return iterations;
	}
	
	/**
	 * Get the maximal number of iterations used for construction of this k-NN model.
	 * 
	 * @return the number of clusters.
	 */
	public int getMaxIterations()
	{
		return maxIterations;
	}
	
	/**
	 * Get number of cluster sets in k-NN model, read from the statistic.
	 * See {@link ILevelCStat#getNClusters()}.
	 * 
	 * @return the number of cluster sets.
	 */
	public int getNClusters()
	{
		return statistic.getNClusters();
	}
	
	/**
	 * Get the number of computed representative examples in the cluster corresponding to the ID.
	 * See {@link ILevelCStat#getClusterExamplesByID(String)}.
	 * 
	 * @param id the ID of the cluster.
	 * 
	 * @return the number of examples in particular cluster.
	 */
	public int getClusterExamples(String id)
	{
		return statistic.getClusterExamplesByID(id);
	}
	
	/**
	 * Get the computed representative examples in the cluster corresponding to the ID. 
	 * The method returns a mapping from attributes to values.
	 * See {@link ILevelCStat#getClusterValues(String)}.
	 * 
	 * @param id the ID of the cluster.
	 * 
	 * @return the computed representative examples in particular cluster.
	 */
	public HashMap<String, String> getClusterValues(String id)
	{
		return statistic.getClusterValues(id);
	}
	
	/**
	 * Get the specific implementation of the statistic. Also see {@link ClusNode#getStatistic()}.
	 * 
	 * @return the implementation of statistic.
	 */
	@Override
	public StatisticImpl getStatistic()
	{
		return statistic;
	}
	
	/**
	 * A short description of the k-NN model.
	 * 
	 * @return a <code>String</code> representation of the model.
	 */
	public String getShortDescription()
	{
		return String.join(": ", new String[]{"COPKMeans",
				String.join(", ", new String[]{"CL:"+getNClusters(),
						"S:"+getCSets(), "I:"+getIterations(), "M:"+getMaxIterations()})});
	}
	
	/**
	 * Exports a {@link COPKMeansNode} to XML. Attributes for {@link #clusters},
	 * {@link #cSets}, {@link #iterations} and {@link #maxIterations} are
	 * also exported. 
	 * 
	 * @param document the document in which elements are constructed.
	 * @param exportedNodes the set of nodes, that should be exported.
	 * 
	 * @return the Element representing this COPKMeans node.
	 */
	@Override
	public Element exportToXML(Document document, HashSet<ClusNode> exportedNodes)
	{
		Element node = document.createElement("COPKMeans");
		//exporting statistic
		node.appendChild(statistic.exportToXML(document));
		ExampleRepo example = this.getExamples();
		node.appendChild(example.exportToXML(document));
		
		node.setAttribute("clusters", String.valueOf(clusters));
		node.setAttribute("csets", String.valueOf(cSets));
		node.setAttribute("iter", String.valueOf(iterations));
		node.setAttribute("max", String.valueOf(maxIterations));
				
		return node;
	}
}