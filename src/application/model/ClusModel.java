/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import javax.management.modelmbean.XMLParseException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import application.model.examples.ExampleRepo;

/**
 * The main data structure that holds the whole model. All the interactions for the purpose
 * of GUI go through this class.
 * 
 * @author Nejc Trdin
 *
 */
public class ClusModel {
	
	/**
	 * The model root node.
	 */
	private ClusNode rootNode;
	
	/**
	 * The model name read from XML.
	 */
	private String modelName;
	
	/**
	 * The file name, from which the model was read.
	 */
	private String modelFileName;
	
	/**
	 * Number of nodes of the model.
	 */
	private int nNodes;
	
	/**
	 * Number of leaf in the model.
	 */
	private int nLeafs;
	
	/**
	 * Number of examples used in the model.
	 */
	private int nExamples;
	
	/**
	 * The statistic name used in the model. 
	 */
	private String statisticName;
	
	/**
	 * If the model is hierarchy prediction it also needs to store the 
	 * prediction hierarchy.
	 */
	private Hierarchy hierarchy;
	
	/**
	 * This is the constructor which given an XML file reads its contents, parses the XML structure and then
	 * parses the whole model into the data structures.
	 * 
	 * @param file the XML file used for reading.
	 * 
	 * @throws ParserConfigurationException if the parser configuration does not succeed.
	 * @throws SAXException if there is an exception during library parsing of the XML file.
	 * @throws IOException if the file does not exist.
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public ClusModel(File file) throws ParserConfigurationException, SAXException, IOException, XMLParseException
	{	
		modelFileName = file.getName();
		modelName = "";
		rootNode = new ClusNode();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		doc.getDocumentElement().normalize();
		Element root = doc.getDocumentElement();
		String rootElementName = root.getNodeName();
		
		//root name should be Model
		if(rootElementName == "Model")
		{
			if(root.hasAttribute("name"))
			{
				modelName = root.getAttribute("name");
			}
			else
			{
				modelName = "Unknown";
			}
			NodeList nList = root.getChildNodes();
			int length = 0;
			//in some special cases the node name is "#text" so we need to skip it
			//we also count legit Elements
			for(int i=0;i<nList.getLength();i++)
			{
				if(!nList.item(i).getNodeName().equals("#text"))
				{
					length++;
				}
			}
			
			if(length==1)
			{
				//the model is a regular model with one root and no hierarchy
				if(!(nList.item(0) instanceof Element))
				{
					throw new XMLParseException("Model must have one root of type Element! (Leaf, Node)");
				}
				rootNode = new ClusNode((Element)nList.item(0));	
			}
			else if(length==2)
			{
				//the model is used for hierarchical prediction and should have 2 root elements
				if(!(nList.item(0) instanceof Element))
				{
					throw new XMLParseException("First root of model needs to be type Element! (Model)");
				}
				rootNode = new ClusNode((Element)nList.item(0));
				
				//we need to get the hierarchy, from the index
				boolean gotHierarchy = false;
				for(int i=1;i<nList.getLength();i++)
				{
					if(nList.item(i) instanceof Element && nList.item(i).getNodeName().equals("Hierarchy"))
					{
						gotHierarchy = true;
						hierarchy = new Hierarchy((Element)nList.item(i));
						rootNode.updateHierarchy(hierarchy);
						break;
					}
				}				
				if(!gotHierarchy)
				{
					throw new XMLParseException("Second root of model needs to be type Element! (Hierarchy)");
				}				
			}
			else
			{				
				throw new XMLParseException("Model can have at most two roots of type Element! (Model, Hierarchy)");
			}			
		}
		else
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Unknown model type: ", rootElementName, ". Expected Model."}));
		}		
		
		//computing the model stats
		computeNNodes();
		computeNLeafs();
		computeNExamples();
		statisticName = rootNode.getStatisticName();		
	}
	
	/**
	 * Get the number of nodes in the model.
	 * 
	 * @return the number of nodes.
	 */
	public int getNNodes()
	{
		return nNodes;
	}
	
	/**
	 * Compute the number of nodes in the model. See {@link ClusNode#getNSuccessors()}.
	 */
	private void computeNNodes()
	{
		nNodes = 1 + rootNode.getNSuccessors();
	}
	
	/**
	 * Compute the number of leafs in the model. See {@link ClusNode#getNLeafs()}.
	 */
	private void computeNLeafs()
	{
		nLeafs = rootNode.getNLeafs();
	}
	
	/**
	 * Compute the number of leafs in the model. See {@link ClusNode#getNExamples()}.
	 */
	private void computeNExamples()
	{
		nExamples = rootNode.getNExamples();
	}
	
	/**
	 * Get the number of leafs in the model.
	 * 
	 * @return the number of leafs.
	 */
	public int getNLeafs()
	{
		return nLeafs;
	}
	
	/**
	 * Get the number of examples in the model.
	 * 
	 * @return the number of examples.
	 */
	public int getNExamples()
	{
		return nExamples;
	}
	
	/**
	 * Gets all the examples in the model. See {@link ClusNode#getExamples()}.
	 * 
	 * @return the examples as {@link ExampleRepo}.
	 */
	public ExampleRepo getExamples()
	{
		return rootNode.getExamples();
	}
	
	/**
	 * Gets the statistic name of the model.
	 * 
	 * @return the statistic name as <code>String</code>.
	 */
	public String getStatisticName()
	{
		return statisticName;
	}
	
	/**
	 * Gets the model name.
	 * 
	 * @return the model name as <code>String</code>.
	 */
	public String getModelName()
	{
		return modelName;
	}
	
	/**
	 * Gets the file name from which the model was read..
	 * 
	 * @return the file name as <code>String</code>.
	 */
	public String getModelFileName()
	{
		return modelFileName;
	}
	
	/**
	 * Gets the root node of the model.
	 * 
	 * @return the {@link ClusNode} representing the root.
	 */
	public ClusNode getRoot()
	{
		return rootNode;
	}	
	
	/**
	 * If the model is used for hierarchical prediction it returns an instance of {@link Hierarchy}.
	 * 
	 * @return the hierarchy used for prediction.
	 */
	public Hierarchy getHierarchy()
	{
		return hierarchy;
	}

	/**
	 * The main entry point to construct a XML model, with supplied visible nodes. It constructs
	 * the same model as it receives (in the same format). 
	 * 
	 * @param document the document in which elements are constructed.
	 * @param exportedNodes a set of {@link ClusNode}s, which are visible in the visualization.
	 * 
	 * @return the Element representing the whole model.
	 */
	public Element exportToXML(Document document, HashSet<ClusNode> exportedNodes) 
	{		
		Element model = document.createElement("Model");
		model.setAttribute("name", getModelName());
		
		Element rootElement = null;
		if(rootNode.isCOPKMeans())
		{
			rootElement = rootNode.getCOPKMeansNode().exportToXML(document, exportedNodes);
		}
		else
		{
			rootElement = rootNode.exportToXML(document, exportedNodes);	
		}		
		model.appendChild(rootElement);
		
		//check if hierarchy exists
		if(hierarchy != null)
		{
			Element hierarchyElement = hierarchy.exportToXML(document);
			model.appendChild(hierarchyElement);
		}
		return model;
	}
}
