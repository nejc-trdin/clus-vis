/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model.statistic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This is a class for storing cluster stats.
 * 
 * @author Nejc Trdin
 *
 */
public class ILevelCStat extends StatisticImpl {
	
	/**
	 * A mapping which represents the values of examples in each cluster. The key is the
	 * cluster ID and the value is the number of examples in the corresponding cluster.
	 */
	private HashMap<String, Integer> clusterExamples = new HashMap<String, Integer>();
	
	/**
	 * A mapping which represents the computed representative examples for each cluster.
	 * The key is the cluster ID and the value is a mapping which has values of the 
	 * attributes as key's and values as the computed representative example values for this
	 * particular attribute value. 
	 */
	private HashMap<String, HashMap<String, String>> clusterValues = new HashMap<String, HashMap<String, String>>();
	
	/**
	 * Constructor used for creation of cluster statistics. It is generated from XML element.
	 * 
	 * @param element the XML description of ILevelCStat.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public ILevelCStat(Element element) throws XMLParseException
	{
		if(!element.getNodeName().equals("ILevelCStat"))
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Unknown node type: ", element.getNodeName(), 
					". Expected ILevelCStat."}));
		}
		name = element.getNodeName();		
		NodeList list = element.getChildNodes();
		for(int i=0;i<list.getLength();i++)
		{
			Node node = list.item(i);
			if(!(node instanceof Element))
			{
				throw new XMLParseException(
						"Children of ILevelCStat should always be XML Element "
						+ "(Cluster).");
			}
			String nodeName = node.getNodeName();
			if(nodeName == "Cluster")
			{
				//handling clusters
				String id;				
				Element cluster = (Element)node;
				
				//cluster ID
				if(cluster.hasAttribute("cluster"))
				{
					id = cluster.getAttribute("cluster");
				}
				else
				{
					throw new XMLParseException(
							"Cluster element should have 'cluster' attribute");
				}
				
				// getting number of examples
				if(cluster.hasAttribute("examples"))
				{
					clusterExamples.put(id, Integer.parseInt(cluster.getAttribute("examples")));
				}
				else
				{
					throw new XMLParseException(
							"Cluster element should have 'examples' attribute");
				}
				NodeList targetList = cluster.getChildNodes();
				
				//generating representative examples
				HashMap<String, String> values = new HashMap<String, String>();
				for(int j=0;j<targetList.getLength();j++)
				{
					Node targetNode = targetList.item(j);
					if(!(targetNode instanceof Element))
					{
						throw new XMLParseException(
								"Children of Cluster in ILevelCStat should "
								+ "always be XML Element (Target).");
					}
					Element target = (Element)targetNode;
					if(target.getNodeName().equals("Target"))
					{
						if(!target.hasAttribute("name"))
						{
							throw new XMLParseException(
									"Target should always have a name attribute.");
						}
						//putting the name and the value of the attribute into the mapping
						values.put(target.getAttribute("name"), target.getTextContent());						
					}
					else
					{
						throw new XMLParseException(String.join("", 
								new String[]{"Unknown node type: ", nodeName, 
								". Expected Target."}));
					}
					//putting the cluster representative example into the mapping
					clusterValues.put(id, values);
				}
			}			
			else
			{
				throw new XMLParseException(String.join("", 
						new String[]{"Unknown node type: ", nodeName, 
						". Expected Cluster."}));
			}
		}
	}

	/**
	 * Gets a list of cluster attributes, which is not equal to target attributes.
	 * It adds all attributes from one cluster into a list.
	 * 
	 * @return a list of cluster attributes.
	 */
	public ArrayList<String> getClusterAttributes()
	{
		ArrayList<String> out = new ArrayList<String>();
		for(String name: clusterValues.keySet())
		{
			for(String attr: clusterValues.get(name).keySet())
			{
				out.add(attr);
			}
			break;
		}
		return out;
	}
	
	/**
	 * Gets the number of clusters.
	 * 
	 * @return number of clusters.
	 */
	public int getNClusters() 
	{		
		return clusterExamples.size();
	}
	
	/**
	 * Gets the cluster IDs as a set.
	 * 
	 * @return a set of cluster IDs.
	 */
	public Set<String> getClusterIDs()
	{
		return clusterExamples.keySet();
	}
	
	/**
	 * Gets the total list of examples, by summing the number of examples
	 * over all clusters.
	 * 
	 * @return the total number of examples in all clusters.
	 */
	@Override
	public int getNExamples()
	{
		int n = 0;
		for(Integer i: clusterExamples.values())
		{
			n+=i;
		}		
		return n;
	}

	/**
	 * Get the mapping from cluster IDs to number of examples.
	 * 
	 * @return the mapping of IDs to number of examples.
	 */
	public HashMap<String, Integer> getClusterExamples() 
	{	
		return clusterExamples;
	}

	/**
	 * Get the representative example of the cluster as a mapping attribute--value.
	 * 
	 * @param id of the cluster, for which the representative example is needed.
	 * 
	 * @return the representative example.
	 */
	public HashMap<String, String> getClusterValues(String id) 
	{		
		return clusterValues.get(id);
	}
	
	/**
	 * Get the number of examples in the specified cluster.
	 * 
	 * @param id of the cluster, for which the number of examples is needed.
	 * 
	 * @return the number of examples.
	 */
	public int getClusterExamplesByID(String id)
	{
		return clusterExamples.get(id);
	}

	/**
	 * Gets the short string describing the statistic. See {@link StatisticImpl#getShortString()}.
	 * 
	 * @return the short string representing the statistic.
	 */
	@Override
	public String getShortString() 
	{		
		return String.join("", 
				new String[]{"Clusters: ", String.valueOf(clusterExamples.size())});
	}
	
	/**
	 * Gets the FXML file path of the statistic. See {@link StatisticImpl#getStatisticFXML()}.
	 * 
	 * @return the path to the FXML file as <code>String</code>.
	 */
	@Override
	public String getStatisticFXML() 
	{		
		return "gui/plot/ClusterPlot.fxml";
	}
	
	/**
	 * Gets the XML representation of statistic.
	 * 
	 * @param document the document in which elements are constructed.
	 * 
	 * @return an XML Element representing the statistic.
	 */
	@Override
	public Element exportToXML(Document document) 
	{		
		Element stat = document.createElement("ILevelCStat");		
		for(Entry<String, Integer> entry: clusterExamples.entrySet())
		{
			Element cluster = document.createElement("Cluster");
			cluster.setAttribute("cluster", entry.getKey());
			cluster.setAttribute("examples", String.valueOf(entry.getValue()));
			stat.appendChild(cluster);
			for(Entry<String,String> values: clusterValues.get(entry.getKey()).entrySet())
			{
				Element target = document.createElement("Target");
				target.setAttribute("name", values.getKey());
				target.setTextContent(values.getValue());
				cluster.appendChild(target);
			}
		}
		
		return stat;
	}
}