/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model.statistic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import application.model.Hierarchy;

/**
 * This is a class for storing hierarchy prediction stats.
 * Also see {@link StatisticImpl}.
 * 
 * @author Nejc Trdin
 *
 */
public class WHTDStat extends StatisticImpl {

	/**
	 * The probabilities of nodes in the hierarchy. The key is the node's ID and the
	 * value the corresponding probability.
	 */
	private HashMap<String, Double> targetWeights = new HashMap<String, Double>();
	
	/**
	 * Predictions is a list of paths in the hierarchy, that are constructed.
	 */
	private ArrayList<String> predictions = new ArrayList<String>();
	
	/**
	 * The hierarchy that is actually plotted, pruning the hierarchy for nodes
	 * that have probability 0.0.
	 */
	private Hierarchy displayHierarchy;
	
	/**
	 * Constructor used for creation of hierarchical prediction statistics. It is generated from XML element.
	 * 
	 * @param element the XML description of WHTDStat.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public WHTDStat(Element element) throws XMLParseException
	{
		if(!element.getNodeName().equals("WHTDStat"))
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Unknown node type: ", element.getNodeName(), 
					". Expected WHTDStat."}));
		}
		name = element.getNodeName();
		
		//get the number of examples
		if(element.hasAttribute("examples"))
		{
			nExamples = Integer.parseInt(element.getAttribute("examples"));
		}
		NodeList targets = element.getChildNodes();
		for(int i=0;i<targets.getLength();i++)
		{
			Node node = targets.item(i);
			if(!(node instanceof Element))
			{
				throw new XMLParseException(
						"Children of WHTDStat should always be XML Element "
						+ "(Target).");
			}			
			String nodeName = node.getNodeName();	
			
			//handling targets
			Element target = (Element)node;
			if(target.getNodeName().equals("Target"))
			{
				if(!target.hasAttribute("name"))
				{
					throw new XMLParseException(
							"Target should always have a name attribute.");
				}
				//put the weight of the node ID into the mapping
				targetWeights.put(target.getAttribute("name"), Double.parseDouble(target.getTextContent()));						
			}
			else if(target.getNodeName().equals("Predictions"))
			{
				//we have predictions as a set of paths (representing a hierarchy)
				NodeList predictionList = target.getChildNodes();
				for(int j=0;j<predictionList.getLength();j++)
				{
					Node predictionNode = predictionList.item(j);
					if(!(predictionNode instanceof Element))
					{
						throw new XMLParseException(
								"Children of Predictions in WHTDStat should always be XML Element "
								+ "(Prediction).");
					}
					Element prediction = (Element)predictionNode;
					predictions.add(prediction.getTextContent());
				}
				//create a new empty hierarchy
				displayHierarchy = new Hierarchy();
				//extend the hierarchy with paths
				displayHierarchy.extend(predictions);
			}
			else
			{
				throw new XMLParseException(String.join("", 
						new String[]{"Unknown node type: ", nodeName, 
						". Expected Target or Predictions."}));
			}			
		}
	}

	/**
	 * Gets the short string describing the statistic. See {@link StatisticImpl#getShortString()}.
	 * 
	 * @return the short string representing the statistic.
	 */
	@Override
	public String getShortString() 
	{
		String output = "";
		if(targetWeights.size()>0)
		{
			output = String.join(" ", 
					new String[]{"Weights:", String.valueOf(targetWeights.size())});
		}
		if(predictions.size()>0)
		{
			output = String.join(" ", 
					new String[]{output, "Predictions:", String.valueOf(predictions.size())});
		}
		return output;
	}	
	
	/**
	 * Gets the FXML file path of the statistic. See {@link StatisticImpl#getStatisticFXML()}.
	 * 
	 * @return the path to the FXML file as <code>String</code>.
	 */
	@Override
	public String getStatisticFXML() 
	{		
		return "gui/plot/HierarchyPlot.fxml";
	}

	/**
	 * Sets the display hierarchy while reading the XML file.
	 * 
	 * @param hierarchy that is used for prediction
	 */
	public void updateHierarchy(Hierarchy hierarchy)
	{	
		if(targetWeights.size()>0)
		{
			displayHierarchy = new Hierarchy(hierarchy);
			for(Entry<String, Double> entry: targetWeights.entrySet())
			{
				displayHierarchy.getNodeFromID(entry.getKey()).setWeight(entry.getValue());
			}
			displayHierarchy.prune();
		}
	}
	
	/**
	 * Get the {@link Hierarchy} for the display purposes in the plotting.
	 * 
	 * @return the hierarchy reference.
	 */
	public Hierarchy getDisplayHierarchy()
	{
		return displayHierarchy;
	}
	
	/**
	 * Gets the XML representation of statistic.
	 * 
	 * @param document the document in which elements are constructed.
	 * 
	 * @return an XML Element representing the statistic.
	 */
	@Override
	public Element exportToXML(Document document) 
	{		
		Element stat = document.createElement("WHTDStat");
		stat.setAttribute("examples", String.valueOf(nExamples));
		if(predictions.size()>0)
		{
			Element predictionsEl = document.createElement("Predictions");
			stat.appendChild(predictionsEl);
			for(String predStr: predictions)
			{
				Element prediction = document.createElement("Prediction");
				predictionsEl.appendChild(prediction);
				prediction.setTextContent(predStr);
			}
		}
		else if(targetWeights.size()>0)
		{
			for(Entry<String,Double> entry: targetWeights.entrySet())
			{
				Element target = document.createElement("Target");
				stat.appendChild(target);
				
				target.setAttribute("name", entry.getKey());
				target.setTextContent(String.format(FORMAT, entry.getValue()));
			}
		}
		
		return stat;
	}
}
