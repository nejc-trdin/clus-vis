/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model.statistic;

import java.util.ArrayList;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This is a class for storing timeseries stats.
 * Also see {@link StatisticImpl}.
 * 
 * @author Nejc Trdin
 *
 */
public class TimeseriesStat extends StatisticImpl {
	
	/**
	 * Storing the values of the computed mean.
	 */
	private ArrayList<Double> mean = new ArrayList<Double>();
	
	/**
	 * Storing the values of the computed median.
	 */
	private ArrayList<Double> median = new ArrayList<Double>();
	
	/**
	 * Average distance of the examples from the median. 
	 */
	private double avgDistance = 0.0;

	/**
	 * Constructor used for creation of timeseries statistics. It is generated from XML element.
	 * 
	 * @param element the XML description of TimeseriesStat.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public TimeseriesStat(Element element) throws XMLParseException
	{
		if(!element.getNodeName().equals("TimeseriesStat"))
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Unknown node type: ", element.getNodeName(), 
					". Expected TimeseriesStat."}));
		}
		name = element.getNodeName();
		
		//get number of examples
		if(element.hasAttribute("examples"))
		{
			nExamples = Integer.parseInt(element.getAttribute("examples"));
		}
		NodeList nodes = element.getChildNodes();
		for(int i=0;i<nodes.getLength();i++)
		{
			Node node = nodes.item(i);
			String nodeName = node.getNodeName();
			if(!(node instanceof Element))
			{
				throw new XMLParseException(String.join("", 
						new String[]{"Unknown node type: ", nodeName, 
						". Expected element Median or Mean."}));
			}
			if(nodeName.equals("Median"))
			{
				//paring the median
				String content = node.getTextContent();
				String[] values = content.replace("[", "").replace("]", "").split(",");
				for(String value:values)
				{
					median.add(Double.parseDouble(value));
				}
				Element median = (Element)node;
				if(!median.hasAttribute("distances"))
				{
					throw new XMLParseException("Node Median should have distances attribute.");
				}
				//parsing the average distance
				avgDistance = Double.parseDouble(median.getAttribute("distances"));
			}
			else if(nodeName.equals("Mean"))
			{
				//parsing the mean
				String content = node.getTextContent();
				String[] values = content.replace("[", "").replace("]", "").split(",");
				for(String value:values)
				{
					mean.add(Double.parseDouble(value));
				}
			}
			else
			{
				throw new XMLParseException(String.join("", 
						new String[]{"Unknown node name: ", nodeName, 
						". Expected element Median or Mean."}));
			}
		}
	}
	
	/**
	 * Gets the short string describing the statistic. See {@link StatisticImpl#getShortString()}.
	 * 
	 * @return the short string representing the statistic.
	 */
	@Override
	public String getShortString() 
	{		
		return "TS";
	}

	/**
	 * Gets the FXML file path of the statistic. See {@link StatisticImpl#getStatisticFXML()}.
	 * 
	 * @return the path to the FXML file as <code>String</code>.
	 */
	@Override
	public String getStatisticFXML() 
	{		
		return "gui/plot/TimeseriesPlot.fxml";
	}
	
	/**
	 * Gets the list of doubles, that represent the mean.
	 * 
	 * @return the list as a timeseries.
	 */
	public ArrayList<Double> getMean()
	{
		return mean;
	}
	
	/**
	 * Gets the list of doubles, that represent the median.
	 * 
	 * @return the list as a timeseries.
	 */
	public ArrayList<Double> getMedian()
	{
		return median;
	}
	
	/**
	 * Gets the average distance of examples from the median.
	 * 
	 * @return the average distance from the median.
	 */
	public double getAvgDistance()
	{
		return avgDistance;
	}
	
	/**
	 * Gets the XML representation of statistic.
	 * 
	 * @param document the document in which elements are constructed.
	 * 
	 * @return an XML Element representing the statistic.
	 */
	@Override
	public Element exportToXML(Document document) 
	{		
		Element stat = document.createElement("TimeseriesStat");
		stat.setAttribute("examples", String.valueOf(nExamples));
		Element meanEl = document.createElement("Mean");
		stat.appendChild(meanEl);
		String[] meanStrings = new String[mean.size()];
		for(int i=0;i<mean.size();i++)
		{
			meanStrings[i] = String.format(FORMAT, mean.get(i));
		}
		String meanStr = "["+String.join(",", meanStrings)+"]";
		meanEl.setTextContent(meanStr);
		
		Element medianEl = document.createElement("Median");
		stat.appendChild(medianEl);
		String[] medianStrings = new String[mean.size()];
		for(int i=0;i<median.size();i++)
		{
			medianStrings[i] = String.format(FORMAT, median.get(i));
		}
		String medianStr = "["+String.join(",", medianStrings)+"]";
		medianEl.setTextContent(medianStr);
		medianEl.setAttribute("distances", String.format(FORMAT, avgDistance));
		
		return stat;
	}
}