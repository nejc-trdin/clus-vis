/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model.statistic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This is a class for storing classification stats. It also support 
 * multi-target classification.
 * Also see {@link StatisticImpl}.
 * 
 * @author Nejc Trdin
 *
 */
public class ClassificationStat extends StatisticImpl {
	
	/**
	 * Mapping stores the majorities of targets, where the key is the
	 * target name and the value is the value of the target's majority.
	 */
	private HashMap<String, String> majorities = new HashMap<String, String>();
	
	/**
	 * A mapping that stores distributions of the targets, where the key is the
	 * target name and the value is a distribution. Distribution is described with
	 * a mapping where the key is the value of the target and the key is the corresponding
	 * frequency.
	 */
	private HashMap<String, HashMap<String, Double>> distributions = new HashMap<String, HashMap<String, Double>>();
	
	/**
	 * Constructor used for creation of classification statistics. It is generated from XML element.
	 * 
	 * @param element the XML description of ClassificationStat.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public ClassificationStat(Element element) throws XMLParseException
	{
		if(!element.getNodeName().equals("ClassificationStat"))
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Unknown node type: ", element.getNodeName(), 
					". Expected ClassificationStat."}));
		}
		name = element.getNodeName();
		
		//get the number of examples
		if(element.hasAttribute("examples"))
		{
			nExamples = Integer.parseInt(element.getAttribute("examples"));
		}
		NodeList list = element.getChildNodes();
		for(int i=0;i<list.getLength();i++)
		{
			Node node = list.item(i);
			if(!(node instanceof Element))
			{
				throw new XMLParseException(
						"Children of ClassificationStat should always be XML Element "
						+ "(MajorityClasses, Distributions).");
			}
			String nodeName = node.getNodeName();
			if(nodeName == "MajorityClasses")
			{
				//handling majority classes
				Element targets = (Element)node;
				NodeList targetList = targets.getChildNodes();
				for(int j=0;j<targetList.getLength();j++)
				{
					Node targetNode = targetList.item(j);
					if(!(targetNode instanceof Element))
					{
						throw new XMLParseException(
								"Children of MajorityClasses in ClassificationStat should "
								+ "always be XML Element (Target).");
					}
					Element target = (Element)targetNode;
					if(target.getNodeName().equals("Target"))
					{
						//get the targets name
						if(!target.hasAttribute("name"))
						{
							throw new XMLParseException(
									"Target should always have a name attribute.");
						}
						//put the majority value into the mapping
						majorities.put(target.getAttribute("name"), target.getTextContent());						
					}
					else
					{
						throw new XMLParseException(String.join("", 
								new String[]{"Unknown node type: ", nodeName, 
								". Expected Target."}));
					}
				}
			}
			else if(nodeName == "Distributions")
			{
				//handling distributions over classes
				Element distrs = (Element)node;				
				NodeList targetList = distrs.getChildNodes();
				for(int j=0;j<targetList.getLength();j++)
				{
					Node targetNode = targetList.item(j);
					if(!(targetNode instanceof Element))
					{
						throw new XMLParseException(
								"Children of Distributions in ClassificationStat should "
								+ "always be XML Element (Target).");
					}
					Element target = (Element)targetNode;
					if(target.getNodeName().equals("Target"))
					{
						if(!target.hasAttribute("name"))
						{
							throw new XMLParseException(
									"Target should always have a name attribute.");
						}
						
						//getting the target
						String targetName = target.getAttribute("name");
						
						NodeList valueList = target.getChildNodes();
						
						//create a new distribution
						HashMap<String, Double> distribution = new HashMap<String, Double>(); 
						for(int k=0;k<valueList.getLength();k++)
						{
							Node valueNode = valueList.item(k);
							if(!(valueNode instanceof Element))
							{
								throw new XMLParseException(
										"Children of Target in Distributions in ClassificationStat should "
										+ "always be XML Element (Value).");
							}
							Element elementValue = (Element)valueNode;
							if(!elementValue.hasAttribute("name"))
							{
								throw new XMLParseException(
										"Value should always have a name attribute.");
							}
							//put the value of frequency in the distribution
							distribution.put(elementValue.getAttribute("name"), 
									Double.parseDouble(elementValue.getTextContent()));
						}
						//put the distribution in the target->distribution mapping
						distributions.put(targetName, distribution);
					}
					else
					{
						throw new XMLParseException(String.join("", 
								new String[]{"Unknown node type: ", nodeName, 
								". Expected Target."}));
					}
				}				
			}
			else
			{
				throw new XMLParseException(String.join("", 
						new String[]{"Unknown node type: ", nodeName, 
						". Expected MajorityClasses or Distributions."}));
			}
		}
	}

	/**
	 * Gets the short string describing the statistic. See {@link StatisticImpl#getShortString()}.
	 * 
	 * @return the short string representing the statistic.
	 */
	@Override
	public String getShortString()
	{
		ArrayList<String> majoritiesOut = new ArrayList<String>();
		for(Entry<String, String> entry: majorities.entrySet())
		{
			majoritiesOut.add(String.join(":", 
					new String[]{entry.getKey(), entry.getValue()}));
		}
		return String.join(", ", majoritiesOut);
	}

	/**
	 * Gets the FXML file path of the statistic. See {@link StatisticImpl#getStatisticFXML()}.
	 * 
	 * @return the path to the FXML file as <code>String</code>.
	 */
	@Override
	public String getStatisticFXML() 
	{		
		return "gui/plot/BarPlot.fxml";
	}
	
	/**
	 * Gets a list of targets from the <code>majorities</code> mapping.
	 * 
	 * @return a list of target names.
	 */
	public ArrayList<String> getTargets()
	{
		ArrayList<String> out = new ArrayList<String>();
		for(String key: majorities.keySet())
		{
			out.add(key);
		}
		return out;
	}
	
	/**
	 * Gets a mapping which represents a distribution
	 * of values of this particular <code>selectedTarget</code>.
	 * 
	 * @param selectedTarget the target name, for which a distribution is returned.
	 * 
	 * @return the distribution as a mapping.
	 */
	public HashMap<String, Double> getDistribution(String selectedTarget)
	{
		return distributions.get(selectedTarget);
	}

	/**
	 * Gets the majority value for particular <code>selectedTarget</code>.
	 * 
	 * @param selectedTarget the target name, for which the majority value is returned.
	 * 
	 * @return the majority value.
	 */
	public String getMajority(String selectedTarget) 
	{	
		return majorities.get(selectedTarget);
	}
	
	/**
	 * Gets the XML representation of statistic.
	 * 
	 * @param document the document in which elements are constructed.
	 * 
	 * @return an XML Element representing the statistic.
	 */
	@Override
	public Element exportToXML(Document document) 
	{		
		Element stat = document.createElement("ClassificationStat");
		stat.setAttribute("examples", String.valueOf(nExamples));	
		
		//handling majortiy classes
		Element majorityC = document.createElement("MajorityClasses");
		stat.appendChild(majorityC);
		for(Entry<String,String> entry: majorities.entrySet())
		{
			Element target = document.createElement("Target");
			target.setAttribute("name", entry.getKey());
			target.setTextContent(entry.getValue());
			majorityC.appendChild(target);
		}
		
		//handling distributions
		Element distrs = document.createElement("Distributions");
		stat.appendChild(distrs);
		for(Entry<String, HashMap<String,Double>> entry: distributions.entrySet())
		{
			//create target
			Element target = document.createElement("Target");
			target.setAttribute("name", entry.getKey());
			//create values with frequencies
			for(Entry<String,Double> valueEntry: entry.getValue().entrySet())
			{
				Element value = document.createElement("Value");
				value.setAttribute("name", valueEntry.getKey());
				value.setTextContent(String.valueOf(valueEntry.getValue()));
				target.appendChild(value);
			}			
			distrs.appendChild(target);
		}
		
		return stat;
	}
}