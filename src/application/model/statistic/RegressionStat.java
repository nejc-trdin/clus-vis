/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model.statistic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This is a class for storing regression stats. It also support multi-target regression.
 * Also see {@link StatisticImpl}.
 * 
 * @author Nejc Trdin
 *
 */
public class RegressionStat extends StatisticImpl {

	/**
	 * A mapping that stores target predictions. The key is the target name and prediction
	 * is the value in the mapping.
	 */
	private HashMap<String, Double> targetPredictions = new HashMap<String, Double>();	
	
	/**
	 * Constructor used for creation of regression statistics. It is generated from XML element.
	 * 
	 * @param element the XML description of RegressionStat.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public RegressionStat(Element element) throws XMLParseException
	{
		if(!element.getNodeName().equals("RegressionStat"))
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Unknown node type: ", element.getNodeName(), 
					". Expected RegressionStat."}));
		}
		name = element.getNodeName();
		
		//getting the number of examples
		if(element.hasAttribute("examples"))
		{
			nExamples = Integer.parseInt(element.getAttribute("examples"));
		}
		NodeList targets = element.getChildNodes();
		for(int i=0;i<targets.getLength();i++)
		{
			Node node = targets.item(i);
			if(!(node instanceof Element))
			{
				throw new XMLParseException(
						"Children of RegressionStat should always be XML Element "
						+ "(Target).");
			}			
			String nodeName = node.getNodeName();			
			//handling targets
			Element target = (Element)node;
			if(target.getNodeName().equals("Target"))
			{
				if(!target.hasAttribute("name"))
				{
					throw new XMLParseException(
							"Target should always have a name attribute.");
				}
				
				//putting the target name and prediction into the mapping
				targetPredictions.put(target.getAttribute("name"), Double.parseDouble(target.getTextContent()));						
			}
			else
			{
				throw new XMLParseException(String.join("", 
						new String[]{"Unknown node type: ", nodeName, 
						". Expected Target."}));
			}			
		}
	}

	/**
	 * Gets the short string describing the statistic. See {@link StatisticImpl#getShortString()}.
	 * 
	 * @return the short string representing the statistic.
	 */
	@Override
	public String getShortString()
	{
		ArrayList<String> predictions = new ArrayList<String>();
		for(Entry<String, Double> entry: targetPredictions.entrySet())
		{
			predictions.add(String.join(":", 
					new String[]{entry.getKey(), String.format("%.2f", entry.getValue())}));
		}
		return String.join(", ", predictions);
	}	
	
	/**
	 * Gets a set of prediction targets from the mapping <code>targetPredictions</code>.
	 * 
	 * @return a set of regression targets.
	 */
	public Set<String> getTargets()
	{
		return targetPredictions.keySet();
	}
	
	/**
	 * Gets the predicted target for the supplied target name.
	 * 
	 * @param target name of the target for which the prediction is returned.
	 * 
	 * @return the regression prediction.
	 */
	public Double getTargetPrediction(String target)
	{
		return targetPredictions.get(target);
	}
	
	/**
	 * Gets the FXML file path of the statistic. See {@link StatisticImpl#getStatisticFXML()}.
	 * 
	 * @return the path to the FXML file as <code>String</code>.
	 */
	@Override
	public String getStatisticFXML() 
	{		
		return "gui/plot/RegressionPlot.fxml";
	}
	
	/**
	 * Gets the XML representation of statistic.
	 * 
	 * @param document the document in which elements are constructed.
	 * 
	 * @return an XML Element representing the statistic.
	 */
	@Override
	public Element exportToXML(Document document) 
	{		
		Element stat = document.createElement("RegressionStat");
		stat.setAttribute("examples", String.valueOf(nExamples));
		for(Entry<String,Double> entry: targetPredictions.entrySet())
		{
			Element target = document.createElement("Target");
			target.setAttribute("name", entry.getKey());
			target.setTextContent(String.format(FORMAT, entry.getValue()));
			stat.appendChild(target);
		}
		return stat;
	}
}
