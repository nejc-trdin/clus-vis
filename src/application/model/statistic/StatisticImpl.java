/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model.statistic;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This is an <code>abstract</code> class used for implementing statistics. 
 * 
 * @author Nejc Trdin
 *
 */
public abstract class StatisticImpl {
	
	/**
	 * The format used for formatting doubles during export.	
	 */
	protected static final String FORMAT = "%.5f";
	
	/**
	 * The statistic name.
	 */
	protected String name;
	
	/**
	 * Number of examples in this statistic.
	 */
	protected int nExamples;	
	
	/**
	 * Gets the statistic name.
	 * 
	 * @return the name of the statistic.
	 */
	public String getStatisticName()
	{
		return name;
	}
	
	/**
	 * Gets number of examples.
	 * 
	 * @return the number of examples.
	 */
	public int getNExamples()
	{
		return nExamples;
	}	
	
	/**
	 * An <code>abstract</code> method used for getting a short string
	 * representation of the statistic, which is used for display in labels.
	 * 
	 * @return a short string representation of statistic.
	 */
	public abstract String getShortString();
	
	/**
	 * The path to the corresponding statistic's FXML file from the <code>ClusVis</code> class position.
	 * 
	 * @return the path to the FXML file.
	 */
	public abstract String getStatisticFXML();

	/**
	 * An abstract method for exporting statistics to XML element.
	 * 
	 * @param document the document in which elements are constructed.
	 * 
	 * @return an XML Element representing the statistic.
	 */
	public abstract Element exportToXML(Document document);
}
