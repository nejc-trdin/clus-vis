/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model.statistic;

import java.util.Arrays;
import java.util.HashSet;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This is a main entry point for parsing the statistic. It also holds the specific implementation.
 * See {@link StatisticImpl}.
 * 
 * @author Nejc Trdin
 *
 */
public class Statistic {
	
	/**
	 * The allowed statistic names in the XML.
	 */
	private static final String[] STATS = new String[] {
		"ClassificationStat", "RegressionStat", "WHTDStat", "BitVectorStat", "UnknownStat", 
		"ILevelCStat", "TimeseriesStat", "CombStat"};
	/**
	 * The allowed names as a hash set.
	 */
	private static final HashSet<String> ALLOWEDSTATS = new HashSet<String>(Arrays.asList(STATS));
	
	/**
	 * A specific implementation of the statistic.
	 */
	private StatisticImpl implementation;
	
	/**
	 * The constructor receives an XML element type, which describes a statistic. It calls the correct
	 * method for handling the statistic, based on its name.
	 * 
	 * @param element the XML element holding the statistic description.
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public Statistic(Element element) throws XMLParseException
	{	
		String name = element.getNodeName();
		if(!ALLOWEDSTATS.contains(name))
		{
			throw new XMLParseException(String.join("", 
					new String[]{name, " is not one of the allowed Stats types. Allowed are only: ",
					String.join(", ", STATS), "."}));
		}
		if(name.equals("ClassificationStat"))
		{
			implementation = new ClassificationStat(element);
		}
		else if(name.equals("RegressionStat"))
		{
			implementation = new RegressionStat(element);
		}
		else if(name.equals("WHTDStat"))
		{
			implementation = new WHTDStat(element);
		}
		else if(name.equals("BitVectorStat"))
		{
			implementation = new BitVectorStat(element);
		}
		else if(name.equals("ILevelCStat"))
		{
			implementation = new ILevelCStat(element);
		}
		else if(name.equals("TimeseriesStat"))
		{
			implementation = new TimeseriesStat(element);
		}
		else if(name.equals("CombStat"))
		{
			implementation = new CombStat(element);
		}
		else if(name.equals("UnknownStat"))
		{
			implementation = new UnknownStat(element);
		}				
	}
	
	/**
	 * Gets the statistic name. See {@link StatisticImpl#getStatisticName()}.
	 * 
	 * @return the statistic name.
	 */
	public String getStatisticName()
	{
		if(implementation != null)
		{			
			return implementation.getStatisticName();
		}
		return null;
	}
	
	/**
	 * Gets the number of examples reported by the statistic. See {@link StatisticImpl#getNExamples()}.
	 * 
	 * @return number of examples reported by the statistic.
	 */
	public int getNExamples()
	{
		if(implementation != null)
		{
			return implementation.getNExamples();
		}
		return 0;
	}
	
	/**
	 * Gets the specific implementation of the statistic.
	 * 
	 * @return the implementation of statistic.
	 */
	public StatisticImpl getImplementation()
	{
		return implementation;
	}
	
	/**
	 * Gets the path to the statistic's corresponding FXML file. 
	 * See {@link StatisticImpl#getStatisticFXML()}.
	 * 
	 * @return the path to the FXML file.
	 */
	public String getStatisticFXML()
	{
		return implementation.getStatisticFXML();
	}

	/**
	 * An intermediate function for exporting statistics to XML. It calls
	 * the appropriate implementation specific export function.
	 * 
	 * @param document the document in which elements are constructed.
	 * 
	 * @return the XML Element representing the statistic.
	 */
	public Element exportToXML(Document document) 
	{	
		return implementation.exportToXML(document);
	}
}
