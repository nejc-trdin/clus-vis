package application.model.statistic;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This is a class for storing combined stats (regression and classification).
 * It also support multi-target regression and multi-target classification.
 * Also see {@link StatisticImpl}, {@link RegressionStat} and {@link ClassificationStat}.
 * 
 * @author Nejc Trdin
 *
 */
public class CombStat extends StatisticImpl {

	/**
	 * The classification statistic.
	 */
	private ClassificationStat classification;
	
	/**
	 * The regression statistic.
	 */
	private RegressionStat regression;
	
	/**
	 * Constructor used for creation of combined statistics. It is generated from XML element.
	 * 
	 * @param element the XML description of CombStat.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public CombStat(Element element) throws XMLParseException
	{
		if(!element.getNodeName().equals("CombStat"))
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Unknown node type: ", element.getNodeName(), 
					". Expected CombStat."}));
		}
		name = element.getNodeName();		
		NodeList stats = element.getChildNodes();
		for(int i=0;i<stats.getLength();i++)
		{
			Node node = stats.item(i);
			String nodeName = node.getNodeName();
			
			if(nodeName.equals("ClassificationStat") && (node instanceof Element))
			{
				//setting the classification
				classification = new ClassificationStat((Element)node);
				//setting number of examples
				nExamples = classification.getNExamples();
			}
			else if(nodeName.equals("RegressionStat") && (node instanceof Element))
			{
				//setting the regression
				regression = new RegressionStat((Element)node);
				//setting number of examples
				nExamples = regression.getNExamples();
			}
			else
			{
				throw new XMLParseException(String.join("", 
						new String[]{"Unknown node type in CombStat: ", nodeName, 
						". Expected RegressionStat or ClassificationStat."}));
			}
		}
	}
	
	/**
	 * Gets the short string describing the statistic. See {@link StatisticImpl#getShortString()}.
	 * 
	 * @return the short string representing the statistic.
	 */
	@Override
	public String getShortString() 
	{	
		return String.join(", ", new String[]{classification.getShortString(), regression.getShortString()});
	}

	/**
	 * Gets the FXML file path of the statistic. See {@link StatisticImpl#getStatisticFXML()}.
	 * 
	 * @return the path to the FXML file as <code>String</code>.
	 */
	@Override
	public String getStatisticFXML() 
	{		
		return "gui/plot/CombStatPlot.fxml";
	}

	/**
	 * Get the regression statistic of the combination.
	 * 
	 * @return the regression statistic.
	 */
	public RegressionStat getRegression()
	{
		return regression;
	}

	/**
	 * Get the classification statistic of the combination.
	 * 
	 * @return the classification statistic.
	 */
	public ClassificationStat getClassification() 
	{
		return classification;
	}
	
	/**
	 * Gets the XML representation of statistic.
	 * 
	 * @param document the document in which elements are constructed.
	 * 
	 * @return an XML Element representing the statistic.
	 */
	@Override
	public Element exportToXML(Document document) 
	{		
		Element comb = document.createElement("CombStat");
		comb.appendChild(classification.exportToXML(document));
		comb.appendChild(regression.exportToXML(document));
		return comb;
	}
}
