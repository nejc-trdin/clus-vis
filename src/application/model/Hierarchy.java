/*************************************************************************
 * ClusVis is a visualization application for models produced with Clus. * 
 * To use the tool, import an xml file generated with Clus giving it     *
 * -xml argument.                                                        *
 *                                                                       *
 * Clus is a decision tree and rule induction system that implements the *
 * predictive clustering framework. This framework unifies unsupervised  *
 * clustering and predictive modeling and allows for a natural           *
 * extension to more complex prediction settings such as multi-task      *
 * learning and multi-label classification. While most decision tree     *
 * learners induce classification or regression trees, Clus generalizes  *
 * this approach by learning trees that are interpreted as cluster       *
 * hierarchies. We call such trees predictive clustering trees or PCTs.  *
 * Depending on the learning task at hand, different goal criteria are   *
 * to be optimized while creating the clusters, and different            *
 * heuristics will be suitable to achieve this.                          *
 *                                                                       *
 * Clus - Software for Predictive Clustering                             *
 * Copyright (C) 2015                                                    *
 *    Katholieke Universiteit Leuven, Leuven, Belgium                    *
 *    Jozef Stefan Institute, Ljubljana, Slovenia                        *
 *                                                                       *
 * This program is free software: you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * This program is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public License     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                       *
 * Contact information for ClusVis: (Nejc Trdin) nejc.trdin@ijs.si.      *
 * Contact information: <http://www.cs.kuleuven.be/~dtai/clus/>.         *
 *************************************************************************/

package application.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.management.modelmbean.XMLParseException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * A class for the hierarchy for usage in prediction.
 * 
 * @author Nejc Trdin
 *
 */

public class Hierarchy 
{
	/**
	 * List of roots of the hierarchy.
	 */	
	private ArrayList<HierarchyNode> roots = new ArrayList<HierarchyNode>();
	
	/**
	 * A mapping used for getting nodes of the hierarchy from thier IDs.
	 */
	private HashMap<String,HierarchyNode> nodeIDs = new HashMap<String,HierarchyNode>(); 
	
	/**
	 * A constructor for the hierarchy from an XML element Hierarchy. This constructor is used
	 * when the hierarchy is parsed from the XML file.
	 * 
	 * @param hierarchy an XML descritpion of the hierarchy.
	 * 
	 * @throws javax.management.modelmbean.XMLParseException when there is an inconsistency 
	 * 			in the supplied element.
	 */
	public Hierarchy(Element hierarchy) throws XMLParseException
	{
		if(!hierarchy.getNodeName().equals("Hierarchy"))
		{
			throw new XMLParseException(String.join("", 
					new String[]{"Expected hierarchy, got : ", hierarchy.getNodeName(), "."}));
		}
		NodeList rootC = hierarchy.getChildNodes();
		for(int i=0;i<rootC.getLength();i++)
		{
			Node child = rootC.item(i);
			if(!(child instanceof Element))
			{
				throw new XMLParseException(String.join("", 
						new String[]{"Roots of hierarchy can only be elements."}));
			}
			roots.add(new HierarchyNode((Element)rootC.item(i), this));
		}
	}
	
	/**
	 * Method used by nodes, to set thier corresponding node IDs.
	 * @param id the node id.
	 * @param node reference to the corresponding node.
	 * 
	 * @throws IllegalStateException if the ID is already present in the hierarchy.
	 * 			This should not happen.
	 */
	public void setNodeID(String id, HierarchyNode node) throws IllegalStateException
	{
		if(nodeIDs.containsKey(id))
		{
			throw new IllegalStateException(String.join("", 
					new String[]{"ID ", id, " already in the IDs."}));
		}
		nodeIDs.put(id, node);
	}
	
	/**
	 * A constructor that creates an empty hierarchy.
	 */
	public Hierarchy(){}
	
	/**
	 * A constructor used for cloning an existing hierarchy.
	 * 
	 * @param old the old hierarchy being cloned. 	 
	 */
	public Hierarchy(Hierarchy old)
	{
		for(HierarchyNode root: old.getRoots())
		{
			roots.add(new HierarchyNode(root, this));
		}
	}
	
	/**
	 * Getting the roots of the hierarchy.
	 * 
	 * @return the list of roots.
	 */
	public ArrayList<HierarchyNode> getRoots()
	{
		return roots;
	}
	
	/**
	 * Given a list of paths, each path is used to extend the hierarchy with it 
	 * {@link application.model.HierarchyNode#extend(String)}.
	 * 
	 * @param paths a list of paths used for extending, separated with "/".
	 */
	public void extend(ArrayList<String> paths)
	{
		for(String path: paths)
		{
			boolean handled = false;
			String[] names = path.split("/");
			for(HierarchyNode root: roots)
			{
				if(names[0].equals(root.getName()))
				{
					handled = true;
					root.extend(String.join("/", Arrays.copyOfRange(names, 1, names.length)));
					break;
				}
			}
			//if this path was not handled, we need to construct a new root and all nodes below
			if(!handled)
			{
				roots.add(new HierarchyNode(path));
			}
		}			
	}
	
	/**
	 * Get the <code>HierarchyNode</code> corresponding to the node ID.
	 * 
	 * @param id a string ID of the node.
	 * 
	 * @return the node corresponding to the ID.
	 */
	public HierarchyNode getNodeFromID(String id)
	{
		return nodeIDs.get(id);
	}

	/**
	 * Method used for pruning the hierarchy, removing all nodes and their corresponding
	 * sub-trees, which have probability of 0.0. Also see {@link application.model.HierarchyNode#prune()}.
	 */
	public void prune() 
	{		
		ArrayList<HierarchyNode> newRoots = new ArrayList<HierarchyNode>();
		for(HierarchyNode root: roots)
		{
			if(root.getWeight()>0.0)
			{
				newRoots.add(root);
				root.prune();
			}
		}
		roots = newRoots;		
	}

	/**
	 * Exporting the hierarchy to XML Element. It exports all the roots, by calling
	 * the method {@link HierarchyNode#exportToXML(Document)} and appending them
	 * hierarchically.
	 * 
	 * @param document the document in which elements are constructed.
	 * 
	 * @return the Element representing the hierarchy.
	 */
	public Element exportToXML(Document document) 
	{
		Element hierarchy = document.createElement("Hierarchy");
		for(HierarchyNode node: roots)
		{
			hierarchy.appendChild(node.exportToXML(document));
		}
		return hierarchy;
	}
}
