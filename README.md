# ClusVis
ClusVis is a visualization application for models produced with Clus. To use the tool, import an xml file generated with Clus giving it an `-xml` argument.

The tool supports viewing of different predictions (*Multi-target regression*, *Multi-target classification*, *Hierarchy prediction*, *Timeseries prediction* and *Cluster prediction*). It also supports merging and expanding the models and after that exporting the merged models. This utility is useful for hand pruning the tree, and opens a possibility to feed it back into Clus. All of the visualizations and predictions can also be exported into PNG image files for later usage.

## Clus
Clus is a *decision tree* and *rule induction* system that implements the *predictive clustering* framework. This framework unifies *unsupervised clustering* and *predictive modeling* and allows for a natural extension to more complex prediction settings such as *multi-task learning* and *multi-label classification*. While most decision tree learners induce *classification* or *regression trees*, Clus generalizes this approach by learning trees that are interpreted as *cluster hierarchies*. We call such trees *predictive clustering trees* or *PCTs*. Depending on the learning task at hand, different goal criteria are to be optimized while creating the clusters, and different heuristics will be suitable to achieve this.

## Retrieving ClusVis
There are two options to get *ClusVis*.
1. Download the compiled *jar* from *build/clus-vis.jar*.
2. Download the whole repository using *Git*:
   1. *# git clone git@gitlab.com:nejc-trdin/clus-vis.git*
   2. Import the whole project into *Eclipse* or *NetBeans*.

## Example usage

Retrieve the *jar* files from *build* directory.

`java -jar clus-xml.jar -xml some_settings_file.s`

`java -jar clus-vis.jar`

When the window of *ClusVis* is started, click *File>Open* and select the previously generated `XML` file.

Note that `clus-vis.jar` is written using *JavaFX 2.0* and hence needs at least **Java JRE 1.8** to run.

## Documentation

The documentation for *ClusVis* is available as *javadoc* in *doc/* directory of the repository.

---

## Copyright
Clus - Software for Predictive Clustering 
Copyright &copy; 2015
* Katholieke Universiteit Leuven, Leuven, Belgium
* Jozef Stefan Institute, Ljubljana, Slovenia

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see [GNU License](http://www.gnu.org/licenses/).

---

## Contact
Contact information for ClusVis: [Nejc Trdin](mailto:nejc.trdin@ijs.si).

Contact information: [Clus Contact](http://www.cs.kuleuven.be/~dtai/clus/). 
